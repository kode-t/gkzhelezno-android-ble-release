package ru.majordom.blemonitor.ble.device.listeners;

public interface OnBondingChangedListener {
    void call(boolean isBonded);
}
