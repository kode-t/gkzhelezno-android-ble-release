package ru.majordom.blemonitor.ble.device.gatt.callbacks;

public interface GattCharacteristicReadCallback extends GattErrorListener {
    void onRead(byte[] characteristic);
}
