package ru.majordom.blemonitor.ble.device.gatt.operations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import ru.majordom.blemonitor.ble.device.interaction.BaseGatt;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

import java.util.Arrays;
import java.util.UUID;

import ru.majordom.blemonitor.ble.device.gatt.callbacks.GattCharacteristicWriteCallback;

public class GattCharacteristicWriteOperation extends GattDataOperation {
    private final byte[] value;
    private boolean withResponse;
    private final GattCharacteristicWriteCallback callback;

    GattCharacteristicWriteOperation(BlueteethDevice device, UUID service, UUID characteristic, int bytesCount, byte[] value, boolean withResponse, GattCharacteristicWriteCallback callback) {
        super(device, service, characteristic, bytesCount);
        this.value = value;
        this.callback = callback;
        this.withResponse = withResponse;
        if (!withResponse) {
            this.delay = WWOR_DELAY;
        }
        hasAvailableCompletionCallback = true;
    }

    @Override
    public void execute() {
        Log.d(TAG, String.format("Writing %s to {%s}", Arrays.toString(value), characteristic));
        BaseGatt gatt = device.getBluetoothGatt();
        BluetoothGattCharacteristic gattCharacteristic = getCharacteristic(gatt);
        if (gattCharacteristic != null) {
            if (!withResponse) {
                gattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            }
            gattCharacteristic.setValue(value);
            gatt.writeCharacteristic(gattCharacteristic);
        }
    }

    @Override
    public boolean canExecute() {
        // check value before execution
        return super.canExecute() && isDataLengthCorrect(value);
    }

    @Override
    public void onTimeout() {
        super.onTimeout();
        if (callback != null) {
            callback.onTimeout();
        }
    }

    @Override
    public void onError(String errorMsg) {
        super.onError(errorMsg);
        if (callback != null) {
            callback.onError(errorMsg);
        }
    }

    public void onWrite() {
        if (callback != null) {
            callback.onWrite();
        }
        super.onOperationCompleted();
    }
}
