package ru.majordom.blemonitor.ble;

import android.os.Build;
import android.support.annotation.IntRange;

public class BluetoothConstants {
    public static final int ONE_SECOND = 1000;

    //
    public static final String GATE_GENERAL_NAME = "MAJORBLE";
    public static final int GATE_ID_PLACE_IN_NAME = 1;
    public static final int GATE_ORDER_PLACE_IN_NAME = 2;
    public static final String GATE_DEVICE_NUM_PREFIX = "_";
    public static final String GATE_ADVERTISING_MARKER = "00006D62-0000-1000-8000-00805F9B34FB";

    // ble device behaviour constants
    private static final int L_TIME = 4;
    private static final int M_TIME = 5;
    private static final int N_TIME = 6;

    public static final int SCAN_POWER_LOW = 0;
    public static final int SCAN_POWER_MID = 1;
    public static final int SCAN_POWER_HIGH = 2;

    public static final int CONN_PRIORITY_LOW = 0;
    public static final int CONN_PRIORITY_MID = 1;
    public static final int CONN_PRIORITY_HIGH = 2;

    public static final int MIN_RSSI_LEVEL = -100;
    public static final int CONNECTION_TIMEOUT = 3 * ONE_SECOND;
    public static final int DISCOVERING_TIMEOUT = 5 * ONE_SECOND;
    public static final int GATE_STATUS_EXPLICIT_READ_AFTER = 5 * ONE_SECOND;
    public static final int PAUSE_DEFAULT = 5 * BluetoothConstants.ONE_SECOND;
    public static final int PAUSE_CONN_AUTH_ERROR = BluetoothConstants.ONE_SECOND;
    public static final int PAUSE_AFTER_TECHNICAL_ISSUE = 4 * BluetoothConstants.ONE_SECOND;
    public static final int PAUSE_AFTER_NETWORK_ISSUE = 14 * BluetoothConstants.ONE_SECOND;
    public static final int SCAN_MODE = SCAN_POWER_HIGH;
    public static final int CONN_PRIORITY = CONN_PRIORITY_HIGH;
    public static final int DEFAULT_DISTANCE_LIMIT = 3;
    public static final int DEFAULT_K_COEFFICIENT = 60;

    private static volatile int minRssiLevel = MIN_RSSI_LEVEL;
    private static volatile int connectionTimeout = CONNECTION_TIMEOUT;
    private static volatile int discoveringTimeout = DISCOVERING_TIMEOUT;
    private static volatile int gateStatusReadAfter = GATE_STATUS_EXPLICIT_READ_AFTER;
    private static volatile int scanTimeout = getStandardScanTimeoutBasedOnApi();
    private static volatile int pauseDefault = PAUSE_DEFAULT;
    private static volatile int pauseConnAuthError = PAUSE_CONN_AUTH_ERROR;
    private static volatile int pauseAfterTech = PAUSE_AFTER_TECHNICAL_ISSUE;
    private static volatile int pauseAfterNetwork = PAUSE_AFTER_NETWORK_ISSUE;
    private static volatile int scanMode = SCAN_MODE;
    private static volatile int connPriority = CONN_PRIORITY;
    private static volatile int distanceLimit = DEFAULT_DISTANCE_LIMIT;
    private static volatile int kCoefficient = DEFAULT_K_COEFFICIENT;

    // DO NOT CALL THESE DIRECTLY, RATHER USE Prefs.BleOptions instance

    public static int getMinRssiLevel() {
        return minRssiLevel;
    }

    public static void setMinRssiLevel(int level) {
        minRssiLevel = level;
    }

    public static int getConnectionTimeout() {
        return connectionTimeout;
    }

    public static void setConnectionTimeout(int timeout) {
        connectionTimeout = timeout;
    }

    public static int getGateStatusReadAfter() {
        return gateStatusReadAfter;
    }

    public static void setGateStatusReadAfter(int timeout) {
        gateStatusReadAfter = timeout;
    }

    public static int getScanTimeout() {
        return scanTimeout;
    }

    public static void setScanTimeout(int timeout) {
        scanTimeout = timeout;
    }

    public static int getDiscoveringTimeout() {
        return DISCOVERING_TIMEOUT;
    }

    public static void setDiscoveringTimeout(int timeout) {
        discoveringTimeout = timeout;
    }

    public static int getStandardScanTimeoutBasedOnApi() {
        int standardTime = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            standardTime = L_TIME;
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            standardTime = M_TIME;
        } else {
            standardTime = N_TIME;
        }
        return standardTime * ONE_SECOND;
    }

    public static int getPauseDefault() {
        return pauseDefault;
    }

    public static void setPauseDefault(int pause) {
        pauseDefault = pause;
    }

    public static int getPauseConnAuthError() {
        return pauseConnAuthError;
    }

    public static void setPauseConnAuthError(int pause) {
        pauseConnAuthError = pause;
    }

    public static int getPauseAfterTechnicalIssue() {
        return pauseAfterTech;
    }

    public static void setPauseAfterTechnicalIssue(int pause) {
        pauseAfterTech = pause;
    }

    public static int getPauseAfterNetwork() {
        return pauseAfterNetwork;
    }

    public static void setPauseAfterNetwork(int pause) {
        pauseAfterNetwork = pause;
    }

    public static int getScanMode() {
        return scanMode;
    }

    public static void setScanMode(@IntRange(from = SCAN_POWER_LOW, to = SCAN_POWER_HIGH) int mode) {
        scanMode = mode;
    }

    public static int getConnPriority() {
        return connPriority;
    }

    public static void setConnPriority(@IntRange(from = CONN_PRIORITY_LOW, to = CONN_PRIORITY_HIGH) int priority) {
        connPriority = priority;
    }

    public static int getDistanceLimit() {
        return distanceLimit;
    }

    public static void setDistanceLimit(int distanceLimit) {
        BluetoothConstants.distanceLimit = distanceLimit;
    }

    public static int getkCoefficient() {
        return kCoefficient;
    }

    public static void setkCoefficient(int kCoefficient) {
        BluetoothConstants.kCoefficient = kCoefficient;
    }
}