package ru.majordom.blemonitor.ble.device.listeners;


import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

public interface OnConnectionChangedListener {
    void call(BlueteethDevice thisDevice, boolean isConnected);
}
