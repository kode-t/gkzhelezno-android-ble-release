package ru.majordom.blemonitor.ble.device.gatt.operations;

import android.support.annotation.NonNull;

import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

import java.util.UUID;

public abstract class GattDataOperation extends GattOperation {
    public static final int DO_NOT_CHECK_BYTES = -1;
    protected int bytesCount;

    protected GattDataOperation(@NonNull BlueteethDevice device, @NonNull UUID service, @NonNull UUID characteristic, int bytesCount) {
        super(device, service, characteristic);
        this.bytesCount = bytesCount;
    }

    public boolean isDataLengthCorrect(byte[] bytes) {
        if (bytesCount != DO_NOT_CHECK_BYTES && bytes.length != bytesCount) {
            lastInternalCheckError = "Characteristic " + this.characteristic.toString() + " error! Operation bytes length: " + bytes.length + ", expected: " + bytesCount;
            return false;
        }
        return true;
    }
}
