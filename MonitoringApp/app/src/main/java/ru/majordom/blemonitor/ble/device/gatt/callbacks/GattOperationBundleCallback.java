package ru.majordom.blemonitor.ble.device.gatt.callbacks;

public interface GattOperationBundleCallback extends GattErrorListener {
    void onAllOperationsDone();
}
