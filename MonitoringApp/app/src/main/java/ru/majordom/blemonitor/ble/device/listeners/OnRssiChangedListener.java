package ru.majordom.blemonitor.ble.device.listeners;

public interface OnRssiChangedListener {
    void onRssiChanged(int currentRssi, int previousRssi, int stashedRssi, int timesRssiRead);
}
