package ru.majordom.blemonitor.ble.interaction;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import ru.majordom.blemonitor.R;
import ru.majordom.blemonitor.ble.ErrorListener;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethManager;
import ru.majordom.blemonitor.ble.device.listeners.OnDeviceDiscoveredListener;
import ru.majordom.blemonitor.ble.device.listeners.OnScanCompletedListener;
import ru.majordom.blemonitor.ble.interaction.search_strategy.SearchStrategy;

import java.util.List;

public class DeviceModule extends BaseModule {

    private final static String TAG = DeviceModule.class.getSimpleName();

    public static void searchForGate(
            @NonNull final Context context,
            @NonNull final BlueteethManager manager,
            @NonNull final SearchStrategy searchStrategy,
            @NonNull final SearchListener listener) {
        Log.d(TAG, "Searching for devices...");
        manager.scanForPeripherals(searchStrategy.getDefaultScanTimeout(),
                new OnDeviceDiscoveredListener() {
                    @Override
                    public void call(BlueteethDevice blueteethDevice) {
                        boolean isFound = searchStrategy.checkDeviceFound(blueteethDevice);
                        if (isFound) {
                        //if (searchStrategy.isSingleDeviceScan() && isFound) {
                            //manager.stopScanForPeripherals();
                            listener.onDeviceSpotted(blueteethDevice);
                        }
                    }
                }, new OnScanCompletedListener() {
                    @Override
                    public void call(List<BlueteethDevice> blueteethDevices) {
                        final BlueteethDevice chosenDevice = searchStrategy.pickTheMostSuitable();
                        if (chosenDevice == null) {
                            Log.d(TAG, context.getString(R.string.bluetooth_search_no_device));
                            getUiHandler().post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onNoDeviceFound();
                                }
                            });
                        } else {
                            getUiHandler().post(new Runnable() {
                                @Override
                                public void run() {
                                    chosenDevice.setInUse(true);
                                    listener.onSearchCompleted(chosenDevice);
                                }
                            });
                        }
                        searchStrategy.release();
                    }
                });
    }

    /*public static void connectToTheGate(
            @NonNull final Context context,
            final BlueteethDevice device,
            @NonNull final BaseGatt gatt,
            @NonNull final OnConnectionChangedListener connChangedListener,
            @NonNull final ConnectionListener listener) {
        if (device != null) {
            final boolean[] connectionCallbackCalled = {false};
            final boolean[] discoveryCallbackCalled = {false};
            final Handler uiHandler = getUiHandler();
            final Runnable explicitConnectionCheck = new Runnable() {
                @Override
                public void run() {
                    if (!connectionCallbackCalled[0]) {
                        //device.disconnect();
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onTimeout(context.getString(R.string.bluetooth_connection_failed));
                            }
                        });
                    }
                }
            };
            final Runnable explicitDiscoveryCheck = new Runnable() {
                @Override
                public void run() {
                    if (!discoveryCallbackCalled[0]) {
                        //device.disconnect();
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onTimeout(context.getString(R.string.bluetooth_service_discovery_failed));
                            }
                        });
                    }
                }
            };
            device.connect(false, gatt, new OnConnectionChangedListener() {
                @Override
                public void call(final BlueteethDevice thisDevice, final boolean isConnected) {
                    Log.d(TAG, "Connected to " + thisDevice.getName() + ": " + isConnected);
                    connChangedListener.call(thisDevice, isConnected);
                    if (isConnected) {
                        uiHandler.removeCallbacks(explicitConnectionCheck);
                        connectionCallbackCalled[0] = true;
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onGateConnection(thisDevice, true);
                            }
                        });
                        thisDevice.discoverServices(new OnServicesDiscoveredListener() {
                            @Override
                            public void call(BlueteethDevice.BlueteethResponse response) {
                                uiHandler.removeCallbacks(explicitDiscoveryCheck);
                                discoveryCallbackCalled[0] = true;

                                if (response == BlueteethDevice.BlueteethResponse.NO_ERROR) {
                                    Log.d(TAG, "Services discovered.");
                                    uiHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            listener.onServicesDiscovered(thisDevice);
                                        }
                                    });
                                } else {
                                    uiHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            listener.onError(context.getString(R.string.bluetooth_service_discovery_failed));
                                        }
                                    });
                                }
                            }
                        });
                        uiHandler.postDelayed(explicitDiscoveryCheck, Prefs.getInstance().getBleOptions().getConnectionTimeout());
                    } else {
                        connectionCallbackCalled[0] = true;
                        uiHandler.removeCallbacks(explicitConnectionCheck);
                        discoveryCallbackCalled[0] = true;
                        uiHandler.removeCallbacks(explicitDiscoveryCheck);
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onGateConnection(thisDevice, false);
                            }
                        });
                    }
                }
            });
            uiHandler.postDelayed(explicitConnectionCheck, Prefs.getInstance().getBleOptions().getConnectionTimeout());
        } else getUiHandler().post(new Runnable() {
            @Override
            public void run() {
                listener.onError(context.getString(R.string.bluetooth_connection_failed));
            }
        });
    }

    public static void authenticate(
            @NonNull final Context context,
            final BlueteethDevice device,
            final int userId,
            @NonNull final UUID deviceUUID,
            @NonNull final AuthListener listener) {
        if (okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            traceGateResponse(context, device, new GateStatusListener() {
                @Override
                public void onStatusReceived(Enums.GateStatus status) {
                    listener.onAuthResponse(status);
                }

                @Override
                public void onError(String errorMsg) {
                    listener.onError(errorMsg);
                }

                @Override
                public void onTimeout(String errorMsg) {
                    listener.onTimeout(errorMsg);
                }
            });
            byte[] bytesToWrite = new byte[20];
            byte[] uuidArray = BluetoothUtils.uuidToBytes(deviceUUID, false);
            byte[] userIdArray = BluetoothUtils.intToBytes(userId, true);
            System.arraycopy(uuidArray, 0, bytesToWrite, 0, uuidArray.length);
            System.arraycopy(userIdArray, 0, bytesToWrite, uuidArray.length, userIdArray.length);
            GattCharacteristicWriteOperation uuidOp = GattOperation.createWriteCharacteristic(
                    device,
                    bytesToWrite,
                    BluetoothServices.BarrierControl.SERVICE_UUID,
                    BluetoothServices.BarrierControl.COMMAND,
                    BluetoothServices.BarrierControl.COMMAND_BYTES_COUNT,
                    true,
                    null);
            GattOperationBundle bundle = new GattOperationBundle(
                    new SimpleBundleListener(context, getUiHandler(), listener), uuidOp);
            device.queueBundle(bundle);
        }
    }

    public static void traceGateResponse(
            @NonNull final Context context,
            final BlueteethDevice device,
            @NonNull final GateStatusListener listener) {
        if (okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            final boolean[] callbackCalled = {false};
            final Handler uiHandler = getUiHandler();
            final Runnable explicitCheck = new Runnable() {
                @Override
                public void run() {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (!callbackCalled[0]) {
                                //checkGateResponse(context, device, listener);
                                listener.onTimeout(context.getString(R.string.bluetooth_operation_timeout));
                            }
                        }
                    });
                }
            };
            final GattSetNotificationOperation notifOp = GattOperation.createNotification(
                    device,
                    BluetoothServices.BarrierControl.SERVICE_UUID,
                    BluetoothServices.BarrierControl.RESPONSE,
                    true,
                    new GattCharacteristicReadCallback() {
                        @Override
                        public void onRead(byte[] characteristic) {
                            callbackCalled[0] = true;
                            uiHandler.removeCallbacks(explicitCheck);
                            final int statusCode = BluetoothUtils.bytesToIntReversed(characteristic);
                            final Enums.GateStatus status = Enums.GateStatus.getByCode(statusCode);
                            Log.d(TAG, "Gate status: " + status);
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onStatusReceived(status);
                                }
                            });
                            // remove subscription to notification
                            GattSetNotificationOperation notifOffOp = GattOperation.createNotification(
                                    device,
                                    BluetoothServices.BarrierControl.SERVICE_UUID,
                                    BluetoothServices.BarrierControl.RESPONSE,
                                    false,
                                    null);
                            GattOperationBundle notifOffBundle = new GattOperationBundle(
                                    new SimpleBundleListener(context, uiHandler, listener), notifOffOp);
                            device.queueBundle(notifOffBundle);
                        }

                        @Override
                        public void onTimeout() {
                            // processed via delayed runnable below
                            *//*uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onTimeout(context.getString(R.string.bluetooth_operation_timeout));
                                }
                            });*//*
                        }

                        @Override
                        public void onError(final String errMessage) {
                            callbackCalled[0] = true;
                            uiHandler.removeCallbacks(explicitCheck);
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onError(errMessage);
                                }
                            });
                        }
                    });

            GattOperationBundle notifBundle = new GattOperationBundle(new SimpleBundleListener(context, uiHandler, listener), notifOp);
            device.queueBundle(notifBundle);

            uiHandler.postDelayed(explicitCheck, Prefs.getInstance().getBleOptions().getGateStatusReadAfter());
        }
    }

    private static void checkGateResponse(
            @NonNull final Context context,
            BlueteethDevice device,
            @NonNull final GateStatusListener listener) {
        if (okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            GattCharacteristicReadOperation gateOp = GattOperation.createReadCharacteristic(
                    device,
                    BluetoothServices.BarrierControl.SERVICE_UUID,
                    BluetoothServices.BarrierControl.RESPONSE,
                    BluetoothServices.BarrierControl.RESPONSE_BYTES_COUNT,
                    new BaseReadListener() {
                        @Override
                        public void onRead(byte[] characteristic) {
                            final int statusCode = BluetoothUtils.bytesToIntReversed(characteristic);
                            final Enums.GateStatus status = Enums.GateStatus.getByCode(statusCode);
                            Log.d(TAG, "Gate status: " + status);
                            getUiHandler().post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onStatusReceived(status);
                                }
                            });
                        }
                    }
            );
            GattOperationBundle bundle = new GattOperationBundle(
                    new SimpleBundleListener(context, getUiHandler(), listener), gateOp);
            device.queueBundle(bundle);
        }
    }*/

    public interface SearchListener extends ErrorListener {
        void onSearchCompleted(BlueteethDevice device);
        void onDeviceSpotted(BlueteethDevice device);
        void onNoDeviceFound();
    }

    public interface ConnectionListener extends ErrorListener {
        void onServicesDiscovered(BlueteethDevice device);
        void onGateConnection(BlueteethDevice device, boolean isConnected);
    }
}