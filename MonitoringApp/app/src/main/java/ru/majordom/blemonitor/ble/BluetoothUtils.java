package ru.majordom.blemonitor.ble;


import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BluetoothUtils {
    private final static String TAG = BluetoothUtils.class.getSimpleName();
    private final static char[] HEX_ARRAY = "0123456789abcdef".toCharArray();
    private final static int BYTES_IN_INTEGER = 4;

    /*public static byte[] calculateUserHash(String userId) {
        // Create MD5 Hash
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        String stringToHash = userId + BluetoothConstants.HASH_SALT;
        digest.update(stringToHash.getBytes());
        return digest.digest();
    }*/

    public static byte[] intToBytes(int integer, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.allocate(BYTES_IN_INTEGER)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
                .putInt(integer);
        return bb.array();
    }

    public static byte[] longToBytes(long longValue, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.allocate(BYTES_IN_INTEGER * 2)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
                .putLong(longValue);
        return bb.array();
    }

    public static byte[] shortToBytes(short value, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.allocate(2)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
                .putShort(value);
        return bb.array();
    }

    public static byte[] intToSingleByteArray(int integer) {
        return new byte[] { (byte) integer };
    }

    public static int bytesToInt(byte[] bytes) {
        return bytesToInt(bytes, false);
    }

    public static int bytesToIntReversed(byte[] bytes) {
        return bytesToInt(bytes, true);
    }

    public static short bytesToShort(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        if (bb.position() < bb.limit()) {
            return bb.get();
        }
        return bb.getShort();
    }

    public static UUID bytesTosUuid(byte[] bytes, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.wrap(bytes)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(littleEndian ? secondLong : firstLong, littleEndian ? firstLong : secondLong);
    }

    public static byte[] uuidToBytes(UUID uuid, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        byte[] byteArray = bb.array();
        if (littleEndian) {
            reverseByteArray(byteArray);
        }
        return byteArray;
    }

    public static byte[] hexStringToBytes(String hexString) {
        int len = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i + 1), 16));
        }
        return data;
    }

    public static String bytesToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = convertByte(bytes[j]);
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String bytesToMacAddress(byte[] data) {
        if (data.length != 6) {
            Log.w(TAG, "MAC must be 6 bytes length, current length is: " + data.length);
            return null;
        }
        StringBuilder sb = new StringBuilder(18);
        for (byte b : data) {
            if (sb.length() > 0)
                sb.append(':');
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }

    public static byte[] macToBytes(String mac) {
        byte[] macAddressBytes = new byte[6];
        if (isEmptyOrNull(mac)) {
            return macAddressBytes;
        }
        String[] macAddressParts = mac.split("[:-]");
        if (macAddressParts.length != 6) {
            Log.e(TAG, "Wrong mac format: " + mac);
            return macAddressBytes;
        }
        // convert hex string to byte values
        for (int i=0; i<6; i++){
            Integer hex = Integer.parseInt(macAddressParts[i], 16);
            macAddressBytes[i] = hex.byteValue();
        }
        return macAddressBytes;
    }

    public static void reverseByteArray(byte[] data) {
        for (int i = 0; i < data.length / 2; i++) {
            byte temp = data[i];
            data[i] = data[data.length - i - 1];
            data[data.length - i - 1] = temp;
        }
    }

    public static String bytesToIpString(byte[] bytes) {
        return bytesToIpString(bytes, false);
    }

    public static String bytesToIpStringReversed(byte[] bytes) {
        return bytesToIpString(bytes, true);
    }

    public static byte[] ipStringToBytes(String ipString) {
        return ipStringToBytes(ipString, false);
    }

    public static byte[] ipStringToBytesReversed(String ipString) {
        return ipStringToBytes(ipString, true);
    }

    public static boolean isEmptyOrNull(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static Character byteToChar(byte b) {
        return (char) convertByte(b);
    }

    private static byte[] ipStringToBytes(String ipString, boolean reverse) {
        try {
            InetAddress ip = InetAddress.getByName(ipString);
            byte[] address = ip.getAddress();
            if (reverse) {
                reverseByteArray(address);
            }
            return address;
        } catch (UnknownHostException e) {
            Log.e(TAG, e.toString());
        }
        return new byte[4];
    }

    private static String bytesToIpString(byte[] bytes, boolean reversed) {
        if (reversed) {
            reverseByteArray(bytes);
        }
        try {
            return InetAddress.getByAddress(bytes).getHostAddress();
        } catch (UnknownHostException e) {
            Log.e(TAG, e.toString());
        }
        return "";
    }

    private static int bytesToInt(byte[] bytes, boolean reversed) {
        if (bytes.length == 1) {
            return convertByte(bytes[0]);
        }
        if (reversed) {
            reverseByteArray(bytes);
        }
        ByteBuffer bb;
        if (bytes.length == BYTES_IN_INTEGER) {
            bb = ByteBuffer.wrap(bytes);
        } else {
            bb = ByteBuffer.allocate(BYTES_IN_INTEGER).order(ByteOrder.LITTLE_ENDIAN);
            for (int i = 0; i < BYTES_IN_INTEGER; i++) {
                bb.put(i < bytes.length ? bytes[i] : (byte) 0);
            }
            bb.position(0);
        }
        return bb.getInt();
    }

    /**
     * Converts byte to int using '& 0xFF'
     * @param b
     * @return
     */
    private static int convertByte(byte b) {
        return b & 0xFF;
    }

    public static Map<Integer, String> parseScanRecord(byte[] scanRecord) {
        Map<Integer, String> ret = new HashMap<>();
        if (scanRecord != null) {
            int index = 0;
            while (index < scanRecord.length) {
                int length = scanRecord[index++];
                //Zero value indicates that we are done with the record now
                if (length == 0) break;

                int type = scanRecord[index];
                //if the type is zero, then we are pass the significant section of the data,
                // and we are thud done
                if (type == 0) break;

                byte[] data = Arrays.copyOfRange(scanRecord, index + 1, index + length);
                if (data.length > 0) {
                    StringBuilder hex = new StringBuilder(data.length * 2);
                    // the data appears to be there backwards
                    for (int bb = data.length - 1; bb >= 0; bb--) {
                        hex.append(String.format("%02X", data[bb]));
                    }
                    ret.put(type, hex.toString());
                }
                index += length;
            }
        }

        return ret;
    }
}