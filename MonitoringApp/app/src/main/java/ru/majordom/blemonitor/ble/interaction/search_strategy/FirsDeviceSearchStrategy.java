package ru.majordom.blemonitor.ble.interaction.search_strategy;

import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethManager;

public class FirsDeviceSearchStrategy extends BaseSearchStrategy {

    public FirsDeviceSearchStrategy(BlueteethManager manager) {
        super(manager);
    }

    @Override
    protected BlueteethDevice compareWhichIsBetter(BlueteethDevice selectedDevice, BlueteethDevice currentDevice) {
        return currentDevice != null ? currentDevice : selectedDevice;
    }

    @Override
    public boolean isSingleDeviceScan() {
        return true;
    }
}