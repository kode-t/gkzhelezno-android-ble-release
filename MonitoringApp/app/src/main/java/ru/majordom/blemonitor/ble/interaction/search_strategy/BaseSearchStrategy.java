package ru.majordom.blemonitor.ble.interaction.search_strategy;

import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import ru.majordom.blemonitor.ble.BluetoothConstants;
import ru.majordom.blemonitor.ble.BluetoothUtils;
import ru.majordom.blemonitor.ble.common.ScanRecord;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BaseSearchStrategy implements SearchStrategy {

    protected final String TAG;
    protected BlueteethManager manager;
    protected final List<BlueteethDevice> suitableDevices = new ArrayList<>();

    protected BaseSearchStrategy(BlueteethManager manager) {
        TAG = this.getClass().getSimpleName();
        this.manager = manager;
    }

    @Override
    public boolean checkDeviceFound(@NonNull BlueteethDevice device) {
        String devName = device.getName();
        boolean isNameMatches = !BluetoothUtils.isEmptyOrNull(devName) && devName.toUpperCase().contains(BluetoothConstants.GATE_GENERAL_NAME);
        if (isNameMatches) {
            ScanRecord scanRecord = ScanRecord.parseFromBytes(device.getScanRecord());
            boolean isCorrectDevice = false;
            boolean hasProperService = false;
            if (scanRecord != null) {
                for (ParcelUuid service : scanRecord.getServiceUuids()) {
                    hasProperService = service.toString().toUpperCase().contains(BluetoothConstants.GATE_ADVERTISING_MARKER);
                    if (hasProperService) {
                        isCorrectDevice = true;
                        break;
                    }
                }
            } else {
                isCorrectDevice = true; // no scan record
            }
            if (isCorrectDevice) {
                Log.d(TAG, "Found device with name = " + device.getName() +
                        ", RSSI = " + device.getRssi() +
                        ", has advertising marker: " + hasProperService);
                suitableDevices.add(device);
                return true;
            }
        }
        // we don't need it anymore
        manager.removePeripheral(device);
        return false;

    }

    @Nullable
    @Override
    public BlueteethDevice pickTheMostSuitable() {
        Log.d(TAG, "Found devices: " + suitableDevices.size());
        BlueteethDevice chosenDevice = null;
        synchronized (suitableDevices) {
            for (BlueteethDevice dev : suitableDevices) {
                chosenDevice = compareWhichIsBetter(chosenDevice, dev);
            }
        }
        return chosenDevice;
    }

    @Override
    public int getDefaultScanTimeout() {
        return BluetoothConstants.getScanTimeout();
    }


    @Override
    public void release() {
        this.manager = null;
        suitableDevices.clear();
    }

    List<String> getNameParts(@NonNull String deviceName) {
        String[] nameParts = deviceName.split(BluetoothConstants.GATE_DEVICE_NUM_PREFIX);
        List<String> parts = Arrays.asList(nameParts);
        return parts;
    }

    public int getDeviceNumber(@NonNull String deviceName) {
        int number = 0;
        List<String> nameParts = getNameParts(deviceName);
        if (nameParts.size() > BluetoothConstants.GATE_ORDER_PLACE_IN_NAME) {
            try {
                number = Integer.parseInt(nameParts.get(BluetoothConstants.GATE_ORDER_PLACE_IN_NAME));
            } catch (NumberFormatException e) {
                Log.e(TAG, "Failed to get gate number from name = " + deviceName);
            }
        }
        /*String stringedNum = deviceName.replace(BluetoothConstants.GATE_GENERAL_NAME, "");
        if (stringedNum.length() > 0) {
            number = Integer.MAX_VALUE;
            stringedNum = stringedNum.replace(BluetoothConstants.GATE_DEVICE_NUM_PREFIX, "");
            if (stringedNum.length() > 0) {
                try {
                    number = Integer.parseInt(stringedNum);
                } catch (NumberFormatException e) {
                    Log.e("BaseSearchStrategy", "Failed to get gate number from name = " + deviceName);
                }
            }
        }
        Log.d("BaseSearchStrategy", "Got gate number = " + String.valueOf(number) + " from name = " + deviceName);*/
        return number;
    }

    protected abstract BlueteethDevice compareWhichIsBetter(
            BlueteethDevice selectedDevice,
            BlueteethDevice currentDevice);
}