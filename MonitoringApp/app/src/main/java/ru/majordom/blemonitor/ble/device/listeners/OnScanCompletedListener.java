package ru.majordom.blemonitor.ble.device.listeners;

import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

import java.util.List;

public interface OnScanCompletedListener {
    void call(List<BlueteethDevice> blueteethDevices);
}
