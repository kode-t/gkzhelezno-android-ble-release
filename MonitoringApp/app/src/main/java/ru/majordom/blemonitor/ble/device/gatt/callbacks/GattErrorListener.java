package ru.majordom.blemonitor.ble.device.gatt.callbacks;


public interface GattErrorListener {
    void onTimeout();
    void onError(String errMessage);
}
