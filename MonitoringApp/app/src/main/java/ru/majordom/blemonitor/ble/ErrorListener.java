package ru.majordom.blemonitor.ble;


public interface ErrorListener {
    void onError(String errorMsg);
}
