package ru.majordom.blemonitor.ble.device.gatt.operations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.util.Log;

import ru.majordom.blemonitor.ble.device.gatt.callbacks.GattCharacteristicReadCallback;
import ru.majordom.blemonitor.ble.device.interaction.BaseGatt;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

import java.util.UUID;

public class GattSetNotificationOperation extends GattOperation {
    public final static UUID NTF_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private boolean enable;
    private GattCharacteristicReadCallback callback;

    GattSetNotificationOperation(BlueteethDevice device, UUID service, UUID characteristic, boolean enable, GattCharacteristicReadCallback callback) {
        super(device, service, characteristic);
        this.enable = enable;
        hasAvailableCompletionCallback = true;
        this.callback = callback;
    }

    @Override
    public void execute() {
        BaseGatt gatt = device.getBluetoothGatt();
        if (gatt != null) {
            BluetoothGattCharacteristic gattCharacteristic = getCharacteristic(gatt);
            if (gattCharacteristic != null) {
                Log.d(TAG, "Adding notification to " + characteristic);
                gatt.setCharacteristicNotification(gattCharacteristic, enable);
                try {
                    Thread.sleep(ONE_SECOND_DELAY / 10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                BluetoothGattDescriptor descriptor = gattCharacteristic.getDescriptor(NTF_DESCRIPTOR);
                if (descriptor != null) {
                    descriptor.setValue(enable ?
                            BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE :
                            BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                    gatt.writeDescriptor(descriptor);
                } else {
                    Log.w(TAG, "No notification descriptor found.");
                }
            } else {
                Log.w(TAG, "No characteristic " + characteristic + " found.");
            }
        } else {
            Log.w(TAG, "Null gatt.");
        }
        super.onOperationCompleted();
    }

    public GattCharacteristicReadCallback getCallback() {
        return callback;
    }
}