package ru.majordom.blemonitor.ble.device.gatt.callbacks;


public abstract class BaseBundleListener implements GattOperationBundleCallback {
    @Override
    public void onAllOperationsDone() {
        //
    }

    @Override
    public void onTimeout() {
        //
    }

    @Override
    public void onError(String errMessage) {
        //
    }
}
