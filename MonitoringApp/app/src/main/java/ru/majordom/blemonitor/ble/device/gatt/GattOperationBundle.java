package ru.majordom.blemonitor.ble.device.gatt;


import ru.majordom.blemonitor.ble.device.gatt.callbacks.GattOperationBundleCallback;

import java.util.ArrayList;

import ru.majordom.blemonitor.ble.device.gatt.callbacks.GattErrorListener;
import ru.majordom.blemonitor.ble.device.gatt.operations.GattOperation;

public class GattOperationBundle implements GattErrorListener {
    final ArrayList<GattOperation> operations; // strictly ordered list, because order MATTERS!
    private GattOperationBundleCallback callback;

    public GattOperationBundle(GattOperationBundleCallback callback) {
        operations = new ArrayList<>();
        this.callback = callback;
    }

    /**
     *
     * @param callback
     * @param gattOperations Operations to execute on BLE. Warning, ORDER MATTERS!
     */
    public GattOperationBundle(GattOperationBundleCallback callback, GattOperation... gattOperations) {
        this(callback);
        for (GattOperation op : gattOperations) {
            addOperation(op);
        }
    }

    /**
     * Add operations to the BLE queue.
     * @param gattOperations Operations to execute on BLE. Warning, ORDER MATTERS!
     */
    public void addOperations(GattOperation... gattOperations) {
        for (GattOperation op : gattOperations) {
            addOperation(op);
        }
    }

    /**
     * Add operations to the BLE queue.
     * @param delay delay to apply on each operation
     * @param gattOperations Operations to execute on BLE. Warning, ORDER MATTERS!
     */
    public void addOperations(int delay, GattOperation... gattOperations) {
        for (GattOperation op : gattOperations) {
            op.setDelay(delay);
            addOperation(op);
        }
    }

    private void addOperation(GattOperation operation) {
        if (operation != null) {
            operations.add(operation);
            operation.setBundle(this);
        }
    }

    public ArrayList<GattOperation> getOperations() {
        return new ArrayList<>(operations);
    }

    public void removeOperation(GattOperation operation) {
        boolean removed = operations.remove(operation);
        if (removed) {
            operation.setBundle(null);
            if (callback != null && operations.size() == 0) {
                callback.onAllOperationsDone();
            }
        }
    }

    @Override
    public String toString() {
        return "GattOperationBundle{" +
                "operations=" + operations.size() +
                '}';
    }

    @Override
    public void onTimeout() {
        if (callback != null) {
            callback.onTimeout();
        }
    }

    @Override
    public void onError(String errMessage) {
        if (callback != null) {
            callback.onError(errMessage);
        }
    }
}