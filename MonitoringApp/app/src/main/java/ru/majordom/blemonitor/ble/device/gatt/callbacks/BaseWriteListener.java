package ru.majordom.blemonitor.ble.device.gatt.callbacks;


public abstract class BaseWriteListener implements GattCharacteristicWriteCallback {
    @Override
    public void onWrite() {
        //
    }

    @Override
    public void onTimeout() {
        //
    }

    @Override
    public void onError(String errMessage) {
        //
    }
}
