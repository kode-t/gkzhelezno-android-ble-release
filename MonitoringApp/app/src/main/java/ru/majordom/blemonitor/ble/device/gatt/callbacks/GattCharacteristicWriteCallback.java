package ru.majordom.blemonitor.ble.device.gatt.callbacks;

public interface GattCharacteristicWriteCallback extends GattErrorListener {
    void onWrite();
}
