package ru.majordom.blemonitor.ble.device.listeners;

public interface OnAdvertisingListener {
    void onSuccessStart();
    void onStop();
    void onError(String errorMsg);
}
