package ru.majordom.blemonitor.ble.device.gatt.callbacks;


public abstract class BaseReadListener implements GattCharacteristicReadCallback {
    @Override
    public void onRead(byte[] characteristic) {
        //
    }

    @Override
    public void onTimeout() {
        //
    }

    @Override
    public void onError(String errMessage) {
        //
    }
}
