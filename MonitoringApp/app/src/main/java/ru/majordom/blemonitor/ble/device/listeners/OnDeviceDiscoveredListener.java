package ru.majordom.blemonitor.ble.device.listeners;

import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

public interface OnDeviceDiscoveredListener {
    void call(BlueteethDevice blueteethDevice);
}
