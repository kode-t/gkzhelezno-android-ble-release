package ru.majordom.blemonitor.ble.device.interaction;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import java.util.UUID;

public class RealGatt extends BaseGatt {

    private static final String TAG = RealGatt.class.getSimpleName();

    @Override
    public boolean readRemoteRssi() {
        if (gatt == null) {
            Log.w(TAG, "readRemoteRssi: Gatt is null");
            return false;
        }
        return gatt.readRemoteRssi();
    }

    @Override
    public void disconnect() {
        if (gatt == null) {
            Log.w(TAG, "disconnect: Gatt is null");
            return;
        }
        gatt.disconnect();
    }

    @Override
    public void close() {
        if (gatt == null) {
            Log.w(TAG, "close: Gatt is null");
            return;
        }
        gatt.close();
    }

    @Override
    public boolean discoverServices() {
        if (gatt == null) {
            Log.w(TAG, "discoverServices: Gatt is null");
            return false;
        }
        return gatt.discoverServices();
    }

    @Override
    public BluetoothGattService getService(UUID serviceUuid) {
        if (gatt == null) {
            Log.w(TAG, "getService: Gatt is null");
            return null;
        }
        return gatt.getService(serviceUuid);
    }

    @Override
    public boolean readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (gatt == null) {
            Log.w(TAG, "readCharacteristic: Gatt is null");
            return false;
        }
        return gatt.readCharacteristic(characteristic);
    }

    @Override
    public boolean writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (gatt == null) {
            Log.w(TAG, "writeCharacteristic: Gatt is null");
            return false;
        }
        return gatt.writeCharacteristic(characteristic);
    }

    @Override
    public boolean readDescriptor(BluetoothGattDescriptor descriptor) {
        if (gatt == null) {
            Log.w(TAG, "readDescriptor: Gatt is null");
            return false;
        }
        return gatt.readDescriptor(descriptor);
    }

    @Override
    public boolean writeDescriptor(BluetoothGattDescriptor descriptor) {
        if (gatt == null) {
            Log.w(TAG, "writeDescriptor: Gatt is null");
            return false;
        }
        return gatt.writeDescriptor(descriptor);
    }

    @Override
    public boolean setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enable) {
        if (gatt == null) {
            Log.w(TAG, "setCharacteristicNotification: Gatt is null");
            return false;
        }
        return gatt.setCharacteristicNotification(characteristic, enable);
    }
}