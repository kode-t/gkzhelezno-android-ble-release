package ru.majordom.blemonitor.ble.device.models;

public class RssiModel {

    private int currentRssi;
    private int stashedRssi;
    private int timesRssiRead;

    public RssiModel(int currentRssi, int stashedRssi, int timesRssiRead) {
        this.currentRssi = currentRssi;
        this.stashedRssi = stashedRssi;
        this.timesRssiRead = timesRssiRead;
    }

    public int getCurrentRssi() {
        return currentRssi;
    }

    public int getStashedRssi() {
        return stashedRssi;
    }

    public int getTimesRssiRead() {
        return timesRssiRead;
    }
}
