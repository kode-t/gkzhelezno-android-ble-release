package ru.majordom.blemonitor.ble.interaction;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import ru.majordom.blemonitor.ble.ErrorListener;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

public abstract class BaseModule {
    private static Handler uiHandler;

    /**
     * Explicitly sets UI handler that is shared between all modules. Used for testing purposes.
     * @param handler handler
     */
    public static void setUiHandler(Handler handler) {
        uiHandler = handler;
    }

    /**
     * Gets (and creates, if not created yet) UI handler.
     * @return singleton handler instance.
     */
    static Handler getUiHandler() {
        if (uiHandler == null) {
            uiHandler =  new Handler(Looper.getMainLooper());
        }
        return uiHandler;
    }

    static boolean deviceIsReady(BlueteethDevice device) {
        return device != null && device.isConnected();
    }

    static boolean okToProceed(BlueteethDevice device, @NonNull final String errorMsg, @NonNull final ErrorListener listener) {
        if (deviceIsReady(device)) {
            return true;
        } else {
            getUiHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        listener.onError(errorMsg);
                    }
                });
        }
        return false;
    }
}