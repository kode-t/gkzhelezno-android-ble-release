package ru.majordom.blemonitor.ble.device.listeners;


import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

public interface OnServicesDiscoveredListener {
    void call(BlueteethDevice.BlueteethResponse response);
}
