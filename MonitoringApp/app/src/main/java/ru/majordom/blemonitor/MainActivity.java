package ru.majordom.blemonitor;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.majordom.blemonitor.ble.device.interaction.BaseBleAdapter;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;
import ru.majordom.blemonitor.ble.device.interaction.BlueteethManager;
import ru.majordom.blemonitor.ble.device.interaction.RealBleAdapter;
import ru.majordom.blemonitor.ble.interaction.DeviceModule;
import ru.majordom.blemonitor.ble.interaction.search_strategy.FirsDeviceSearchStrategy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;

import java.util.Locale;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
@EActivity(ru.majordom.blemonitor.R.layout.activity_main)
@OptionsMenu(ru.majordom.blemonitor.R.menu.main_menu)
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PAUSE = 500;

    @ViewById(ru.majordom.blemonitor.R.id.layout_main) ViewGroup mainLayout;
    @ViewById(ru.majordom.blemonitor.R.id.text_device) TextView deviceView;
    @OptionsMenuItem(ru.majordom.blemonitor.R.id.item_scan) MenuItem scanItem;

    private BlueteethManager bleManager;
    private Handler handler;
    private boolean isScanStarted;
    private boolean isConnectionRunnableAdded = false;
    private Runnable connectionRunner = new Runnable() {
        @Override
        public void run() {
            if (isScanStarted && !isConnectionRunnableAdded) {
                isConnectionRunnableAdded = true;
                Log.d(TAG, "Connection runner in process.");
                DeviceModule.searchForGate(
                        MainActivity.this,
                        bleManager,
                        new FirsDeviceSearchStrategy(bleManager),
                        new DeviceModule.SearchListener() {
                            private boolean isAlive = true;

                            @Override
                            public void onSearchCompleted(BlueteethDevice device) {
                                if (isAlive) {
                                    isAlive = false;
                                    mainLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, android.R.color.holo_green_light));
                                    deviceView.setText(String.format(Locale.getDefault(), "%1$s, RSSI %2$d", device.getName(), device.getRssi()));
                                    bleManager.removePeripheral(device);

                                    if (isScanStarted) {
                                        isConnectionRunnableAdded = false;
                                        handler.postDelayed(connectionRunner, PAUSE);
                                    }
                                }
                            }

                            @Override
                            public void onDeviceSpotted(BlueteethDevice device) {
                                mainLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, android.R.color.holo_green_light));
                                deviceView.setText(String.format(Locale.getDefault(), "%1$s, RSSI %2$d", device.getName(), device.getRssi()));
                            }

                            @Override
                            public void onNoDeviceFound() {
                                if (isAlive) {
                                    isAlive = false;
                                    mainLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this, android.R.color.holo_red_light));
                                    deviceView.setText(ru.majordom.blemonitor.R.string.alert_no_devices_found);
                                    onError("");
                                }
                            }

                            @Override
                            public void onError(String errorMsg) {
                                //bleManager.undiscoverAll();
                                if (isScanStarted) {
                                    isConnectionRunnableAdded = false;
                                    handler.postDelayed(connectionRunner, PAUSE);
                                }
                            }
                        });
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseBleAdapter adapter = new RealBleAdapter(BluetoothAdapter.getDefaultAdapter());
        bleManager = BlueteethManager.with(this, adapter, new Handler(Looper.getMainLooper()));
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    protected void onResume() {
        super.onResume();
        //bleManager.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //bleManager.onPause();
    }

    @AfterViews
    void afterViews() {
        //
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MainActivityPermissionsDispatcher.startScanWithPermissionCheck(this);
        return super.onCreateOptionsMenu(menu);
    }

    @NeedsPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    void startScan() {
        if (!bleManager.isBleEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();
        }
        isScanStarted = true;
        handler.post(connectionRunner);
        scanItem.setTitle(ru.majordom.blemonitor.R.string.scan_stop);
    }

    @OptionsItem(ru.majordom.blemonitor.R.id.item_scan)
    void onItemScanClick() {
        if (isScanStarted) {
            stopScan();
            mainLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
            deviceView.setText(ru.majordom.blemonitor.R.string.scan_no);
            scanItem.setTitle(ru.majordom.blemonitor.R.string.scan_start);
        } else {
            MainActivityPermissionsDispatcher.startScanWithPermissionCheck(this);
        }
    }

    @OptionsItem(ru.majordom.blemonitor.R.id.item_exit)
    void onExitClick() {
        this.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        stopScan();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        stopScan();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void stopScan() {
        isScanStarted = false;
        bleManager.stopScanForPeripherals();
        handler.removeCallbacksAndMessages(null);
    }
}