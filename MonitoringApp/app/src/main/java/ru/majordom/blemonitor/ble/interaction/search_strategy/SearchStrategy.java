package ru.majordom.blemonitor.ble.interaction.search_strategy;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ru.majordom.blemonitor.ble.device.interaction.BlueteethDevice;

public interface SearchStrategy {
    boolean checkDeviceFound(@NonNull BlueteethDevice device);
    @Nullable BlueteethDevice pickTheMostSuitable();
    int getDefaultScanTimeout();
    boolean isSingleDeviceScan();
    void release();
}