package ru.majordom.blemonitor.ble.device.interaction;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanSettings;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import ru.majordom.blemonitor.ble.BluetoothConstants;

public class RealBleAdapter extends BaseBleAdapter {
    private BluetoothAdapter adapter;

    private static final String TAG = RealBleAdapter.class.getSimpleName();

    public RealBleAdapter(BluetoothAdapter adapter) {
        this.adapter = adapter;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean startLeScan(BluetoothAdapter.LeScanCallback callback) {
        return adapter.startLeScan(callback);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void stopLeScan(BluetoothAdapter.LeScanCallback callback) {
        adapter.stopLeScan(callback);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void startScan(ScanCallback callback) {
        BluetoothLeScanner scanner = adapter.getBluetoothLeScanner();
        if (scanner != null) {
            // TODO: 6/27/2017 move to the upper levels to avoid coherence
            int scanMode;
            switch (BluetoothConstants.getScanMode()) {
                case BluetoothConstants.SCAN_POWER_LOW: {
                    scanMode = ScanSettings.SCAN_MODE_LOW_POWER;
                    break;
                }
                case BluetoothConstants.SCAN_POWER_MID: {
                    scanMode = ScanSettings.SCAN_MODE_BALANCED;
                    break;
                }
                case BluetoothConstants.SCAN_POWER_HIGH: {
                    scanMode = ScanSettings.SCAN_MODE_LOW_LATENCY;
                    break;
                }
                default: scanMode = ScanSettings.SCAN_MODE_LOW_LATENCY;
            }

            ScanSettings.Builder settings = new ScanSettings.Builder()
                    .setScanMode(scanMode)
                    .setReportDelay(0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                settings.setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES);
            }
            scanner.startScan(null, settings.build(), callback);
        } else {
            Log.w(TAG, "startScan: null LE scanner, BLE is off?");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void stopScan(ScanCallback callback) {
        BluetoothLeScanner scanner = adapter.getBluetoothLeScanner();
        if (scanner != null && callback != null) {
            scanner.stopScan(callback);
        } else {
            Log.w(TAG, "stopScan: null LE scanner or callback, BLE is off?");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void startAdvertising(@NonNull AdvertiseSettings settings, @NonNull AdvertiseData advertiseData, @NonNull AdvertiseCallback callback) {
        startAdvertising(settings, advertiseData, null, callback);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void startAdvertising(@NonNull AdvertiseSettings settings, @NonNull AdvertiseData advertiseData, @Nullable AdvertiseData scanResponse, @NonNull AdvertiseCallback callback) {
        BluetoothLeAdvertiser advertiser = adapter.getBluetoothLeAdvertiser();
        if (advertiser != null) {
            advertiser.startAdvertising(settings, advertiseData, scanResponse, callback);
        } else {
            Log.w(TAG, "startAdvertising: null LE advertiser, BLE is off?");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void stopAdvertising(@NonNull AdvertiseCallback callback) {
        BluetoothLeAdvertiser advertiser = adapter.getBluetoothLeAdvertiser();
        if (advertiser != null) {
            advertiser.stopAdvertising(callback);
        } else {
            Log.w(TAG, "stopAdvertising: null LE advertiser, BLE is off?");
        }
    }

    @Override
    public boolean canAdvertise() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                adapter.getBluetoothLeAdvertiser() != null
                // even if all clauses below are false, advertising may be supported for some reason
                /*adapter.isMultipleAdvertisementSupported() &&
                adapter.isOffloadedFilteringSupported() &&
                adapter.isOffloadedScanBatchingSupported()*/;
    }

    @Override
    public BlueteethDevice getRemoteDevice(BlueteethManager manager, String address) {
        return new BlueteethDevice(manager, adapter.getRemoteDevice(address));
    }

    @Override
    public boolean isEnabled() {
        return adapter.isEnabled();
    }
}
