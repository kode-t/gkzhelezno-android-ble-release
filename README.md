﻿
**Шаги интеграции библиотеки в уже существующий проект:**

 1. задать конфигурацию сервиса - см. *App.kt::initInfrastructure*

	Конфигурация состоит из:

 - *ServiceNotifications* - интерфейс вывода сервисных нотификаций. Пример
   реализации - *AppServiceNotificationsDelegate*
 - ZonesNotifications - интерфейс вывода нотификаций геозон (в данном
   случае она одна и отвечает за оповещение пользователя об
   автоматически включенном Bluetooth). Пример -
   *AppZonesNotificationsDelegate*
 - *ZonesProvider* - провайдер геозон (зон, где расположены контроллеры).
   Пример - *DumbZonesProvider*
 - *LoggerCreator* - интерфейс создания файлового логгера. Пример логгера
 - *FileLogger*. Аргумент опционален, при его отсутствии не будет логирования в файл.
 - *LocalStorage* - поставщик данных, пример - *Prefs*. С помощью поставщика
   данных можно задавать опции автоматического открытия, геозон
   (*ZonesOptions*) и тонко настраивать BLE-часть (*BleOptions*)

2. предоставить приложению разрешение на *ACCESS_FINE_LOCATION* (необходимо для BLE-сканирования и работы геозон)
3. предоставить приложению разрешение на *WRITE_EXTERNAL_STORAGE* в сочетании с выставлением *BleOptions.advancedDebug* = true для записи отладочных логов в файл.
4. в простейшем случае запускать и останавливать сервис соответствующими методами (см. *App::runService* и *App::stopService*)
5. в остальных случаях существует возможность тонко управлять состоянием сервиса посредством команд (см. компаньон *BleService*)
6. для доступа к менеджерам, представленныим в библиотеке сервиса, можно воспользоваться *InternalsFacade*: он отдает *BluetoothManagerContract* и *PowerManagerWrapper*.
При помощи первого можно отслеживать события включения-отключение Bluetooth, при помощи второго проверять, не включено ли энергосбережение.
7. Следует обратить внимание на отображение предупреждения о внесении приложения в список исключений адаптивного питания на кастомных проишвках *OpenGateActivity::checkCustomPhonePowerOptimizations*

Детально с процессом работы сервиса и настройки его параметров можно ознакомиться в тестовом приложении.



