package ru.majordom.bleservice.connectivity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager

class NetworkChangeReceiver internal constructor(private val listener: Listener) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == ConnectivityManager.CONNECTIVITY_ACTION) {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var activeNetwork = cm.activeNetworkInfo

            // see https://stackoverflow.com/a/45231746/6759572 and https://issuetracker.google.com/issues/37137911
            val intentNetworkInfo: NetworkInfo? =
                    intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO)
                            ?: intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO)

            activeNetwork = when (activeNetwork) {
                null -> intentNetworkInfo
                else -> {
                    //when low battery get ActiveNetwork might receive DISCONNECTED/BLOCKED but the intent data is actually CONNECTED/CONNECTED
                    if (intentNetworkInfo != null && activeNetwork.isConnectedOrConnecting != intentNetworkInfo.isConnectedOrConnecting) {
                        intentNetworkInfo
                    } else activeNetwork
                }
            }
            listener.onConnectivityChanged(activeNetwork != null && activeNetwork.isConnected)
        }
    }

    interface Listener {
        fun onConnectivityChanged(isConnected: Boolean)
    }
}

