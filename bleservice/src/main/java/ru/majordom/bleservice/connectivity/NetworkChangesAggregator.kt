package ru.majordom.bleservice.connectivity

interface NetworkChangesAggregator {
    val isConnected: Boolean
    fun addListener(listener: Listener)
    fun removeListener(listener: Listener)

    interface Listener {
        fun onConnectivityChanged(isConnected: Boolean)
    }
}