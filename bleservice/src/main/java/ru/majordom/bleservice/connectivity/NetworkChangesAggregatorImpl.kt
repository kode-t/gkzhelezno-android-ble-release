package ru.majordom.bleservice.connectivity

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import java.util.*

class NetworkChangesAggregatorImpl internal constructor(
        context: Context
) : NetworkChangesAggregator, NetworkChangeReceiver.Listener {

    private val connectivityManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private val listeners: HashSet<NetworkChangesAggregator.Listener> = HashSet(2, 1f)
    private var actualIsConnected: Boolean? = null

    override val isConnected: Boolean
        get() {
            return actualIsConnected ?: {
                val activeNetwork = connectivityManager.activeNetworkInfo
                activeNetwork != null && activeNetwork.isConnectedOrConnecting
            }.invoke()
        }

    init {
        val receiver = NetworkChangeReceiver(this)
        context.registerReceiver(receiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onConnectivityChanged(isConnected: Boolean) {
        actualIsConnected = isConnected
        for (listener in listeners) {
            listener.onConnectivityChanged(isConnected)
        }
    }

    override fun addListener(listener: NetworkChangesAggregator.Listener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: NetworkChangesAggregator.Listener) {
        listeners.remove(listener)
    }
}