package ru.majordom.bleservice

import ru.majordom.bleservice.common.LocalStorage
import ru.majordom.bleservice.common.LoggerCreator
import ru.majordom.bleservice.notifications.ZonesNotifications
import ru.majordom.bleservice.notifications.ServiceNotifications
import ru.majordom.bleservice.zones.ZonesProvider

class BleServiceConfiguration(
        internal val notificationsDelegate: ServiceNotifications,
        internal val geofenceNotificationsDelegate: ZonesNotifications,
        internal val zonesProvider: ZonesProvider,
        internal val loggerCreator: LoggerCreator?,
        internal val localStorage: LocalStorage
)
