package ru.majordom.bleservice.di

import android.content.Context
import org.koin.dsl.module.module
import ru.majordom.bleservice.BleServiceConfiguration
import ru.majordom.bleservice.bluetooth.BluetoothManager
import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.connectivity.NetworkChangesAggregator
import ru.majordom.bleservice.connectivity.NetworkChangesAggregatorImpl
import ru.majordom.bleservice.power.PowerManagerWrapper
import ru.majordom.bleservice.power.PowerManagerWrapperImpl
import ru.majordom.bleservice.zones.ZonesWatcher
import ru.majordom.bleservice.zones.ZonesWatcherImpl

internal fun buildServiceModule(
        context: Context,
        config: BleServiceConfiguration) = module {
    single { context }
    single { BluetoothManager(get()) as BluetoothManagerContract }
    single { PowerManagerWrapperImpl(get()) as PowerManagerWrapper }
    single { NetworkChangesAggregatorImpl(get()) as NetworkChangesAggregator }
    single { config }
    single {
        ZonesWatcherImpl(
                get(),
                get(),
                config.zonesProvider,
                config.localStorage.zonesOptions,
                config.geofenceNotificationsDelegate,
                get()
        ) as ZonesWatcher
    }
}