package ru.majordom.bleservice

import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.power.PowerManagerWrapper

object InternalsFacade {

    val bleManager: BluetoothManagerContract
        get() = BleServiceInitializer.koin.koinContext.get()

    val powerManager: PowerManagerWrapper
        get() = BleServiceInitializer.koin.koinContext.get()
}