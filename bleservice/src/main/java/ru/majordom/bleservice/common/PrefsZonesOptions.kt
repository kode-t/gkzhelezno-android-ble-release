package ru.majordom.bleservice.common

import android.content.SharedPreferences

class PrefsZonesOptions(private val prefs: SharedPreferences) : ZonesOptions {

    override var zoneRadiusInMeters: Int
        get() = prefs.getInt(KEY_ZONE_RADIUS_IN_METERS, ZONE_RADIUS_IN_METERS)
        set(value) = prefs.edit().putInt(KEY_ZONE_RADIUS_IN_METERS, value).apply()

    override var zonesToRequest: Int
        get() = prefs.getInt(KEY_REQUEST_ZONES_IN_RADIUS, REQUEST_ZONES_IN_RADIUS)
        set(value) = prefs.edit().putInt(KEY_REQUEST_ZONES_IN_RADIUS, value).apply()

    override var requestZonesInRadius: Int
        get() = prefs.getInt(KEY_ZONES_TO_REQUEST, ZONES_TO_REQUEST)
        set(value) = prefs.edit().putInt(KEY_ZONES_TO_REQUEST, value).apply()

    override var zoneRadiusLagInMeters: Float
        get() = prefs.getFloat(KEY_DISTANCE_LAG_IN_METERS, DISTANCE_LAG_IN_METERS)
        set(value) = prefs.edit().putFloat(KEY_DISTANCE_LAG_IN_METERS, value).apply()

    override fun resetToDefaults() {
        zoneRadiusInMeters = ZONE_RADIUS_IN_METERS
        zonesToRequest = ZONES_TO_REQUEST
        requestZonesInRadius = REQUEST_ZONES_IN_RADIUS
    }

    private companion object {
        private const val ZONE_RADIUS_IN_METERS = 50
        private const val REQUEST_ZONES_IN_RADIUS = 500
        private const val ZONES_TO_REQUEST = 10
        private const val DISTANCE_LAG_IN_METERS = 10f

        private const val KEY_ZONE_RADIUS_IN_METERS = "key.zone.radius.in.meters"
        private const val KEY_REQUEST_ZONES_IN_RADIUS = "key.request.zones.in.radius"
        private const val KEY_ZONES_TO_REQUEST = "key.zones.to.request"
        private const val KEY_DISTANCE_LAG_IN_METERS = "key.distance.lag.in.meters"
    }
}