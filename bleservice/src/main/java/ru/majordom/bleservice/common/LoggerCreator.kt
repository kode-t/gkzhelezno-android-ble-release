package ru.majordom.bleservice.common

import android.content.Context
import java.io.File

interface LoggerCreator {
    fun createFileLogger(
            context: Context,
            filesDir: File,
            fileName: String,
            append: Boolean): Logger
}