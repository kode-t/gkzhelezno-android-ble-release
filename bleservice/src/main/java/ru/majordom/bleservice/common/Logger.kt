package ru.majordom.bleservice.common

import android.net.Uri
import java.io.Closeable
import java.io.IOException

interface Logger : Closeable {
    val contentUri: Uri?

    @Throws(IOException::class)
    fun log(data: String)

    @Throws(IOException::class)
    fun flush()
}