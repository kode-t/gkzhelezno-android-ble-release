package ru.majordom.bleservice.common

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.support.annotation.IntRange
import ru.majordom.bleservice.bluetooth.BluetoothConstants
import ru.majordom.bleservice.bluetooth.BluetoothConstants.*

class PrefsBleOptions(private val prefs: SharedPreferences) : BleOptions {

    init {
        this.init()
    }

    override var minRssiLevel: Int
        get() = BluetoothConstants.getMinRssiLevel()
        set(level) {
            prefs.edit().putInt(MIN_RSSI_LEVEL, level).apply()
            BluetoothConstants.setMinRssiLevel(level)
        }

    override var connectionTimeout: Long
        get() = BluetoothConstants.getConnectionTimeout()
        set(timeout) {
            prefs.edit().putLong(CONNECTION_TIMEOUT, timeout).apply()
            BluetoothConstants.setConnectionTimeout(timeout)
        }

    override var discoveringTimeout: Long
        get() = BluetoothConstants.getDiscoveringTimeout()
        set(timeout) {
            prefs.edit().putLong(TIMEOUT_DISCOVERING, timeout).apply()
            BluetoothConstants.setDiscoveringTimeout(timeout)
        }

    override var gateStatusReadAfter: Long
        get() = BluetoothConstants.getGateStatusReadAfter()
        set(timeout) {
            prefs.edit().putLong(GATE_STATUS_READ_AFTER, timeout).apply()
            BluetoothConstants.setGateStatusReadAfter(timeout)
        }

    override var scanTimeout: Long
        get() = BluetoothConstants.getScanTimeout()
        set(timeout) {
            prefs.edit().putLong(SCAN_TIMEOUT, timeout).apply()
            BluetoothConstants.setScanTimeout(timeout)
        }

    override var pauseDefault: Long
        get() = BluetoothConstants.getPauseDefault()
        set(pause) {
            prefs.edit().putLong(PAUSE_DEFAULT, pause).apply()
            BluetoothConstants.setPauseDefault(pause)
        }

    override var pauseConnAuthError: Long
        get() = BluetoothConstants.getPauseConnAuthError()
        set(pause) {
            prefs.edit().putLong(PAUSE_CONN_AUTH, pause).apply()
            BluetoothConstants.setPauseConnAuthError(pause)
        }

    override var pauseTech: Long
        get() = BluetoothConstants.getPauseAfterTechnicalIssue()
        set(pause) {
            prefs.edit().putLong(PAUSE_TECH, pause).apply()
            BluetoothConstants.setPauseAfterTechnicalIssue(pause)
        }

    override var pauseNetwork: Long
        get() = BluetoothConstants.getPauseAfterNetwork()
        set(pause) {
            prefs.edit().putLong(PAUSE_NETWORK, pause).apply()
            BluetoothConstants.setPauseAfterNetwork(pause)
        }

    override var scanMode: Int
        get() = BluetoothConstants.getScanMode()
        @SuppressLint("Range")
        set(@IntRange(from = SCAN_POWER_LOW.toLong(), to = SCAN_POWER_HIGH.toLong()) scanMode) {
            if (scanMode in SCAN_POWER_LOW..SCAN_POWER_HIGH) {
                prefs.edit().putInt(SCAN_MODE, scanMode).apply()
                BluetoothConstants.setScanMode(scanMode)
            }
        }

    override var connPriority: Int
        get() = BluetoothConstants.getConnPriority()
        @SuppressLint("Range")
        set(@IntRange(from = CONN_PRIORITY_LOW.toLong(), to = CONN_PRIORITY_HIGH.toLong()) priority) {
            if (scanMode in CONN_PRIORITY_LOW..CONN_PRIORITY_HIGH) {
                prefs.edit().putInt(CONN_PRIORITY, priority).apply()
                BluetoothConstants.setConnPriority(priority)
            }
        }

    override var distanceLimit: Int
        get() = BluetoothConstants.getDistanceLimit()
        set(distanceLimit) {
            prefs.edit().putInt(DISTANCE_LIMIT, distanceLimit).apply()
            BluetoothConstants.setDistanceLimit(distanceLimit)
        }

    override var readerPower: Int
        get() = BluetoothConstants.getReaderPower()
        set(power) {
            val newPower = if (BluetoothConstants.READER_POWER_TABLE[power] == null) BluetoothConstants.DEFAULT_READER_POWER
            else power
            val oldPower = readerPower
            val isPowerChanged = oldPower != newPower
            if (isPowerChanged) {
                prefs.edit().putInt(READER_POWER, newPower).apply()
                BluetoothConstants.setReaderPower(newPower)
            }
            isReaderPowerChaned = isPowerChanged
        }

    override var readerCode: Long
        get() = BluetoothConstants.getReaderCode()
        set(code) {
            prefs.edit().putLong(READER_CODE, code).apply()
            BluetoothConstants.setReaderCode(code)
        }

    override var wiegandVersion: Int
        get() = BluetoothConstants.getWiegandVersion()
        set(version) {
            prefs.edit().putInt(WIEGAND_VERSION, version).apply()
            BluetoothConstants.setWiegandVersion(version)
        }

    override var isReaderPowerChaned: Boolean
        get() = prefs.getBoolean(READER_POWER_CHANGED, false)
        set(changed) {
            prefs.edit().putBoolean(READER_POWER_CHANGED, changed).apply()
        }

    override var useNordic: Boolean
        get() = prefs.getBoolean(USE_NORDIC, USE_NORDIC_DEFAULT)
        set(changed) {
            prefs.edit().putBoolean(USE_NORDIC, changed).apply()
        }

    override var kCoefficient: Int
        get() = BluetoothConstants.getkCoefficient()
        set(coef) {
            prefs.edit().putInt(K_COEFFICIENT, coef).apply()
            BluetoothConstants.setkCoefficient(coef)
        }

    override var advancedDebug: Boolean
        get() = prefs.getBoolean(ADVANCED_DEBUG, ADVANCED_DEBUG_DEFAULT)
        set(value) {
            prefs.edit().putBoolean(ADVANCED_DEBUG, value).apply()
        }

    override var readerSettingsRead: Boolean
        get() = prefs.getBoolean(READER_SETTINGS_READ, false)
        set(value) {
            prefs.edit().putBoolean(READER_SETTINGS_READ, value).apply()
        }

    override var isReaderTimeoutByMovementChanged: Boolean
        get() = prefs.getBoolean(READER_TIMEOUT_BY_MOVEMENT_CHANGED, false)
        set(changed) {
            prefs.edit().putBoolean(READER_TIMEOUT_BY_MOVEMENT_CHANGED, changed).apply()
        }

    override var readerTimeoutByMovement: Int
        get() = prefs.getInt(READER_TIMEOUT_BY_MOVEMENT, 5)
        set(value) {
            val oldTimeout = readerTimeoutByMovement
            val isTimeoutChanged = oldTimeout != value
            if (isTimeoutChanged) {
                prefs.edit().putInt(READER_TIMEOUT_BY_MOVEMENT, value).apply()
            }
            isReaderTimeoutByMovementChanged = isTimeoutChanged
        }

    override var readerFirmwareVersion: String?
        get() = prefs.getString(READER_FIRMWARE_VERSION, null)
        set(value) {
            prefs.edit().putString(READER_FIRMWARE_VERSION, value).apply()
        }

    override fun resetToDefaults() {
        minRssiLevel = BluetoothConstants.MIN_RSSI_LEVEL
        useNordic = USE_NORDIC_DEFAULT
        connectionTimeout = BluetoothConstants.CONNECTION_TIMEOUT
        discoveringTimeout = BluetoothConstants.DISCOVERING_TIMEOUT
        gateStatusReadAfter = BluetoothConstants.GATE_STATUS_EXPLICIT_READ_AFTER
        scanTimeout = BluetoothConstants.getStandardScanTimeoutBasedOnApi()
        pauseDefault = BluetoothConstants.PAUSE_DEFAULT
        pauseConnAuthError = BluetoothConstants.PAUSE_CONN_AUTH_ERROR
        pauseTech = BluetoothConstants.PAUSE_AFTER_TECHNICAL_ISSUE
        pauseNetwork = BluetoothConstants.PAUSE_AFTER_NETWORK_ISSUE
        scanMode = BluetoothConstants.SCAN_MODE
        connPriority = BluetoothConstants.CONN_PRIORITY
        distanceLimit = BluetoothConstants.DEFAULT_DISTANCE_LIMIT
        kCoefficient = BluetoothConstants.DEFAULT_K_COEFFICIENT
        advancedDebug = ADVANCED_DEBUG_DEFAULT
        clearReaderData()
    }

    override fun init() {
        if (!prefs.contains(MIN_RSSI_LEVEL)) {
            prefs.edit().putInt(MIN_RSSI_LEVEL, BluetoothConstants.getMinRssiLevel()).apply()
        } else {
            BluetoothConstants.setMinRssiLevel(prefs.getInt(MIN_RSSI_LEVEL, BluetoothConstants.getMinRssiLevel()))
        }

        if (!prefs.contains(CONNECTION_TIMEOUT)) {
            prefs.edit().putLong(CONNECTION_TIMEOUT, BluetoothConstants.getConnectionTimeout()).apply()
        } else {
            BluetoothConstants.setConnectionTimeout(prefs.getLong(CONNECTION_TIMEOUT, BluetoothConstants.getConnectionTimeout()))
        }

        if (!prefs.contains(TIMEOUT_DISCOVERING)) {
            prefs.edit().putLong(TIMEOUT_DISCOVERING, BluetoothConstants.getDiscoveringTimeout()).apply()
        } else {
            BluetoothConstants.setDiscoveringTimeout(prefs.getLong(TIMEOUT_DISCOVERING, BluetoothConstants.getDiscoveringTimeout()))
        }

        if (!prefs.contains(GATE_STATUS_READ_AFTER)) {
            prefs.edit().putLong(GATE_STATUS_READ_AFTER, BluetoothConstants.getGateStatusReadAfter()).apply()
        } else {
            BluetoothConstants.setGateStatusReadAfter(prefs.getLong(GATE_STATUS_READ_AFTER, BluetoothConstants.getGateStatusReadAfter()))
        }

        if (!prefs.contains(SCAN_TIMEOUT)) {
            prefs.edit().putLong(SCAN_TIMEOUT, BluetoothConstants.getScanTimeout()).apply()
        } else {
            BluetoothConstants.setScanTimeout(prefs.getLong(SCAN_TIMEOUT, BluetoothConstants.getScanTimeout()))
        }

        if (!prefs.contains(PAUSE_DEFAULT)) {
            prefs.edit().putLong(PAUSE_DEFAULT, BluetoothConstants.getPauseDefault()).apply()
        } else {
            BluetoothConstants.setPauseDefault(prefs.getLong(PAUSE_DEFAULT, BluetoothConstants.getPauseDefault()))
        }

        if (!prefs.contains(PAUSE_CONN_AUTH)) {
            prefs.edit().putLong(PAUSE_CONN_AUTH, BluetoothConstants.getPauseConnAuthError()).apply()
        } else {
            BluetoothConstants.setPauseConnAuthError(prefs.getLong(PAUSE_CONN_AUTH, BluetoothConstants.getPauseConnAuthError()))
        }

        if (!prefs.contains(PAUSE_TECH)) {
            prefs.edit().putLong(PAUSE_TECH, BluetoothConstants.getPauseAfterTechnicalIssue()).apply()
        } else {
            BluetoothConstants.setPauseAfterTechnicalIssue(prefs.getLong(PAUSE_TECH, BluetoothConstants.getPauseAfterTechnicalIssue()))
        }

        if (!prefs.contains(PAUSE_NETWORK)) {
            prefs.edit().putLong(PAUSE_NETWORK, BluetoothConstants.getPauseAfterNetwork()).apply()
        } else {
            BluetoothConstants.setPauseAfterNetwork(prefs.getLong(PAUSE_NETWORK, BluetoothConstants.getPauseAfterNetwork()))
        }

        if (!prefs.contains(SCAN_MODE)) {
            prefs.edit().putInt(SCAN_MODE, BluetoothConstants.getScanMode()).apply()
        } else {
            BluetoothConstants.setScanMode(prefs.getInt(SCAN_MODE, BluetoothConstants.getScanMode()))
        }

        if (!prefs.contains(CONN_PRIORITY)) {
            prefs.edit().putInt(CONN_PRIORITY, BluetoothConstants.getConnPriority()).apply()
        } else {
            BluetoothConstants.setConnPriority(prefs.getInt(CONN_PRIORITY, BluetoothConstants.getConnPriority()))
        }

        if (!prefs.contains(DISTANCE_LIMIT)) {
            prefs.edit().putInt(DISTANCE_LIMIT, BluetoothConstants.getDistanceLimit()).apply()
        } else {
            BluetoothConstants.setDistanceLimit(prefs.getInt(DISTANCE_LIMIT, BluetoothConstants.getDistanceLimit()))
        }

        if (!prefs.contains(K_COEFFICIENT)) {
            prefs.edit().putInt(K_COEFFICIENT, BluetoothConstants.getkCoefficient()).apply()
        } else {
            BluetoothConstants.setkCoefficient(prefs.getInt(K_COEFFICIENT, BluetoothConstants.getkCoefficient()))
        }

        if (!prefs.contains(READER_POWER)) {
            prefs.edit().putInt(READER_POWER, BluetoothConstants.getReaderPower()).apply()
        } else {
            BluetoothConstants.setReaderPower(prefs.getInt(READER_POWER, BluetoothConstants.getReaderPower()))
        }

        if (!prefs.contains(READER_CODE)) {
            prefs.edit().putLong(READER_CODE, BluetoothConstants.getReaderCode()).apply()
        } else {
            BluetoothConstants.setReaderCode(prefs.getLong(READER_CODE, BluetoothConstants.getReaderCode()))
        }

        if (!prefs.contains(WIEGAND_VERSION)) {
            prefs.edit().putInt(WIEGAND_VERSION, BluetoothConstants.getWiegandVersion()).apply()
        } else {
            BluetoothConstants.setWiegandVersion(prefs.getInt(WIEGAND_VERSION, BluetoothConstants.getWiegandVersion()))
        }
    }

    override fun printBlePrefs(): String = StringBuilder().apply {
        appendln("Min RSSI = $minRssiLevel")
        appendln("Connection Timeout = $connectionTimeout")
        appendln("Notification Read Timeout = $gateStatusReadAfter")
        appendln("Scan Timeout = $scanTimeout")
        appendln("Pause Default = $pauseDefault")
        appendln("Pause Conn Auth Error = $pauseConnAuthError")
        appendln("Pause Tech Issue = $pauseTech")
        appendln("Pause Network = $pauseNetwork")
        appendln("Scan Mode = $scanMode")
        appendln("Connection Priority = $connPriority")
        appendln("Distance Limit = $distanceLimit")
        appendln("K Coefficient = $kCoefficient")
        appendln("Reader Power = $readerPower")
        appendln("Reader Code = $readerCode")
        appendln("Wiegand Version = $wiegandVersion")
    }.toString()

    override fun clearReaderData() {
        readerPower = BluetoothConstants.DEFAULT_READER_POWER
        readerCode = BluetoothConstants.DEFAULT_CODE
        wiegandVersion = BluetoothConstants.DEFAULT_WIEGAND
        readerSettingsRead = false
        readerFirmwareVersion = null
        readerTimeoutByMovement = 5
    }

    private companion object {
        const val USE_NORDIC_DEFAULT = true
        const val ADVANCED_DEBUG_DEFAULT = false
        const val MIN_RSSI_LEVEL = "min_rssi_level"
        const val USE_NORDIC = "use_nordic"
        const val CONNECTION_TIMEOUT = "connection_timeout"
        const val GATE_STATUS_READ_AFTER = "gate_status_read_after"
        const val SCAN_TIMEOUT = "scan_timeout"
        const val PAUSE_DEFAULT = "pause_default"
        const val PAUSE_CONN_AUTH = "pause_conn_auth"
        const val TIMEOUT_DISCOVERING = "timeout_discovering"
        const val PAUSE_TECH = "pause_tech_issue"
        const val PAUSE_NETWORK = "pause_network"
        const val SCAN_MODE = "scan_mode"
        const val CONN_PRIORITY = "conn_priority"
        const val DISTANCE_LIMIT = "distance_limit"
        const val K_COEFFICIENT = "k_coefficient"
        const val WIEGAND_VERSION = "wiegand_version"
        const val READER_POWER = "reader_power"
        const val READER_CODE = "reader_code"
        const val READER_POWER_CHANGED = "reader_power_changed"
        const val ADVANCED_DEBUG = "advanced_debug"
        const val READER_TIMEOUT_BY_MOVEMENT_CHANGED = "reader_timeout_by_movement_changed"
        const val READER_TIMEOUT_BY_MOVEMENT = "reader_timeout_by_movement"
        const val READER_SETTINGS_READ = "reader_settings_read"
        const val READER_FIRMWARE_VERSION = "reader_firmware_version"
    }
}