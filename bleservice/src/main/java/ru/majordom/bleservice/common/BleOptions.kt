package ru.majordom.bleservice.common

interface BleOptions {
    /**
     * Signal level threshold in range -100 .. 0.
     * Used for filtering devices, -100 is the weakest.
     */
    var minRssiLevel: Int
    var connectionTimeout: Long
    var discoveringTimeout: Long
    var gateStatusReadAfter: Long
    var scanTimeout: Long
    var pauseDefault: Long
    var pauseConnAuthError: Long
    var pauseTech: Long
    var pauseNetwork: Long
    var scanMode: Int
    var connPriority: Int
    var distanceLimit: Int
    var kCoefficient: Int
    var advancedDebug: Boolean
    var useNordic: Boolean
    var readerCode: Long
    var readerSettingsRead: Boolean
    var isReaderPowerChaned: Boolean
    var readerPower: Int
    var isReaderTimeoutByMovementChanged: Boolean
    var readerTimeoutByMovement: Int
    var readerFirmwareVersion: String?
    var wiegandVersion: Int

    fun init()
    fun resetToDefaults()
    fun printBlePrefs(): String
    fun clearReaderData()
}