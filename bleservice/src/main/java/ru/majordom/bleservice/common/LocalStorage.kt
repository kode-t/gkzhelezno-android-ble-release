package ru.majordom.bleservice.common

interface LocalStorage {
    val bleOptions: BleOptions
    val zonesOptions: ZonesOptions
    val isDebug: Boolean
    val appName: String
    val osVersion: String
    val deviceModel: String
    val appVersion: String
    var isAutoOpeningOn: Boolean
    var isPowerManagerAlertShowed: Boolean
}