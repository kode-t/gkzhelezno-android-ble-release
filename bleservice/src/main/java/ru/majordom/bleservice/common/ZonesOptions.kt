package ru.majordom.bleservice.common

interface ZonesOptions {
    /**
     * Radius of the zone.
     */
    var zoneRadiusInMeters: Int

    /**
     * How much zones to request at once.
     */
    var zonesToRequest: Int

    /**
     * Radius from the current location in which to request all visible zones.
     */
    var requestZonesInRadius: Int

    /**
     * Distance from current location to zone's border that not counted as outer area.
     */
    var zoneRadiusLagInMeters: Float

    fun resetToDefaults()
}