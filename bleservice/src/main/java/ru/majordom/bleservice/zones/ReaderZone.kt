package ru.majordom.bleservice.zones

data class ReaderZone(
        var id: String? = null,
        var name: String? = null,
        var location: ZoneLocation
)

data class ZoneLocation(
        var lat: Double = 0.0,
        var lng: Double = 0.0
)

