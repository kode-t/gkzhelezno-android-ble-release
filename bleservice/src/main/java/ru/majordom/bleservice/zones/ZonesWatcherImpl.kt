package ru.majordom.bleservice.zones

import android.annotation.SuppressLint
import android.arch.lifecycle.MediatorLiveData
import android.content.Context
import android.location.Location
import android.util.Log
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.bluetooth.bleService.BleService
import ru.majordom.bleservice.bluetooth.bleService.BluetoothStateReceiver
import ru.majordom.bleservice.common.ZonesOptions
import ru.majordom.bleservice.connectivity.NetworkChangesAggregator
import ru.majordom.bleservice.notifications.ZonesNotifications
import java.util.*

class ZonesWatcherImpl internal constructor(
        private val context: Context,
        private val bluetoothManager: BluetoothManagerContract,
        private val zonesProvider: ZonesProvider,
        private val zonesOptions: ZonesOptions,
        private val geofenceNotifications: ZonesNotifications,
        private val networkChangesAggregator: NetworkChangesAggregator
) : LocationCallback(),
        ZonesWatcher,
        NetworkChangesAggregator.Listener,
        BluetoothStateReceiver.BluetoothStateListener {
    private var isStarted: Boolean = false

    private var currentLocation: Location? = null
    private var zonesUpdatedLocation: Location? = null

    private val registeredZones = HashSet<ReaderZone>(10, 1.0f)

    // Sets the desired interval for active location updates. This interval is
    // inexact. You may not receive updates at all if no location sources are available, or
    // you may receive them slower than requested. You may also receive updates faster than
    // requested if other applications are requesting location at a faster interval.
    // Sets the fastest rate for active location updates. This interval is exact, and your
    // application will never receive updates faster than this value.
    private val locationRequest: LocationRequest
        get() = LocationRequest.create().apply {
            interval = UPDATE_INTERVAL_IN_MILLISECONDS
            fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            smallestDisplacement = SMALLEST_DISPLACEMENT_METERS
        }

    init {
        bluetoothManager.addBluetoothStateListener(this)
    }

    override fun start() {
        val permissionGranted = GeolocationChecker.isLocationPermissionGranted(context)
        if (networkChangesAggregator.isConnected) {
            if (!isStarted && permissionGranted) {
                isStarted = true
                networkChangesAggregator.addListener(this)
                startUpdates()
            } else if (isStarted && !permissionGranted) {
                stop()
            }
        }
    }

    override fun stop() {
        if (isStarted) {
            isStarted = false
            networkChangesAggregator.removeListener(this)
            stopUpdates()
        }
    }

    override val isConnected: Boolean
        get() = isStarted && networkChangesAggregator.isConnected

    override val isGeolocationEnabled: Boolean
        get() = GeolocationChecker.isGeolocationEnabled(context)

    override fun onBluetoothAvailable() = stop()

    override fun onBluetoothUnavailable() = start()

    override fun onLocationResult(locationResult: LocationResult?) {
        locationResult?.let {
            val location = it.lastLocation
            Log.d(TAG, "Location changed: $location")
            currentLocation = location
            updateZones(location)
        }
    }

    override fun onConnectivityChanged(isConnected: Boolean) {
        if (isStarted) {
            if (isConnected) startUpdates()
            else stopUpdates()
        }
    }

    private fun startUpdates() = startLocationUpdates()

    private fun stopUpdates() {
        clearZones()
        stopLocationUpdates()
    }

    private fun updateZones(location: Location?) {
        if (isConnected && location != null) {
            val gfLocation = zonesUpdatedLocation
            if (gfLocation == null || gfLocation.distanceTo(location) >= zonesOptions.zoneRadiusInMeters) {
                zonesUpdatedLocation = location
                val zonesLiveData = zonesProvider.getZones(
                        location,
                        zonesOptions.requestZonesInRadius,
                        zonesOptions.zonesToRequest)
                val mediator = MediatorLiveData<List<ReaderZone>>()
                mediator.addSource(zonesLiveData) {
                    mediator.removeSource(zonesLiveData)
                    mediator.value = it
                }
                mediator.observeForever { data ->
                    data?.let { zones ->
                        val registeredZonesList = registeredZones.toList()
                        val zonesToDelete = registeredZonesList.minus(zones)
                        if (zonesToDelete.isNotEmpty()) removeZones(zonesToDelete)

                        val zonesToAdd = zones.minus(registeredZonesList)
                        if (zonesToAdd.isNotEmpty()) addZones(zonesToAdd)
                    }
                    updateServiceState(location)
                }
            } else updateServiceState(location)
        }
    }

    private fun updateServiceState(location: Location) {
        val mockLocation = Location(location)
        val zonesInRange = registeredZones.filter {
            mockLocation.latitude = it.location.lat
            mockLocation.longitude = it.location.lng
            mockLocation.distanceTo(location) <= zonesOptions.zoneRadiusInMeters + zonesOptions.zoneRadiusLagInMeters
        }
        val areZonesInRange = zonesInRange.isNotEmpty()
        if (areZonesInRange) {
            if (!bluetoothManager.isBluetoothEnabled) {
                Log.d(TAG, "Triggered Ble enabling logic.")
                bluetoothManager.enableBluetooth(true)
                geofenceNotifications.sendBluetoothEnabledNotification(context)
            }
            Log.d(TAG, "There are zones in range: ${zonesInRange.joinToString {
                it.name ?: "unknown"
            }}, starting scan.")
            BleService.startScan(context) // start scan
        } else {
            Log.d(TAG, "There are no zones in range, stopping scan.")
            BleService.stopScan(context) // stop scan
        }
    }

    private fun addZones(geoFences: List<ReaderZone>) = registeredZones.addAll(geoFences)

    private fun removeZones(geoFences: List<ReaderZone>) = registeredZones.removeAll(geoFences)

    private fun clearZones() {
        zonesUpdatedLocation = null
        registeredZones.clear()
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        if (GeolocationChecker.isLocationPermissionGranted(context)) {
            LocationServices.getFusedLocationProviderClient(context)
                    .requestLocationUpdates(locationRequest, this, null)
        }
    }

    private fun stopLocationUpdates() {
        LocationServices.getFusedLocationProviderClient(context)
                .removeLocationUpdates(this)
    }

    private companion object {

        val TAG = ZonesWatcherImpl::class.java.simpleName

        /**
         * The desired interval for location updates. Inexact. Updates may be more or less frequent.
         */
        const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

        /**
         * The distance between current saved location and updated location, at which new location becomes current.
         */
        const val SMALLEST_DISPLACEMENT_METERS = 10f
        /**
         * The fastest rate for active location updates. Exact. Updates will never be more frequent
         * than this value.
         */
        const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    }
}