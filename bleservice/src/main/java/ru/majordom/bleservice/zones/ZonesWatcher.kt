package ru.majordom.bleservice.zones

interface ZonesWatcher {
    val isConnected: Boolean
    val isGeolocationEnabled: Boolean
    fun start()
    fun stop()
}