package ru.majordom.bleservice.zones

import android.arch.lifecycle.LiveData
import android.location.Location

interface ZonesProvider {
    fun getZones(currentLocation: Location, radius: Int, maxCount: Int): LiveData<List<ReaderZone>>
}
