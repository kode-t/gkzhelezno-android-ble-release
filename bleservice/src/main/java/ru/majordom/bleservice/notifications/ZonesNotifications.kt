package ru.majordom.bleservice.notifications

import android.content.Context

interface ZonesNotifications {
    fun sendBluetoothEnabledNotification(context: Context)
}