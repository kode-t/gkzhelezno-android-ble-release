package ru.majordom.bleservice.notifications

import android.app.Service

interface ServiceNotifications {
    fun sendForegroundServiceNotification(service: Service, isServiceStarted: Boolean, type: ServiceNotificationType = ServiceNotificationType.SERVICE_RUNNING)
    fun sendInfoNotification(type: ServiceNotificationType, message: String? = null)
    fun sendPossibleToOpenNotification()
    fun sendOpeningNotification()
    fun sendApproachingNotification(isTooFar: Boolean)
    fun sendBatterySaverNotification()
    fun clearServiceNotifications(all: Boolean)
    fun clear(type: ServiceNotificationType)
}