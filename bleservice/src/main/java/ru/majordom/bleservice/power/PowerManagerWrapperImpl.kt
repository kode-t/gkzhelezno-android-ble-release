package ru.majordom.bleservice.power

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import android.support.annotation.RequiresApi


class PowerManagerWrapperImpl internal constructor(private var context: Context) : PowerManagerWrapper {

    private val pm: PowerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager

    override val isPowerSaveMode: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && pm.isPowerSaveMode

    override val isIgnoringBatteryOptimizations: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && pm.isIgnoringBatteryOptimizations(context.packageName)

    override val isScreenOn: Boolean
        get() = pm.isInteractive

    override val powerManager: PowerManager
        get() = pm

    override val addToWhitelistIntent: Intent
        @RequiresApi(Build.VERSION_CODES.M)
        @SuppressLint("BatteryLife")
        get() = Intent().apply {
            action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
            data = Uri.parse("package:" + context.packageName)
            flags = FLAG_ACTIVITY_NEW_TASK
        }

    override fun getNonStandardPhonesBatteryScreenIntent(): Intent? {
        try {
            val intent = Intent()
            val manufacturer = Build.MANUFACTURER
            intent.component = when {
                "xiaomi".equals(manufacturer, ignoreCase = true) -> ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")
                "oppo".equals(manufacturer, ignoreCase = true) -> ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")
                "vivo".equals(manufacturer, ignoreCase = true) -> ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")
                "oneplus".equals(manufacturer, ignoreCase = true) -> ComponentName("com.oneplus.security", "com.oneplus.security.chainlaunch.view.ChainLaunchAppListAct‌​ivity")
                "Letv".equals(manufacturer, ignoreCase = true) -> ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")
                "Honor".equals(manufacturer, ignoreCase = true) -> ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")
                "huawei".equals(manufacturer, ignoreCase = true) -> ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")
                else -> return null
            }
            val list = context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            if (list.size > 0) {
                intent.flags = FLAG_ACTIVITY_NEW_TASK
                return intent
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }
}