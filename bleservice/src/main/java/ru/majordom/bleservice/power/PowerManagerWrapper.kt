package ru.majordom.bleservice.power

import android.content.Intent
import android.os.Build
import android.os.PowerManager
import android.support.annotation.RequiresApi

interface PowerManagerWrapper {
    val isPowerSaveMode: Boolean
    val isIgnoringBatteryOptimizations: Boolean
    val isScreenOn: Boolean
    val powerManager: PowerManager
    @get:RequiresApi(api = Build.VERSION_CODES.M)
    val addToWhitelistIntent: Intent
    fun getNonStandardPhonesBatteryScreenIntent(): Intent?
}
