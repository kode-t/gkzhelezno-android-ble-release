package ru.majordom.bleservice

import android.annotation.SuppressLint
import android.content.Context
import org.koin.core.Koin
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.StandAloneContext.stopKoin
import ru.majordom.bleservice.bluetooth.bleService.BleService
import ru.majordom.bleservice.di.buildServiceModule
import ru.majordom.bleservice.zones.ZonesWatcher

@SuppressLint("StaticFieldLeak")
object BleServiceInitializer {

    private val isReadyToGo: Boolean
        get() = ::koin.isInitialized

    internal lateinit var koin: Koin

    fun setupBleService(context: Context, configuration: BleServiceConfiguration) {
        BleService.setConfiguration(configuration)
        setupKoin(context, configuration)
    }

    fun runServices() {
        if (isReadyToGo) koin.koinContext.apply {
            BleService.start(get(), true)
            get<ZonesWatcher>().start()
        }
    }

    fun stopServices() {
        if (isReadyToGo) koin.koinContext.apply {
            BleService.stop(get())
            get<ZonesWatcher>().stop()
        }
    }

    private fun setupKoin(context: Context, configuration: BleServiceConfiguration) {
        if (isReadyToGo) stopKoin() // recreate dependency as config is changed
        val localKoin = startKoin(listOf(buildServiceModule(context, configuration)))
        koin = localKoin
    }
}