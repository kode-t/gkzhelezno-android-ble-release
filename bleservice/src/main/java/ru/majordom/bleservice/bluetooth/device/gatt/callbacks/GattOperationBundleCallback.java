package ru.majordom.bleservice.bluetooth.device.gatt.callbacks;

public interface GattOperationBundleCallback extends GattErrorListener {
    void onAllOperationsDone();
}
