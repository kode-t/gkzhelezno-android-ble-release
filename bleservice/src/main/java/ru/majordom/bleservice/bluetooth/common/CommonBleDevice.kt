package ru.majordom.bleservice.bluetooth.common

import no.nordicsemi.android.support.v18.scanner.ScanResult
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice


class CommonBleDevice<Device : Any> private constructor(
        val device: Device,
        val name: String,
        val macAddress: String,
        val scanRecord: ByteArray?,
        val rssi: Int) {

    companion object {
        fun from(device: BlueteethDevice): CommonBleDevice<BlueteethDevice> {
            return CommonBleDevice(device, device.name, device.macAddress, device.scanRecord, device.rssi)
        }

        fun from(result: ScanResult): CommonBleDevice<ScanResult> {
            return CommonBleDevice(result, result.device.name, result.device.address, result.scanRecord?.bytes, result.rssi)
        }
    }
}
