package ru.majordom.bleservice.bluetooth.interaction

import android.os.Handler
import android.os.Looper

import ru.majordom.bleservice.bluetooth.common.ErrorListener
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice

abstract class BaseModule {
    companion object {
        internal val uiHandler: Handler by lazy { Handler(Looper.getMainLooper()) }

        private fun deviceIsReady(device: BlueteethDevice?): Boolean = device != null && device.isConnected

        internal fun okToProceed(device: BlueteethDevice?, errorMsg: String, listener: ErrorListener): Boolean {
            return if (deviceIsReady(device)) true
            else {
                uiHandler.post { listener.onError(errorMsg) }
                false
            }
        }
    }
}