package ru.majordom.bleservice.bluetooth.nordic.interaction.searchstrategy

import android.util.Log
import no.nordicsemi.android.support.v18.scanner.ScanResult
import ru.majordom.bleservice.bluetooth.BluetoothConstants
import ru.majordom.bleservice.bluetooth.common.ScanRecord
import ru.majordom.bleservice.common.BleOptions
import java.util.*

abstract class BaseSearchStrategy protected constructor(
        protected var bleOptions: BleOptions
) : SearchStrategy {

    protected val TAG: String = this.javaClass.simpleName
    protected val suitableDevices: MutableList<ScanResult> = ArrayList()

    override val defaultScanTimeout: Long
        get() = bleOptions.scanTimeout

    override val isSingleDeviceScan: Boolean
        get() = false

    override fun checkDeviceFound(device: ScanResult): Boolean {
        val devName = device.device.name
        val isNameMatches = !devName.isNullOrBlank() && devName.toUpperCase().contains(BluetoothConstants.GATE_GENERAL_NAME)
        if (isNameMatches) {
            val scanRecord = ScanRecord.parseFromBytes(device.scanRecord?.bytes)
            var isCorrectDevice = false
            var hasProperService = false
            if (scanRecord != null) {
                for (service in scanRecord.serviceUuids) {
                    hasProperService = service.toString().toUpperCase().contains(BluetoothConstants.GATE_ADVERTISING_MARKER)
                    if (hasProperService) {
                        isCorrectDevice = true
                        break
                    }
                }
            } else {
                isCorrectDevice = true // no scan record
            }
            if (isCorrectDevice) {
                Log.d(TAG, "Found device with name = ${device.device.name}, RSSI = ${device.rssi}, has advertising marker: $hasProperService")
                suitableDevices.add(device)
                return true
            }
        }
        return false

    }

    override fun pickTheMostSuitable(): ScanResult? {
        Log.d(TAG, "Found devices: ${suitableDevices.size}")
        var chosenDevice: ScanResult? = null
        synchronized(suitableDevices) {
            for (dev in suitableDevices) {
                chosenDevice = compareWhichIsBetter(chosenDevice, dev)
            }
        }
        return chosenDevice
    }

    override fun release() {
        suitableDevices.clear()
    }

    protected abstract fun compareWhichIsBetter(
            selectedDevice: ScanResult?,
            currentDevice: ScanResult): ScanResult
}