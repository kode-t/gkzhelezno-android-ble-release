package ru.majordom.bleservice.bluetooth.interaction.search_strategy

import android.util.Log
import ru.majordom.bleservice.bluetooth.BluetoothConstants
import ru.majordom.bleservice.bluetooth.common.ScanRecord
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethManager
import ru.majordom.bleservice.common.BleOptions
import java.util.*

abstract class BaseSearchStrategy protected constructor(
        protected var manager: BlueteethManager?,
        protected var bleOptions: BleOptions
) : SearchStrategy {

    protected val TAG: String = this.javaClass.simpleName
    protected val suitableDevices: MutableList<BlueteethDevice> = ArrayList()

    override val defaultScanTimeout: Long
        get() = bleOptions.scanTimeout

    override val isSingleDeviceScan: Boolean
        get() = false

    override fun checkDeviceFound(device: BlueteethDevice): Boolean {
        val devName = device.name
        val isNameMatches = !devName.isNullOrBlank() && devName.toUpperCase().contains(BluetoothConstants.GATE_GENERAL_NAME)
        if (isNameMatches) {
            val scanRecord = ScanRecord.parseFromBytes(device.scanRecord)
            var isCorrectDevice = false
            var hasProperService = false
            if (scanRecord != null) {
                for (service in scanRecord.serviceUuids) {
                    hasProperService = service.toString().toUpperCase().contains(BluetoothConstants.GATE_ADVERTISING_MARKER)
                    if (hasProperService) {
                        isCorrectDevice = true
                        break
                    }
                }
            } else {
                isCorrectDevice = true // no scan record
            }
            if (isCorrectDevice) {
                Log.d(TAG, "Found device with name = ${device.name}, RSSI = ${device.rssi}, has advertising marker: $hasProperService")
                suitableDevices.add(device)
                return true
            }
        }
        // we don't need it anymore
        manager?.removePeripheral(device)
        return false

    }

    override fun pickTheMostSuitable(): BlueteethDevice? {
        Log.d(TAG, "Found devices: ${suitableDevices.size}")
        var chosenDevice: BlueteethDevice? = null
        synchronized(suitableDevices) {
            for (dev in suitableDevices) {
                chosenDevice = compareWhichIsBetter(chosenDevice, dev)
            }
        }
        return chosenDevice
    }

    override fun release() {
        this.manager = null
        suitableDevices.clear()
    }

    protected abstract fun compareWhichIsBetter(
            selectedDevice: BlueteethDevice?,
            currentDevice: BlueteethDevice): BlueteethDevice
}