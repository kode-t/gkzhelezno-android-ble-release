package ru.majordom.bleservice.bluetooth.nordic.interaction


import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import no.nordicsemi.android.ble.BleManager
import no.nordicsemi.android.ble.BleManagerCallbacks
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat
import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanResult
import no.nordicsemi.android.support.v18.scanner.ScanSettings
import ru.majordom.bleservice.R
import ru.majordom.bleservice.bluetooth.BluetoothConstants
import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.bluetooth.BluetoothServices
import ru.majordom.bleservice.bluetooth.common.AuthListener
import ru.majordom.bleservice.bluetooth.common.DeviceInfoReadListener
import ru.majordom.bleservice.bluetooth.common.Enums
import ru.majordom.bleservice.bluetooth.common.ErrorListener
import ru.majordom.bleservice.bluetooth.nordic.interaction.searchstrategy.SearchStrategy
import ru.majordom.bleservice.common.BleOptions

class DoorDeviceModule(
        context: Context,
        private val bleOptions: BleOptions
) : BleManager<BleManagerCallbacks>(context) {

    override fun getGattCallback(): BleManagerGattCallback = object : BleManagerGattCallback() {
        override fun onDeviceDisconnected() {
            commandCharacteristic = null
            responseCharacteristic = null
        }

        override fun isRequiredServiceSupported(gatt: BluetoothGatt): Boolean {
            val barrierControlService = gatt.getService(BluetoothServices.BarrierControl.SERVICE_UUID)
            return barrierControlService?.let { service ->
                commandCharacteristic = service.getCharacteristic(BluetoothServices.BarrierControl.COMMAND)
                responseCharacteristic = service.getCharacteristic(BluetoothServices.BarrierControl.RESPONSE)
                commandCharacteristic != null && responseCharacteristic != null
            } ?: false
        }
    }

    private val TAG = DoorDeviceModule::class.java.simpleName
    private var responseCharacteristic: BluetoothGattCharacteristic? = null
    private var commandCharacteristic: BluetoothGattCharacteristic? = null
    private val DEFAULT_READ_DELAY = 333L
    private val DEFAULT_WRITE_DELAY = 2000L

    val uiHandler: Handler by lazy { Handler(Looper.getMainLooper()) }

    private fun deviceIsReady(device: BluetoothDevice?) = device != null && isConnected

    private fun okToProceed(device: BluetoothDevice?, errorMsg: String, listener: ErrorListener): Boolean {
        if (deviceIsReady(device)) {
            return true
        } else {
            uiHandler.post { listener.onError(errorMsg) }
        }
        return false
    }

    fun searchForGate(
            scanner: BluetoothLeScannerCompat,
            bluetoothManager: BluetoothManagerContract,
            searchStrategy: SearchStrategy,
            listener: SearchListener): ScanCallback {
        Log.d(TAG, "Searching for devices...")
        var scanCallback: ScanCallback? = null
        val scanStopper = Runnable {
            scanCallback?.let {
                if (bluetoothManager.isBluetoothEnabled) scanner.stopScan(it)
                searchStrategy.release()
                listener.onNoDeviceFound()
            }
        }
        scanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                val isFound = searchStrategy.checkDeviceFound(result)
                if (searchStrategy.isSingleDeviceScan && isFound) {
                    val chosenDevice = searchStrategy.pickTheMostSuitable()
                    if (chosenDevice != null) {
                        uiHandler.removeCallbacks(scanStopper)
                        uiHandler.post { listener.onSearchCompleted(chosenDevice) }
                        searchStrategy.release()
                    }
                    scanCallback?.let {
                        if (bluetoothManager.isBluetoothEnabled) scanner.stopScan(it)
                    }
                }
            }

            override fun onScanFailed(errorCode: Int) {
                uiHandler.removeCallbacks(scanStopper)
                listener.onError("Scanning error: $errorCode")
                searchStrategy.release()
            }
        }
        val settings = ScanSettings.Builder()
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) // add latency from the prefs
                .setReportDelay(0)
                .build()
        scanner.startScan(mutableListOf(), settings, scanCallback)
        uiHandler.postDelayed(scanStopper, searchStrategy.defaultScanTimeout)
        return scanCallback
    }

    fun connectToTheGate(
            device: BluetoothDevice?,
            listener: ConnectionListener) {
        if (device != null) {
            val connectionCallbackCalled = booleanArrayOf(false)
            val discoveryCallbackCalled = booleanArrayOf(false)
            val explicitConnectionCheck = {
                if (!connectionCallbackCalled[0]) {
                    uiHandler.post { listener.onTimeout(context.getString(R.string.bluetooth_connection_failed)) }
                }
            }
            val explicitDiscoveryCheck = {
                if (!discoveryCallbackCalled[0]) {
                    uiHandler.post { listener.onTimeout(context.getString(R.string.bluetooth_service_discovery_failed)) }
                }
            }
            val callbacks = object : StandardBleCallbacks() {
                override fun onDeviceDisconnected(device: BluetoothDevice) {
                    Log.d(TAG, "Disconnected from ${device.name}")
                    connectionCallbackCalled[0] = true
                    discoveryCallbackCalled[0] = true
                    uiHandler.apply {
                        removeCallbacks(explicitConnectionCheck)
                        removeCallbacks(explicitDiscoveryCheck)
                        post { listener.onGateConnection(device, false) }
                    }
                }

                override fun onDeviceConnected(device: BluetoothDevice) {
                    Log.d(TAG, "Connected to ${device.name}")
                    val connPriority = when (bleOptions.connPriority) {
                        BluetoothConstants.CONN_PRIORITY_LOW -> BluetoothGatt.CONNECTION_PRIORITY_LOW_POWER
                        BluetoothConstants.CONN_PRIORITY_MID -> BluetoothGatt.CONNECTION_PRIORITY_BALANCED
                        BluetoothConstants.CONN_PRIORITY_HIGH -> BluetoothGatt.CONNECTION_PRIORITY_HIGH
                        else -> BluetoothGatt.CONNECTION_PRIORITY_LOW_POWER
                    }
                    requestConnectionPriority(connPriority).enqueue()
                    connectionCallbackCalled[0] = true
                    uiHandler.apply {
                        removeCallbacks(explicitConnectionCheck)
                        post { listener.onGateConnection(device, true) }
                        postDelayed(explicitDiscoveryCheck, bleOptions.discoveringTimeout)
                    }
                }

                override fun onServicesDiscovered(device: BluetoothDevice, optionalServicesFound: Boolean) {
                    Log.d(TAG, "Services discovered.")
                    discoveryCallbackCalled[0] = true
                    uiHandler.apply {
                        removeCallbacks(explicitDiscoveryCheck)
                        post { listener.onServicesDiscovered(device) }
                    }
                }

                override fun onError(device: BluetoothDevice, message: String, errorCode: Int) =
                        listener.onError(message)
            }
            setGattCallbacks(callbacks)
            connect(device)
                    .useAutoConnect(false)
                    .done {
                        // use callbacks to react on connection
                    }
                    .fail { _, _ ->
                        uiHandler.post { listener.onError(context.getString(R.string.bluetooth_connection_failed)) }
                    }
                    .enqueue()
            uiHandler.postDelayed(explicitConnectionCheck, bleOptions.connectionTimeout)
        } else
            uiHandler.post { listener.onError(context.getString(R.string.bluetooth_connection_failed)) }
    }

    fun authenticate(
            device: BluetoothDevice?,
            listener: AuthListener) {
        if (okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            val localCommand = commandCharacteristic
            val localResponse = responseCharacteristic
            if (localCommand != null && localResponse != null) {
                sendData(localResponse, localCommand, listener)
            }
        }
    }

    private fun sendData(
            localResponse: BluetoothGattCharacteristic,
            localCommand: BluetoothGattCharacteristic,
            listener: AuthListener) {
        if (bleOptions.isReaderPowerChaned || bleOptions.isReaderTimeoutByMovementChanged) {
            writePower(localResponse, localCommand, listener) {
                writeTimeout(localResponse, localCommand, listener) {
                    uiHandler.postDelayed({
                        listener.onAuthResponse(Enums.GateStatus.DATA_SET)
                        sendCode(localResponse, localCommand, listener)
                    }, 2200)
                }
            }
        } else {
            sendCode(localResponse, localCommand, listener)
        }
    }

    private fun sendCode(
            localResponse: BluetoothGattCharacteristic,
            localCommand: BluetoothGattCharacteristic,
            listener: AuthListener,
            disableNotification: Boolean = false) {
        val bytesToWrite = BluetoothServices.BarrierControl.prepareCodeCommand(bleOptions.readerCode, bleOptions.wiegandVersion)
        setNotificationCallback(localResponse)
                .with { _, data ->
                    if (disableNotification) disableNotifications(localResponse).enqueue()
                    if (BluetoothServices.BarrierControl.isResponseOk(data.value)) {
                        Log.d(TAG, "Code ${bleOptions.readerCode} wrote")
                        listener.onAuthResponse(Enums.GateStatus.SUCCESS)
                    } else {
                        listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
                    }
                }
        enableNotifications(localResponse).enqueue()
        writeCharacteristic(localCommand, bytesToWrite)
                .fail { _, _ ->
                    listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
                }
                .enqueue()
    }

    private fun writePower(
            localResponse: BluetoothGattCharacteristic,
            localCommand: BluetoothGattCharacteristic,
            listener: AuthListener,
            next: () -> Unit) {
        val powerBytes = BluetoothServices.BarrierControl.prepareSetTxBytes(bleOptions.readerPower)
        setNotificationCallback(localResponse)
                .with { _, data ->
                    disableNotifications(localResponse).enqueue()
                    if (BluetoothServices.BarrierControl.isResponseOk(data.value)) {
                        Log.d(TAG, "Power ${bleOptions.readerPower} wrote")
                        uiHandler.postDelayed({ next.invoke() }, DEFAULT_WRITE_DELAY)
                    }
                }
        enableNotifications(localResponse).enqueue()
        writeCharacteristic(localCommand, powerBytes)
                .fail { _, _ ->
                    listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
                }
                .enqueue()
    }

    private fun writeTimeout(
            localResponse: BluetoothGattCharacteristic,
            localCommand: BluetoothGattCharacteristic,
            listener: AuthListener,
            next: () -> Unit) {
        val timeoutBytes = BluetoothServices.BarrierControl.prepareSetMovementTimeoutBytes(bleOptions.readerTimeoutByMovement)
        setNotificationCallback(localResponse)
                .with { _, data ->
                    disableNotifications(localResponse).enqueue()
                    if (BluetoothServices.BarrierControl.isResponseOk(data.value)) {
                        Log.d(TAG, "Timeout ${bleOptions.readerTimeoutByMovement} wrote")
                        uiHandler.postDelayed({ next.invoke() }, DEFAULT_WRITE_DELAY)
                    }
                }
        enableNotifications(localResponse).enqueue()
        writeCharacteristic(localCommand, timeoutBytes)
                .fail { _, _ ->
                    listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
                }
                .enqueue()
    }

    fun readDeviceInfo(
            device: BluetoothDevice?,
            listener: DeviceInfoReadListener) {
        if (okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            val localCommand = commandCharacteristic
            val localResponse = responseCharacteristic
            if (localCommand != null && localResponse != null) {
                readPowerLevel(localResponse, localCommand, listener) {
                    readMovementTimeout(localResponse, localCommand, listener) {
                        readFirmwareVersion(localResponse, localCommand, listener)
                    }
                }
            }
        }
    }

    private fun readPowerLevel(
            localResponse: BluetoothGattCharacteristic,
            localCommand: BluetoothGattCharacteristic,
            listener: DeviceInfoReadListener,
            next: () -> Unit) {
        val powerBytes = BluetoothServices.BarrierControl.getCommandBytes(BluetoothServices.BarrierControl.COMMAND_READ_TEX)
        setNotificationCallback(localResponse)
                .with { _, data ->
                    disableNotifications(localResponse).enqueue()
                    val powerLevel = BluetoothServices.BarrierControl.readTxBytes(data.value)
                    listener.onTxRead(powerLevel)
                    Log.d(TAG, "Power level read: $powerLevel")
                    uiHandler.postDelayed({ next.invoke() }, DEFAULT_READ_DELAY)
                }
        enableNotifications(localResponse).enqueue()
        writeCharacteristic(localCommand, powerBytes)
                .fail { _, _ ->
                    listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
                }
                .enqueue()
    }

    private fun readMovementTimeout(
            localResponse: BluetoothGattCharacteristic,
            localCommand: BluetoothGattCharacteristic,
            listener: DeviceInfoReadListener,
            next: () -> Unit) {
        val timeoutBytes = BluetoothServices.BarrierControl.getCommandBytes(BluetoothServices.BarrierControl.COMMAND_READ_MOVEMENT_TIMEOUT)
        setNotificationCallback(localResponse)
                .with { _, data ->
                    disableNotifications(localResponse).enqueue()
                    val timeout = BluetoothServices.BarrierControl.readMovementTimeoutBytes(data.value)
                    listener.onTimeoutByMovementRead(timeout)
                    Log.d(TAG, "Movement Timeout read: $timeout")
                    uiHandler.postDelayed({ next.invoke() }, DEFAULT_READ_DELAY)
                }
        enableNotifications(localResponse).enqueue()
        writeCharacteristic(localCommand, timeoutBytes)
                .fail { _, _ ->
                    listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
                }
                .enqueue()
    }

    private fun readFirmwareVersion(
            localResponse: BluetoothGattCharacteristic,
            localCommand: BluetoothGattCharacteristic,
            listener: DeviceInfoReadListener) {
        val fwVersionBytes = BluetoothServices.BarrierControl.getCommandBytes(BluetoothServices.BarrierControl.COMMAND_READ_FIRMWARE)
        setNotificationCallback(localResponse)
                .with { _, data ->
                    disableNotifications(localResponse).enqueue()
                    data.value?.let {
                        val versionCodes = BluetoothServices.BarrierControl.readFirmwareBytes(it)
                        Log.d(TAG, "FW Version read: ${versionCodes[0]}.${versionCodes[1]}")
                        listener.onFirmwareVersionRead(versionCodes[0], versionCodes[1])
                    }
                    listener.onCompele()
                }
        enableNotifications(localResponse).enqueue()
        writeCharacteristic(localCommand, fwVersionBytes)
                .fail { _, _ ->
                    listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
                }
                .enqueue()
    }

    interface SearchListener : ErrorListener {
        fun onSearchCompleted(device: ScanResult)
        fun onNoDeviceFound()
        fun onLogInfo(message: String, isError: Boolean)
    }

    interface ConnectionListener : ErrorListener {
        fun onServicesDiscovered(device: BluetoothDevice)
        fun onGateConnection(device: BluetoothDevice, isConnected: Boolean)
    }

    private abstract class StandardBleCallbacks : BleManagerCallbacks {
        override fun onDeviceConnecting(device: BluetoothDevice) = Unit
        override fun onDeviceDisconnecting(device: BluetoothDevice) = Unit
        override fun onDeviceReady(device: BluetoothDevice) = Unit
        override fun onDeviceNotSupported(device: BluetoothDevice) = onError(device, "Device not aupported", 0)
        override fun onBondingFailed(device: BluetoothDevice) = onError(device, "Bonding failed", 0)
        override fun onBondingRequired(device: BluetoothDevice) = onError(device, "Bonding required", 0)
        override fun onLinkLossOccurred(device: BluetoothDevice) = onDeviceDisconnected(device)
        override fun onBatteryValueReceived(device: BluetoothDevice, value: Int) = Unit
        override fun onBonded(device: BluetoothDevice) = Unit
        override fun shouldEnableBatteryLevelNotifications(device: BluetoothDevice) = false
    }
}