package ru.majordom.bleservice.bluetooth.device.listeners;


import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

import java.util.List;

public interface OnScanResponseListener {
    void call(List<BlueteethDevice> blueteethDevices);
}
