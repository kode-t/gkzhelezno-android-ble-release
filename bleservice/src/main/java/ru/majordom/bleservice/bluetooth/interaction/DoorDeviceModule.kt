package ru.majordom.bleservice.bluetooth.interaction

import android.content.Context
import android.util.Log
import ru.majordom.bleservice.R
import ru.majordom.bleservice.bluetooth.BluetoothServices
import ru.majordom.bleservice.bluetooth.common.AuthListener
import ru.majordom.bleservice.bluetooth.common.DeviceInfoReadListener
import ru.majordom.bleservice.bluetooth.common.Enums
import ru.majordom.bleservice.bluetooth.common.ErrorListener
import ru.majordom.bleservice.bluetooth.device.gatt.GattOperationBundle
import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.GattCharacteristicReadCallback
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattOperation
import ru.majordom.bleservice.bluetooth.device.interaction.BaseGatt
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethManager
import ru.majordom.bleservice.bluetooth.device.listeners.OnConnectionChangedListener
import ru.majordom.bleservice.bluetooth.interaction.search_strategy.SearchStrategy
import ru.majordom.bleservice.common.BleOptions


class DoorDeviceModule(
        private val context: Context,
        private val bleOptions: BleOptions
) : BaseModule() {

    interface SearchListener : ErrorListener {
        fun onSearchCompleted(device: BlueteethDevice?)
        fun onNoDeviceFound()
    }

    interface ConnectionListener : ErrorListener {
        fun onServicesDiscovered(device: BlueteethDevice)
        fun onGateConnection(device: BlueteethDevice, isConnected: Boolean)
    }

    private val TAG = DoorDeviceModule::class.java.simpleName
    private val DEFAULT_READ_DELAY = 333L
    private val DEFAULT_WRITE_DELAY = 2000L

    fun searchForGate(
            manager: BlueteethManager,
            searchStrategy: SearchStrategy,
            listener: SearchListener) {
        Log.d(TAG, "Searching for devices...")
        manager.scanForPeripherals(searchStrategy.defaultScanTimeout.toInt(),
                { device ->
                    val isFound = searchStrategy.checkDeviceFound(device)
                    if (searchStrategy.isSingleDeviceScan && isFound) {
                        manager.stopScanForPeripherals()
                    }
                }, {
            val chosenDevice = searchStrategy.pickTheMostSuitable()
            if (chosenDevice == null) {
                Log.d(TAG, context.getString(R.string.bluetooth_search_no_device))
                uiHandler.post { listener.onNoDeviceFound() }
            } else {
                uiHandler.post {
                    chosenDevice.isInUse = true
                    listener.onSearchCompleted(chosenDevice)
                }
            }
            searchStrategy.release()
        })
    }

    fun connectToTheGate(
            device: BlueteethDevice?,
            gatt: BaseGatt,
            connChangedListener: OnConnectionChangedListener,
            listener: ConnectionListener) {
        if (device != null) {
            val connectionCallbackCalled = booleanArrayOf(false)
            val discoveryCallbackCalled = booleanArrayOf(false)
            val explicitConnectionCheck = Runnable {
                if (!connectionCallbackCalled[0]) {
                    //device.disconnect();
                    uiHandler.post { listener.onTimeout(context.getString(R.string.bluetooth_connection_failed)) }
                }
            }
            val explicitDiscoveryCheck = Runnable {
                if (!discoveryCallbackCalled[0]) {
                    //device.disconnect();
                    uiHandler.post { listener.onTimeout(context.getString(R.string.bluetooth_service_discovery_failed)) }
                }
            }
            device.connect(false, gatt) { thisDevice, isConnected ->
                Log.d(TAG, "Connected to ${thisDevice.name}: $isConnected")
                connChangedListener.call(thisDevice, isConnected)
                if (isConnected) {
                    uiHandler.removeCallbacks(explicitConnectionCheck)

                    connectionCallbackCalled[0] = true
                    uiHandler.post { listener.onGateConnection(thisDevice, true) }
                    thisDevice.discoverServices { response ->
                        uiHandler.removeCallbacks(explicitDiscoveryCheck)
                        discoveryCallbackCalled[0] = true

                        if (response == BlueteethDevice.BlueteethResponse.NO_ERROR) {
                            Log.d(TAG, "Services discovered.")
                            uiHandler.post { listener.onServicesDiscovered(thisDevice) }
                        } else {
                            uiHandler.post { listener.onError(context.getString(R.string.bluetooth_service_discovery_failed)) }
                        }
                    }
                    uiHandler.postDelayed(explicitDiscoveryCheck, bleOptions.discoveringTimeout)
                } else {
                    connectionCallbackCalled[0] = true
                    uiHandler.removeCallbacks(explicitConnectionCheck)
                    discoveryCallbackCalled[0] = true
                    uiHandler.removeCallbacks(explicitDiscoveryCheck)
                    uiHandler.post { listener.onGateConnection(thisDevice, false) }
                }
            }
            uiHandler.postDelayed(explicitConnectionCheck, bleOptions.connectionTimeout)
        } else
            uiHandler.post { listener.onError(context.getString(R.string.bluetooth_connection_failed)) }
    }

    fun authenticate(
            device: BlueteethDevice?,
            listener: AuthListener) {
        if (BaseModule.okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            device?.let { dvc ->
                sendData(dvc, listener)
            }
        }
    }

    fun readDeviceInfo(
            device: BlueteethDevice?,
            listener: DeviceInfoReadListener) {
        if (okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            device?.let { dvc ->
                readPowerLevel(dvc, listener) {
                    readMovementTimeout(dvc, listener) {
                        readFirmwareVersion(dvc, listener)
                    }
                }
            }
        }
    }

    private fun sendData(
            device: BlueteethDevice,
            listener: AuthListener) {
        if (bleOptions.isReaderPowerChaned || bleOptions.isReaderTimeoutByMovementChanged) {
            writePower(device, listener) {
                writeTimeout(device, listener) {
                    uiHandler.postDelayed({
                        listener.onAuthResponse(Enums.GateStatus.DATA_SET)
                        sendCode(device, listener)
                    }, 2200)
                }
            }
        } else {
            sendCode(device, listener)
        }
    }

    private fun sendCode(
            device: BlueteethDevice,
            listener: AuthListener) {
        val bytesToWrite = BluetoothServices.BarrierControl.prepareCodeCommand(bleOptions.readerCode, bleOptions.wiegandVersion)
        traceGateResponse(device, listener, false) { data ->
            if (BluetoothServices.BarrierControl.isResponseOk(data)) {
                Log.d(TAG, "Code ${bleOptions.readerCode} wrote")
                listener.onAuthResponse(Enums.GateStatus.SUCCESS)
            } else {
                listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
            }
        }
        createCommandWriteOperation(device, bytesToWrite, listener)
    }

    private fun writePower(
            device: BlueteethDevice,
            listener: ErrorListener,
            next: () -> Unit) {
        val powerBytes = BluetoothServices.BarrierControl.prepareSetTxBytes(bleOptions.readerPower)
        traceGateResponse(device, listener) { data ->
            if (BluetoothServices.BarrierControl.isResponseOk(data)) {
                Log.d(TAG, "Power ${bleOptions.readerPower} wrote")
                uiHandler.postDelayed({ next.invoke() }, DEFAULT_WRITE_DELAY)
            } else {
                listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
            }
        }
        createCommandWriteOperation(device, powerBytes, listener)
    }

    private fun writeTimeout(
            device: BlueteethDevice,
            listener: ErrorListener,
            next: () -> Unit) {
        val timeoutBytes = BluetoothServices.BarrierControl.prepareSetMovementTimeoutBytes(bleOptions.readerTimeoutByMovement)
        traceGateResponse(device, listener) { data ->
            if (BluetoothServices.BarrierControl.isResponseOk(data)) {
                Log.d(TAG, "Timeout ${bleOptions.readerTimeoutByMovement} wrote")
                uiHandler.postDelayed({ next.invoke() }, DEFAULT_WRITE_DELAY)
            } else {
                listener.onError(context.getString(R.string.bluetooth_write_operation_failed))
            }
        }
        createCommandWriteOperation(device, timeoutBytes, listener)
    }

    private fun readPowerLevel(
            device: BlueteethDevice,
            listener: DeviceInfoReadListener,
            next: () -> Unit) {
        val powerBytes = BluetoothServices.BarrierControl.getCommandBytes(BluetoothServices.BarrierControl.COMMAND_READ_TEX)
        traceGateResponse(device, listener) { data ->
            val powerLevel = BluetoothServices.BarrierControl.readTxBytes(data)
            listener.onTxRead(powerLevel)
            Log.d(TAG, "Power level read: $powerLevel")
            uiHandler.postDelayed({ next.invoke() }, DEFAULT_READ_DELAY)
        }
        createCommandWriteOperation(device, powerBytes, listener)
    }

    private fun readMovementTimeout(
            device: BlueteethDevice,
            listener: DeviceInfoReadListener,
            next: () -> Unit) {
        val timeoutBytes = BluetoothServices.BarrierControl.getCommandBytes(BluetoothServices.BarrierControl.COMMAND_READ_MOVEMENT_TIMEOUT)
        traceGateResponse(device, listener) { data ->
            val timeout = BluetoothServices.BarrierControl.readMovementTimeoutBytes(data)
            listener.onTimeoutByMovementRead(timeout)
            Log.d(TAG, "Movement Timeout read: $timeout")
            uiHandler.postDelayed({ next.invoke() }, DEFAULT_READ_DELAY)
        }
        createCommandWriteOperation(device, timeoutBytes, listener)
    }

    private fun readFirmwareVersion(
            device: BlueteethDevice,
            listener: DeviceInfoReadListener) {
        val fwVersionBytes = BluetoothServices.BarrierControl.getCommandBytes(BluetoothServices.BarrierControl.COMMAND_READ_FIRMWARE)
        traceGateResponse(device, listener) { data ->
            val versionCodes = BluetoothServices.BarrierControl.readFirmwareBytes(data)
            Log.d(TAG, "FW Version read: ${versionCodes[0]}.${versionCodes[1]}")
            listener.onFirmwareVersionRead(versionCodes[0], versionCodes[1])
            listener.onCompele()
        }
        createCommandWriteOperation(device, fwVersionBytes, listener)
    }

    private fun traceGateResponse(
            device: BlueteethDevice,
            listener: ErrorListener,
            disableNotification: Boolean = true,
            onRead: (ByteArray) -> Unit) {
        if (okToProceed(device, context.getString(R.string.bluetooth_operation_timeout), listener)) {
            val callbackCalled = booleanArrayOf(false)
            val explicitCheck = Runnable {
                uiHandler.post {
                    if (!callbackCalled[0]) {
                        //checkGateResponse(context, device, listener);
                        listener.onTimeout(context.getString(R.string.bluetooth_operation_timeout))
                    }
                }
            }
            val notifOp = GattOperation.createNotification(
                    device,
                    BluetoothServices.BarrierControl.SERVICE_UUID,
                    BluetoothServices.BarrierControl.RESPONSE,
                    true,
                    object : GattCharacteristicReadCallback {
                        override fun onRead(characteristic: ByteArray) {
                            callbackCalled[0] = true
                            uiHandler.removeCallbacks(explicitCheck)
                            uiHandler.post { onRead.invoke(characteristic) }
                            if (disableNotification) {
                                // remove subscription to notification
                                val notifOffOp = GattOperation.createNotification(
                                        device,
                                        BluetoothServices.BarrierControl.SERVICE_UUID,
                                        BluetoothServices.BarrierControl.RESPONSE,
                                        false,
                                        null)
                                val notifOffBundle = GattOperationBundle(
                                        SimpleBundleListener(context, uiHandler, listener), notifOffOp)
                                device.queueBundle(notifOffBundle)
                            }
                        }

                        override fun onTimeout() = Unit

                        override fun onError(errMessage: String) {
                            callbackCalled[0] = true
                            uiHandler.removeCallbacks(explicitCheck)
                            uiHandler.post { listener.onError(errMessage) }
                        }
                    })

            val notifBundle = GattOperationBundle(SimpleBundleListener(context, uiHandler, listener), notifOp)
            device.queueBundle(notifBundle)

            uiHandler.postDelayed(explicitCheck, bleOptions.connectionTimeout)
        }
    }

    private fun createCommandWriteOperation(
            device: BlueteethDevice,
            bytesToWrite: ByteArray,
            listener: ErrorListener) {
        val codeOp = GattOperation.createWriteCharacteristic(
                device,
                bytesToWrite,
                BluetoothServices.BarrierControl.SERVICE_UUID,
                BluetoothServices.BarrierControl.COMMAND,
                BluetoothServices.BarrierControl.COMMAND_BYTES_COUNT,
                true, null)
        val bundle = GattOperationBundle(
                SimpleBundleListener(context, uiHandler, listener), codeOp)
        device.queueBundle(bundle)
    }
}