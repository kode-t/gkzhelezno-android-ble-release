package ru.majordom.bleservice.bluetooth.device.interaction;


import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;

import java.util.UUID;

public abstract class BaseGatt {
    protected BluetoothGattCallback callback;
    protected BluetoothGatt gatt;

    public BaseGatt connect(Context context, boolean autoConnect, BlueteethDevice device, BluetoothGattCallback callback) {
        this.callback = callback;
        gatt = device.getBluetoothDevice().connectGatt(context, autoConnect, callback);
        return this;
    }

    public boolean readRemoteRssi() {
        callback.onReadRemoteRssi(null, 0, BluetoothGatt.GATT_SUCCESS);
        return true;
    }

    /**
     * Callback is called
     */
    public void disconnect() {
        //
    }

    /**
     * Callback is not called
     */
    public void close() {
        //
    }

    public boolean discoverServices() {
        callback.onServicesDiscovered(gatt, BluetoothGatt.GATT_SUCCESS);
        return true;
    }

    public BluetoothGattService getService(UUID serviceUuid) {
        return gatt.getService(serviceUuid);
    }

    public boolean readCharacteristic(BluetoothGattCharacteristic characteristic) {
        callback.onCharacteristicRead(gatt, characteristic, BluetoothGatt.GATT_SUCCESS);
        return true;
    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        callback.onCharacteristicWrite(gatt, characteristic, BluetoothGatt.GATT_SUCCESS);
        return true;
    }

    public boolean readDescriptor(BluetoothGattDescriptor descriptor) {
        callback.onDescriptorRead(gatt, descriptor, BluetoothGatt.GATT_SUCCESS);
        return true;
    }

    public boolean writeDescriptor(BluetoothGattDescriptor descriptor) {
        callback.onDescriptorWrite(gatt, descriptor, BluetoothGatt.GATT_SUCCESS);
        return true;
    }

    public boolean setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enable) {
        return true;
    }
}
