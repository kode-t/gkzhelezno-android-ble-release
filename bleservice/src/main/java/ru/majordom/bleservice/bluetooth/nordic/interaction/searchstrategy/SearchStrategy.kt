package ru.majordom.bleservice.bluetooth.nordic.interaction.searchstrategy

import no.nordicsemi.android.support.v18.scanner.ScanResult

interface SearchStrategy {
    val defaultScanTimeout: Long
    val isSingleDeviceScan: Boolean
    fun checkDeviceFound(device: ScanResult): Boolean
    fun pickTheMostSuitable(): ScanResult?
    fun release()
}