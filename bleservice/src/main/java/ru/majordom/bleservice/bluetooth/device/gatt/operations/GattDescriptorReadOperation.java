package ru.majordom.bleservice.bluetooth.device.gatt.operations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.util.Log;

import java.util.UUID;

import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.GattCharacteristicReadCallback;
import ru.majordom.bleservice.bluetooth.device.interaction.BaseGatt;
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

public class GattDescriptorReadOperation extends GattOperation {
    private final UUID descriptor;
    private final GattCharacteristicReadCallback callback;

    GattDescriptorReadOperation(BlueteethDevice device, UUID service, UUID characteristic, UUID descriptor, GattCharacteristicReadCallback callback) {
        super(device, service, characteristic);
        this.descriptor = descriptor;
        this.callback = callback;
        hasAvailableCompletionCallback = true;
    }

    @Override
    public void execute() {
        Log.d(TAG, "Reading from {" + descriptor + "}");
        BaseGatt gatt = device.getBluetoothGatt();
        BluetoothGattCharacteristic gattCharacteristic = getCharacteristic(gatt);
        if (gattCharacteristic != null) {
            BluetoothGattDescriptor descriptor = gattCharacteristic.getDescriptor(this.descriptor);
            gatt.readDescriptor(descriptor);
        }
    }

    @Override
    public void onTimeout() {
        super.onTimeout();
        if (callback != null) {
            callback.onTimeout();
        }
    }

    @Override
    public void onError(String errorMsg) {
        super.onError(errorMsg);
        if (callback != null) {
            callback.onError(errorMsg);
        }
    }

    public void onRead(BluetoothGattDescriptor descriptor) {
        if (callback != null) {
            callback.onRead(descriptor.getValue());
        }
        super.onOperationCompleted();
    }
}