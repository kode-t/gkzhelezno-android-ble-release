package ru.majordom.bleservice.bluetooth.device.listeners;

public interface OnBondingChangedListener {
    void call(boolean isBonded);
}
