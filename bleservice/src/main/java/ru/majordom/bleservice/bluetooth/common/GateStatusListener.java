package ru.majordom.bleservice.bluetooth.common;

public interface GateStatusListener extends ErrorListener {
    void onStatusReceived(Enums.GateStatus status);
}
