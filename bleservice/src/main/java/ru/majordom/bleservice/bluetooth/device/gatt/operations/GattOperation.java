package ru.majordom.bleservice.bluetooth.device.gatt.operations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.UUID;

import ru.majordom.bleservice.bluetooth.device.gatt.GattOperationBundle;
import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.GattCharacteristicReadCallback;
import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.GattCharacteristicWriteCallback;
import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.GattErrorListener;
import ru.majordom.bleservice.bluetooth.device.interaction.BaseGatt;
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

public abstract class GattOperation implements GattErrorListener {
    public static final int ONE_SECOND_DELAY = 1000;
    public static final int FIRMWARE_STATUS_READ_DELAY = 5000;
    public static final int DEFAULT_TIMEOUT_IN_MILLS = 10000;
    public static final int WWOR_DELAY = 15;

    protected final String TAG;
    protected final UUID service;
    protected final UUID characteristic;
    protected boolean hasAvailableCompletionCallback;
    protected BlueteethDevice device;
    protected GattOperationBundle bundle;
    protected int delay;
    protected String lastInternalCheckError;

    protected GattOperation(@NonNull BlueteethDevice device, @NonNull UUID service, @NonNull UUID characteristic) {
        this.device = device;
        this.TAG = this.getClass().getSimpleName();
        this.service = service;
        this.characteristic = characteristic;
        this.delay = 0;
    }

    @Override
    public void onTimeout() {
        if (bundle != null) {
            bundle.onTimeout();
        }
    }

    @Override
    public void onError(String errorMsg) {
        if (bundle != null) {
            bundle.onError(errorMsg);
        }
    }

    @Override
    public String toString() {
        return "GattOperation{" +
                "characteristic=" + characteristic +
                '}';
    }

    public abstract void execute();

    public boolean hasAvailableCompletionCallback() {
        return hasAvailableCompletionCallback;
    }

    public GattOperationBundle getBundle() {
        return bundle;
    }

    public void setBundle(GattOperationBundle bundle) {
        this.bundle = bundle;
    }

    public int getDelay() {
        return delay;
    }

    public String getLastInternalCheckError() {
        return lastInternalCheckError;
    }

    public BlueteethDevice getDevice() {
        return device;
    }

    /**
     * Sets execution delay of an operation.
     * @param delay in ms
     */
    public void setDelay(int delay) {
        if (delay > 0) {
            this.delay = delay;
        }
    }

    /**
     * Call to inform bundle that operation is complete.
     */
    protected void onOperationCompleted() {
        if (bundle != null) {
            bundle.removeOperation(this);
        }
    }

    public boolean canExecute() {
        return device != null && device.getBluetoothGatt() != null;
    }

    protected BluetoothGattCharacteristic getCharacteristic(BaseGatt gatt) {
        if (gatt == null) {
            Log.w(TAG, "GATT is null");
            return null;
        }

        BluetoothGattService gattService = gatt.getService(service);
        if (gattService == null) {
            Log.e(TAG, "Service not available - " + service.toString());
            return null;
        }

        BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(characteristic);
        if (gattCharacteristic == null) {
            Log.e(TAG, "Characteristic not available - " + characteristic.toString());
            return null;
        }
        return gattCharacteristic;
    }

    public static GattCharacteristicReadOperation createReadCharacteristic(@NonNull BlueteethDevice device, @NonNull UUID service, @NonNull UUID characteristic, int bytesCount, @Nullable GattCharacteristicReadCallback callback) {
        return new GattCharacteristicReadOperation(device, service, characteristic, bytesCount, callback);
    }

    public static GattCharacteristicWriteOperation createWriteCharacteristic(@NonNull BlueteethDevice device, @NonNull byte[] data, @NonNull UUID service, @NonNull UUID characteristic, int bytesCount, boolean withResponse, @Nullable GattCharacteristicWriteCallback callback) {
        return new GattCharacteristicWriteOperation(device, service, characteristic, bytesCount, data, withResponse, callback);
    }

    public static GattSetNotificationOperation createNotification(@NonNull BlueteethDevice device, @NonNull UUID service, @NonNull final UUID characteristic, boolean enable, final GattCharacteristicReadCallback callback) {
        GattSetNotificationOperation operation = new GattSetNotificationOperation(device, service, characteristic, enable, callback);
        device.registerNotification(operation, characteristic, enable);
        return operation;
    }
}