package ru.majordom.bleservice.bluetooth.device.gatt.callbacks;

public interface GattCharacteristicWriteCallback extends GattErrorListener {
    void onWrite();
}
