package ru.majordom.bleservice.bluetooth.device.listeners;

import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

public interface OnConnectionChangedListener {
    void call(BlueteethDevice thisDevice, boolean isConnected);
}
