package ru.majordom.bleservice.bluetooth

import android.bluetooth.BluetoothAdapter
import android.content.Context
import ru.majordom.bleservice.bluetooth.bleService.BluetoothStateReceiver
import java.util.*

class BluetoothManager internal constructor(
        context: Context
) : BluetoothManagerContract, BluetoothStateReceiver.BluetoothStateListener {

    private val listeners = HashSet<BluetoothStateReceiver.BluetoothStateListener>(3)
    override val bleAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    override val isBluetoothEnabled: Boolean
        get() = bleAdapter != null && bleAdapter.isEnabled

    init {
        context.registerReceiver(BluetoothStateReceiver(this), BluetoothStateReceiver.getIntentFilter())
    }

    override fun addBluetoothStateListener(listener: BluetoothStateReceiver.BluetoothStateListener) {
        listeners.add(listener)
    }

    override fun removeBluetoothStateListener(listener: BluetoothStateReceiver.BluetoothStateListener) {
        listeners.remove(listener)
    }

    override fun enableBluetooth(enable: Boolean) {
        if (enable) bleAdapter?.enable()
        else bleAdapter?.disable()
    }

    override fun onBluetoothAvailable() {
        for (listener in listeners) {
            listener.onBluetoothAvailable()
        }
    }

    override fun onBluetoothUnavailable() {
        for (listener in listeners) {
            listener.onBluetoothUnavailable()
        }
    }
}