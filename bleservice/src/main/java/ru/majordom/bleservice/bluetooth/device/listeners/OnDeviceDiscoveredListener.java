package ru.majordom.bleservice.bluetooth.device.listeners;


import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

public interface OnDeviceDiscoveredListener {
    void call(BlueteethDevice blueteethDevice);
}
