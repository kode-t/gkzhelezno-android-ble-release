package ru.majordom.bleservice.bluetooth.bleService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;


public class BleReceiver extends BroadcastReceiver {

    public static final String ACTION_GATE_STATUS = "app.action_device_ready";
    public static final String KEY_GATE_STATUS = "app.extra_status";
    public static final String ACTION_LOG_STATUS = "app.action_log";
    public static final String KEY_LOG_STATUS = "app.extra_log";

    private final BleCallbackListener listener;

    public BleReceiver(BleCallbackListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent != null) {
            String action = intent.getAction();
            switch (action) {
                case ACTION_GATE_STATUS:
                    GateState state = (GateState) intent.getSerializableExtra(KEY_GATE_STATUS);
                    Log.d("BleREceiver", state.toString());
                    if (listener != null) listener.onGateStatus(state);
                    break;
                case ACTION_LOG_STATUS:
                    String message = intent.getStringExtra(KEY_LOG_STATUS);
                    if (listener != null) listener.onLogData(message);
                    break;
            }
        }
    }

    public interface BleCallbackListener {
        void onGateStatus(GateState state);
        void onLogData(String log);
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_GATE_STATUS);
        intentFilter.addAction(ACTION_LOG_STATUS);
        return intentFilter;
    }
}
