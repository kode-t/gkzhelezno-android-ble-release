package ru.majordom.bleservice.bluetooth;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

public class BluetoothUtils {
    private final static String TAG = BluetoothUtils.class.getSimpleName();
    private final static char[] HEX_ARRAY = "0123456789abcdef".toCharArray();
    private final static int BYTES_IN_INTEGER = 4;

    public static byte[] intToBytes(int integer, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.allocate(BYTES_IN_INTEGER)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
                .putInt(integer);
        return bb.array();
    }

    public static byte[] longToBytes(long longValue, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.allocate(BYTES_IN_INTEGER * 2)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
                .putLong(longValue);
        return bb.array();
    }

    public static byte[] shortToBytes(short value, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.allocate(2)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
                .putShort(value);
        return bb.array();
    }

    public static byte[] intToSingleByteArray(int integer) {
        return new byte[]{(byte) integer};
    }

    public static int bytesToInt(byte[] bytes) {
        return bytesToInt(bytes, false);
    }

    public static int bytesToIntReversed(byte[] bytes) {
        return bytesToInt(bytes, true);
    }

    public static short bytesToShort(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        if (bb.position() < bb.limit()) {
            return bb.get();
        }
        return bb.getShort();
    }

    public static UUID bytesTosUuid(byte[] bytes, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.wrap(bytes)
                .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(littleEndian ? secondLong : firstLong, littleEndian ? firstLong : secondLong);
    }

    public static byte[] removeZeroes(byte[] bytes, boolean littleEndian) {
        int zeroesCount = 0;
        if (littleEndian) {
            int i = bytes.length - 1;
            while (bytes[i] == 0) {
                zeroesCount++;
                i--;
            }
            byte[] resultingBytes = new byte[bytes.length - zeroesCount];
            System.arraycopy(bytes, 0, resultingBytes, 0, resultingBytes.length);
            return resultingBytes;
        } else {
            int i = 0;
            while (bytes[i] == 0) {
                zeroesCount++;
                i++;
            }
            byte[] resultingBytes = new byte[bytes.length - zeroesCount];
            System.arraycopy(bytes, zeroesCount, resultingBytes, 0, resultingBytes.length);
            return resultingBytes;
        }
    }

    public static byte[] uuidToBytes(UUID uuid, boolean littleEndian) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        byte[] byteArray = bb.array();
        if (littleEndian) {
            reverseByteArray(byteArray);
        }
        return byteArray;
    }

    public static byte[] hexStringToBytes(String hexString) {
        int len = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i + 1), 16));
        }
        return data;
    }

    public static String bytesToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = convertByte(bytes[j]);
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static void reverseByteArray(byte[] data) {
        for (int i = 0; i < data.length / 2; i++) {
            byte temp = data[i];
            data[i] = data[data.length - i - 1];
            data[data.length - i - 1] = temp;
        }
    }

    public static Character byteToChar(byte b) {
        return (char) convertByte(b);
    }

    private static int bytesToInt(byte[] bytes, boolean reversed) {
        if (bytes.length == 1) {
            return convertByte(bytes[0]);
        }
        if (reversed) {
            reverseByteArray(bytes);
        }
        ByteBuffer bb;
        if (bytes.length == BYTES_IN_INTEGER) {
            bb = ByteBuffer.wrap(bytes);
        } else {
            bb = ByteBuffer.allocate(BYTES_IN_INTEGER).order(ByteOrder.LITTLE_ENDIAN);
            for (int i = 0; i < BYTES_IN_INTEGER; i++) {
                bb.put(i < bytes.length ? bytes[i] : (byte) 0);
            }
            bb.position(0);
        }
        return bb.getInt();
    }

    /**
     * Converts byte to int using '& 0xFF'
     *
     * @param b
     * @return
     */
    private static int convertByte(byte b) {
        return b & 0xFF;
    }
}