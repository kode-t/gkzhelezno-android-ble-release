package ru.majordom.bleservice.bluetooth.device.gatt.operations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.util.Log;

import java.util.UUID;

import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.GattCharacteristicWriteCallback;
import ru.majordom.bleservice.bluetooth.device.interaction.BaseGatt;
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

public class GattDescriptorWriteOperation extends GattOperation {
    private final UUID descriptor;
    private final GattCharacteristicWriteCallback callback;

    GattDescriptorWriteOperation(BlueteethDevice device, UUID service, UUID characteristic, UUID descriptor, GattCharacteristicWriteCallback callback) {
        super(device, service, characteristic);
        this.descriptor = descriptor;
        this.callback = callback;
        hasAvailableCompletionCallback = true;
    }

    @Override
    public void execute() {
        Log.d(TAG, "Writing to {" + descriptor + "}");
        BaseGatt gatt = device.getBluetoothGatt();
        final BluetoothGattCharacteristic gattCharacteristic = getCharacteristic(gatt);
        if (gattCharacteristic != null) {
            BluetoothGattDescriptor descriptor = gattCharacteristic.getDescriptor(this.descriptor);
            gatt.writeDescriptor(descriptor);
        }
    }

    @Override
    public void onTimeout() {
        super.onTimeout();
        if (callback != null) {
            callback.onTimeout();
        }
    }

    @Override
    public void onError(String errorMsg) {
        super.onError(errorMsg);
        if (callback != null) {
            callback.onError(errorMsg);
        }
    }

    public void onWrite() {
        if (callback != null) {
            callback.onWrite();
        }
        super.onOperationCompleted();
    }
}