package ru.majordom.bleservice.bluetooth

import android.bluetooth.BluetoothAdapter
import ru.majordom.bleservice.bluetooth.bleService.BluetoothStateReceiver

interface BluetoothManagerContract {
    val isBluetoothEnabled: Boolean
    val bleAdapter: BluetoothAdapter?
    fun addBluetoothStateListener(listener: BluetoothStateReceiver.BluetoothStateListener)
    fun removeBluetoothStateListener(listener: BluetoothStateReceiver.BluetoothStateListener)
    fun enableBluetooth(enable: Boolean)
}
