package ru.majordom.bleservice.bluetooth.interaction.search_strategy


import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethManager
import ru.majordom.bleservice.common.BleOptions

class FirsDeviceSearchStrategy(
        manager: BlueteethManager,
        bleOptions: BleOptions
) : BaseSearchStrategy(manager, bleOptions) {
    override val isSingleDeviceScan: Boolean
        get() = true

    override fun compareWhichIsBetter(selectedDevice: BlueteethDevice?, currentDevice: BlueteethDevice): BlueteethDevice {
        return selectedDevice ?: currentDevice
    }
}
