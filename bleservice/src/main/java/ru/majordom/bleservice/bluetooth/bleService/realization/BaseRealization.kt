package ru.majordom.bleservice.bluetooth.bleService.realization

import android.content.Context
import android.support.annotation.IntRange
import android.support.annotation.StringRes
import android.util.Log
import ru.majordom.bleservice.R
import ru.majordom.bleservice.bluetooth.BluetoothConstants
import ru.majordom.bleservice.bluetooth.BluetoothConstants.*
import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.bluetooth.bleService.GateState
import ru.majordom.bleservice.bluetooth.common.AuthListener
import ru.majordom.bleservice.bluetooth.common.CommonBleDevice
import ru.majordom.bleservice.bluetooth.common.DeviceInfoReadListener
import ru.majordom.bleservice.bluetooth.common.Enums
import ru.majordom.bleservice.common.LocalStorage
import java.util.*

abstract class BaseRealization<Manager : Any, Device : Any> protected constructor(
        protected val context: Context,
        protected val bluetoothManager: BluetoothManagerContract,
        protected val listener: Listener,
        protected val localStorage: LocalStorage
) {
    protected var TAG: String = this.javaClass.simpleName

    protected val PAUSE_DEFAULT: Long
    protected val PAUSE_CONN_AUTH_ERROR: Long
    protected val PAUSE_AFTER_TECHNICAL_ISSUE: Long
    protected val PAUSE_AFTER_NETWORK_ISSUE: Long
    private val MAX_DISCONNECTS_COUNT = 2
    protected var currentDisconnectCount = 0

    internal lateinit var bleManager: Manager
    internal var deviceWrapper: CommonBleDevice<Device>? = null

    var gateState: GateState
        internal set
    var isInScanMode: Boolean = false
        internal set
    internal var gateWasOpened: Boolean = false
    internal var gateErrorCodeReceived: Boolean = false
    internal var isReleased: Boolean = false
    internal var openedGates: HashMap<String, Long>
    protected var lastActiveSessionEventTime: Long = 0
    protected var hasScanningAppsInRange: Boolean = false
    protected abstract val realizationInfo: String

    internal val isAutomatic: Boolean
        get() = localStorage.isAutoOpeningOn

    init {
        gateState = GateState()
        openedGates = HashMap()
        localStorage.bleOptions.apply {
            PAUSE_DEFAULT = pauseDefault
            PAUSE_CONN_AUTH_ERROR = pauseConnAuthError
            PAUSE_AFTER_TECHNICAL_ISSUE = pauseTech
            PAUSE_AFTER_NETWORK_ISSUE = pauseNetwork
        }
        listener.onLogGlobal(false, realizationInfo)
        listener.onLogGlobal(false, localStorage.bleOptions.printBlePrefs())
    }

    protected val defaultAuthListener = object : AuthListener {
        override fun onAuthResponse(status: Enums.GateStatus) {
            gateState.ready()
            listener.onSendMessage(R.string.gate_msg_authenticated)
            processGateResponse(status)
        }

        override fun onError(errorMsg: String) {
            listener.apply {
                onSendMessage(errorMsg)
                onDisconnectFromDevice()
                onResumeConnection(PAUSE_CONN_AUTH_ERROR)
            }
        }

        override fun onTimeout(errorMsg: String) {
            listener.apply {
                onNotificationTimeout(errorMsg)
                onSendMessage(errorMsg)
                onDisconnectFromDevice()
                onResumeConnection(PAUSE_CONN_AUTH_ERROR)
            }
        }
    }

    protected val defaultDeviceReadListener = object : DeviceInfoReadListener {
        override fun onTxRead(txLevel: Int) {
            localStorage.bleOptions.readerPower = txLevel
        }

        override fun onTimeoutByMovementRead(timeout: Int) {
            localStorage.bleOptions.readerTimeoutByMovement = timeout
        }

        override fun onFirmwareVersionRead(major: Int, minor: Int) {
            localStorage.bleOptions.readerFirmwareVersion = "$major.$minor"
        }

        override fun onCompele() = listener.onDeviceSettingsRead()

        override fun onError(errorMsg: String) {
            listener.apply {
                onLogGlobal(true, errorMsg)
                onDisconnectFromDevice()
                onResumeConnection(PAUSE_CONN_AUTH_ERROR)
            }
        }

        override fun onTimeout(errorMsg: String) = this.onError(errorMsg)
    }

    fun startNewScan() {
        if (!isInScanMode) {
            Log.d(TAG, "Starting new BLE scan.")
            isInScanMode = true
            listener.onResumeConnection(0)
        }
    }

    fun hasScanningAppsInRange(): Boolean {
        return hasScanningAppsInRange
    }

    protected abstract fun onSearchForGate(listener: Listener)
    protected abstract fun onConnectAndOpenGate()
    protected abstract fun onAuthProceed()
    protected abstract fun onDisconnectFromDevice(): Boolean
    protected abstract fun readDeviceSettings()

    protected fun processFoundDevice(deviceWrapper: CommonBleDevice<Device>) {
        listener.onDeviceFound(deviceWrapper.name + ", device RSSI = " + deviceWrapper.rssi)
        if (deviceWrapper.rssi < localStorage.bleOptions.minRssiLevel) {
            listener.onLowRssiLevel()
            return
        }
        listener.onGoodRssiLevel()

        this.deviceWrapper?.apply {
            // if new deviceWrapper is not the same as previously found
            if (this.macAddress != deviceWrapper.macAddress) {
                listener.onDisconnectFromDevice()
            }
        }
        this.deviceWrapper = deviceWrapper

        if (!localStorage.bleOptions.readerSettingsRead) {
            gateState.isOpeningInProcess = true
            readDeviceSettings()
            return
        }

        gateState.isInRange = true
        if (localStorage.isDebug) {
            gateState.gateName = this.deviceWrapper?.name + ", device RSSI = " + deviceWrapper.rssi
        }
        listener.apply {
            onSendGateStatus()
            onLogGlobal(false, "Automatic reader opening = $isAutomatic")
            onDeviceIsCheckedAndOk()
        }
        if (isAutomatic && canOpenInAutomaticMode()) {
            Log.d(TAG, "Automatic reader opening is enabled, connecting")
            listener.onAutoOpenAfterChecks(doSendOpeningNotification(deviceWrapper.macAddress))
        } else {
            listener.onNoAutoOpenAfterChecks()
            listener.onResumeConnection(PAUSE_DEFAULT)
        }
    }

    protected open fun processConnPriorityLogic() {
        if (currentDisconnectCount == MAX_DISCONNECTS_COUNT - 1) {
            val options = localStorage.bleOptions
            var currentConnPriority = options.connPriority
            when (currentConnPriority) {
                BluetoothConstants.CONN_PRIORITY_HIGH -> currentConnPriority = BluetoothConstants.CONN_PRIORITY_MID
                BluetoothConstants.CONN_PRIORITY_MID -> currentConnPriority = BluetoothConstants.CONN_PRIORITY_LOW
                BluetoothConstants.CONN_PRIORITY_LOW -> currentConnPriority = BluetoothConstants.CONN_PRIORITY_HIGH
            }
            currentDisconnectCount = 0
            options.connPriority = currentConnPriority
            listener.onConnectionIntervalChanged(currentConnPriority)
        } else
            currentDisconnectCount++
    }

    open fun release() {
        isReleased = true
        deviceWrapper = null
    }

    open fun stopScan() {
        listener.onLogGlobal(true, "Stopping scan.")
        isInScanMode = false
        lastActiveSessionEventTime = 0
        listener.onLogSessionEnded()
    }

    fun disconnectFromDevice() {
        if (deviceWrapper != null) {
            Log.d(TAG, "Disconnecting from current device.")
            val wasConnected = onDisconnectFromDevice()
            if (wasConnected) listener.onLogGlobal(true, "Disconnecting.")
        }
        disconnectInternal()
    }

    internal fun onDeviceFound(name: String) {
        if (lastActiveSessionEventTime == 0L) {
            Log.d(TAG, "Server log session started.")
            lastActiveSessionEventTime = System.currentTimeMillis()
            listener.onCreateNewLogSession(name)
            listener.onLogSessionOnly(false, realizationInfo)
        } else
            lastActiveSessionEventTime = System.currentTimeMillis()
    }

    internal fun onNoDeviceFoundInternal() {
        listener.onNoDeviceFound()

        if (lastActiveSessionEventTime > 0 && lastActiveSessionEventTime + SESSION_DURATION < System.currentTimeMillis()) {
            Log.d(TAG, "Server log session ended.")
            lastActiveSessionEventTime = 0
            listener.onLogSessionEnded()
        }
    }

    protected fun disconnectInternal() {
        gateState.disconnect()
        listener.onSendGateStatus()
    }

    fun deviceChosen(): Boolean {
        return deviceWrapper != null
    }

    fun searchForGate() {
        if (!isInScanMode || gateState.isOpeningInProcess) return
        if (bluetoothManager.isBluetoothEnabled) {
            gateWasOpened = false
            gateErrorCodeReceived = false
            Log.d(TAG, "Connection runner in process.")
            listener.onLogGlobal(true, "Searching for readers.")
            onSearchForGate(listener)
        } else {
            stopScan()
        }
    }

    fun connectAndOpenGate() {
        if (!gateState.isOpeningInProcess) {
            gateState.isOpeningInProcess = true
            listener.onSendMessage(R.string.gate_msg_connecting)
            listener.onLogGlobal(true, "Connecting to the reader.")
            onConnectAndOpenGate()
        }
    }

    internal fun canOpenInAutomaticMode(): Boolean = listener.canOpenInAutomaticMode()

    internal fun authGateUser() {
        listener.onSendMessage(R.string.gate_msg_authenticating)
        listener.onLogGlobal(true, "Authenticating.")
        onAuthProceed()
    }

    internal fun processGateResponse(status: Enums.GateStatus) {
        listener.onLogGlobal(true, "Response received: " + status.name)
        gateErrorCodeReceived = true
        var timeout: Long = 0
        when (status) {
            Enums.GateStatus.SUCCESS -> {
                gateWasOpened = true
                gateErrorCodeReceived = false
                gateState.isOpened = true
                listener.onGateOpened()
                listener.onSendMessage(R.string.gate_msg_opened)
                timeout = PAUSE_DEFAULT
            }
            Enums.GateStatus.DATA_SET -> {
                localStorage.bleOptions.isReaderPowerChaned = false
                localStorage.bleOptions.isReaderTimeoutByMovementChanged = false
                listener.onSendMessage(R.string.gate_msg_tx_set)
                return  // do not disconnect
            }
        }
        disconnectFromDevice()
        // making an interval of silence
        //acquirePartialWakeLock(PAUSE_AFTER_GATE_RESPONSE + ADDITIONAL_PERIOD);
        listener.onResumeConnection(timeout)
    }

    internal fun doSendOpeningNotification(deviceMac: String): Boolean {
        val currentTime = System.currentTimeMillis()
        return if (!openedGates.containsKey(deviceMac) || currentTime - (openedGates[deviceMac]
                        ?: 0L) >= ONE_HOUR) {
            openedGates[deviceMac] = currentTime
            true
        } else {
            val keysToDelete = ArrayList<String>()
            for ((key, value) in openedGates) {
                if (currentTime - value >= ONE_HOUR) {
                    keysToDelete.add(key)
                }
            }
            for (key in keysToDelete) {
                openedGates.remove(key)
            }
            false
        }
    }

    interface Listener {
        fun onStopConnection()

        /**
         * Device found, create new server log session
         * @param deviceName
         */
        fun onCreateNewLogSession(deviceName: String)

        /**
         * Device is lost or scan has stopped, end server log session.
         */
        fun onLogSessionEnded()

        /**
         * Found device with given search strategy.
         * @param name
         */
        fun onDeviceFound(name: String)

        /**
         * Device is found and search has stopped.
         */
        fun onSearchCompleted()

        fun onNoDeviceFound()
        fun onScanError(message: String)
        fun onResumeConnection(timeout: Long)
        fun onSendError(@StringRes title: Int? = null, @StringRes errorMsg: Int)
        fun onSendMessage(@StringRes msg: Int)
        fun onSendMessage(msg: String)
        fun onSendGateStatus()

        /**
         * Called for local and server logs
         * @param withTime
         * @param message
         */
        fun onLogGlobal(withTime: Boolean, message: String)

        /**
         * Called for server logs
         * @param withTime
         * @param message
         */
        fun onLogSessionOnly(withTime: Boolean, message: String)

        fun onLowRssiLevel()
        fun onGoodRssiLevel()
        fun onUnitTooFar(meters: Float)

        /**
         * Called if distance level is ok.
         * @param meters
         */
        fun onUnitDistanceOk(meters: Float)

        fun onDisconnectFromDevice()

        /**
         * Called after rssi and distance checks, and if their levels is ok.
         */
        fun onDeviceIsCheckedAndOk()

        fun canOpenInAutomaticMode(): Boolean
        fun onAutoOpenAfterChecks(showOpeningNotification: Boolean)
        fun onNoAutoOpenAfterChecks()
        fun onGateOpened()
        fun onNotificationTimeout(msg: String)
        fun onFailedConnectAndOpen(msg: String)
        fun onConnectionIntervalChanged(@IntRange(from = CONN_PRIORITY_LOW.toLong(), to = CONN_PRIORITY_HIGH.toLong()) connInterval: Int)
        fun onDeviceSettingsRead()
    }

    companion object {
        internal const val ONE_HOUR = ONE_SECOND * 60 * 60
        internal const val SESSION_DURATION = ONE_SECOND * 30
    }
}