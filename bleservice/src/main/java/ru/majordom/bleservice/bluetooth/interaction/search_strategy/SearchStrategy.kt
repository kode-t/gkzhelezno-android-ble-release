package ru.majordom.bleservice.bluetooth.interaction.search_strategy

import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice

interface SearchStrategy {
    val defaultScanTimeout: Long
    val isSingleDeviceScan: Boolean
    fun checkDeviceFound(device: BlueteethDevice): Boolean
    fun pickTheMostSuitable(): BlueteethDevice?
    fun release()
}