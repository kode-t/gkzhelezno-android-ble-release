package ru.majordom.bleservice.bluetooth.nordic.interaction.searchstrategy

import no.nordicsemi.android.support.v18.scanner.ScanResult
import ru.majordom.bleservice.common.BleOptions

class FirsDeviceSearchStrategy(
        bleOptions: BleOptions
) : BaseSearchStrategy(bleOptions) {

    override val isSingleDeviceScan: Boolean
        get() = true

    override fun compareWhichIsBetter(selectedDevice: ScanResult?, currentDevice: ScanResult): ScanResult {
        return selectedDevice ?: currentDevice
    }
}
