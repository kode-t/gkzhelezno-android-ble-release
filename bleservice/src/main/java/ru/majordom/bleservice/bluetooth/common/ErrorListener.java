package ru.majordom.bleservice.bluetooth.common;


public interface ErrorListener {
    void onError(String errorMsg);
    void onTimeout(String errorMsg);
}
