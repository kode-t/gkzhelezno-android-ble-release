package ru.majordom.bleservice.bluetooth.bleService

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.support.annotation.IntRange
import android.support.annotation.StringRes
import android.util.Log
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import ru.majordom.bleservice.BleServiceConfiguration
import ru.majordom.bleservice.R
import ru.majordom.bleservice.bluetooth.BluetoothConstants
import ru.majordom.bleservice.bluetooth.BluetoothConstants.CONN_PRIORITY_HIGH
import ru.majordom.bleservice.bluetooth.BluetoothConstants.CONN_PRIORITY_LOW
import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.bluetooth.bleService.realization.BaseRealization
import ru.majordom.bleservice.bluetooth.bleService.realization.BlueteethRealization
import ru.majordom.bleservice.bluetooth.bleService.realization.NordicRealization
import ru.majordom.bleservice.common.Logger
import ru.majordom.bleservice.notifications.ServiceNotificationType
import ru.majordom.bleservice.power.PowerManagerWrapper
import ru.majordom.bleservice.power.PowerManagerWrapperImpl
import ru.majordom.bleservice.zones.ZonesWatcher
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class BleService : Service(), KoinComponent, BluetoothStateReceiver.BluetoothStateListener {

    private var handler: Handler? = null
    private var handlerThread: HandlerThread? = null
    private var wakeLock: PowerManager.WakeLock? = null
    private var pmWrapper: PowerManagerWrapper? = null
    private var baseBleRealization: BaseRealization<*, *>? = null

    private var serviceLogger: Logger? = null
    private var fieldTestLogger: Logger? = null

    private val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.US)
    private val fieldLogFileFormat = SimpleDateFormat("yyyy-MM-dd.HH-mm-ss", Locale.US)
    private val bleManager by inject<BluetoothManagerContract>()
    private val geofenceManager by inject<ZonesWatcher>()

    private lateinit var appBleLogs: String
    private lateinit var appFieldLogs: String

    private val connectionRunner = Runnable {
        flushLoggers()
        flushFieldLogger()
        checkPowerSaver()
        baseBleRealization?.searchForGate()
    }

    private val bleListener = object : BaseRealization.Listener {
        override fun onDeviceFound(name: String) = logString(true, "Found gate: $name")

        override fun onResumeConnection(timeout: Long) = resumeConnectionRunner(timeout)

        override fun onStopConnection() = stopConnectionRunner()

        override fun onCreateNewLogSession(deviceName: String) = Unit

        override fun onLogSessionEnded() = Unit

        override fun onSendError(@StringRes title: Int?, @StringRes errorMsg: Int) =
                sendError(if (title == null) null else getString(title), getString(errorMsg))

        override fun onSendMessage(@StringRes msg: Int) = sendMessage(getString(msg))

        override fun onSendMessage(msg: String) = sendMessage(msg)

        override fun onSendGateStatus() = sendGateStatus()

        override fun onLogGlobal(withTime: Boolean, message: String) = logString(withTime, message)

        override fun onLogSessionOnly(withTime: Boolean, message: String) = Unit

        override fun onLowRssiLevel() {
            logString(false, "RSSI level is not appropriate.")
            stopConnectionRunner()
            disconnectFromDevice()
            sendApproachingNotification(false)
            resumeConnectionRunner(NOTIFICATION_APPROACHING_DELAY)
        }

        override fun onGoodRssiLevel() {
            logString(false, "RSSI level is appropriate.")
            configuration?.notificationsDelegate?.clear(ServiceNotificationType.APPROACHING)
        }

        override fun onUnitTooFar(meters: Float) {
            logString(false, "Unit is too far: ~$meters meters.")
            stopConnectionRunner()
            disconnectFromDevice()
            sendApproachingNotification(true)
            resumeConnectionRunner(NOTIFICATION_APPROACHING_DELAY)
        }

        override fun onUnitDistanceOk(meters: Float) =
                logString(false, "Distance to the unit is ~$meters meters.")

        override fun onDisconnectFromDevice() = disconnectFromDevice()

        override fun onDeviceIsCheckedAndOk() {
            configuration?.notificationsDelegate?.clear(ServiceNotificationType.APPROACHING)
        }

        override fun canOpenInAutomaticMode() = true

        override fun onAutoOpenAfterChecks(showOpeningNotification: Boolean) {
            sendOpeningNotification(true)
            connectAndOpenGate()
        }

        override fun onNoAutoOpenAfterChecks() = sendPossibleToOpenNotification()

        override fun onGateOpened() {
            configuration?.apply {
                if (localStorage.isDebug) {
                    sendInfoNotification(ServiceNotificationType.OPENED)
                }
                notificationsDelegate.clear(ServiceNotificationType.OPENING)
            }
        }

        override fun onNotificationTimeout(msg: String) {
            logString(true, "Notification receive timeout: $msg")
            if (configuration?.localStorage?.isDebug == true) {
                sendInfoNotification(ServiceNotificationType.NOTIFICATION_TIMEOUT, msg)
            }
        }

        override fun onFailedConnectAndOpen(msg: String) {
            logString(true, "Failed to connect and open: $msg")
            sendInfoNotification(ServiceNotificationType.BARRIER_ERROR, msg)
        }

        @SuppressLint("Range")
        override fun onConnectionIntervalChanged(@IntRange(from = CONN_PRIORITY_LOW.toLong(), to = CONN_PRIORITY_HIGH.toLong()) connInterval: Int) {
            if (configuration?.localStorage?.isDebug == true) {
                val msg = "Connection interval changed to $connInterval"
                logString(true, msg)
                sendInfoNotification(ServiceNotificationType.CONNECTION_INTERVAL_CHANGED, msg)
            }
        }

        override fun onSearchCompleted() = Unit

        override fun onNoDeviceFound() {
            logString(true, "No compatible devices found.")
            checkGeolocationEnabledAndUpdateNotification()
        }

        override fun onScanError(message: String) {
            logString(true, "Scan error: $message")
            checkGeolocationEnabledAndUpdateNotification()
        }

        override fun onDeviceSettingsRead() {
            configuration?.localStorage?.bleOptions?.readerSettingsRead = true
            stopConnectionRunner()
            disconnectFromDevice()
            resumeConnectionRunner()
        }
    }

    private val isLocationPermissionGranted: Boolean
        get() = checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    override fun onCreate() {
        Log.d(TAG, "onCreate called")
        super.onCreate()
        with(HandlerThread("service")) {
            handlerThread = this
            this.start()
            handler = Handler(this.looper)
        }
        bleManager.addBluetoothStateListener(this)
        pmWrapper = PowerManagerWrapperImpl(this)
        appBleLogs = configuration?.localStorage?.deviceModel + " AppBleLogs.txt"
        appFieldLogs = configuration?.localStorage?.deviceModel + " app-%1\$s.log"
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy called")
        stopScanAndDisconnect()
        handlerThread?.quit()
        bleManager.removeBluetoothStateListener(this)
        pmWrapper = null
        destroyLogger()
        destroyFieldLogger()
        baseBleRealization?.apply { release() }
        SERVICE_IS_STARTED = false
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            val action = intent.action
            when (action) {
                ACTION_START -> onServiceStart(false)
                ACTION_START_WITH_SCAN -> onServiceStart(true)
                ACTION_STOP -> {
                    stopSelf()
                    stopForeground(true)
                    clearServiceNotifications()
                }
                ACTION_START_SCAN -> onServiceStart(true)
                ACTION_STOP_SCAN -> stopScanAndDisconnect()
                ACTION_OPEN_GATE -> {
                    logString(true, "Received manual open command.")
                    connectAndOpenGate()
                }
                ACTION_STATUS -> sendGateStatus()
                ACTION_SHARE_LOGS -> shareLogs()
                ACTION_UPDATE_SERVICE_NOTIFICATION -> if (SERVICE_IS_STARTED) sendForegroundServiceNotification()
            }
        }
        return Service.START_STICKY
    }

    private fun onServiceStart(startScan: Boolean) {
        val isLocationPermitted = isLocationPermissionGranted
        if (!SERVICE_IS_STARTED) {
            SERVICE_IS_STARTED = true
            if (!isLocationPermitted) {
                stopScanAndDisconnect()
                sendForegroundServiceNotification(ServiceNotificationType.SERVICE_GEOLOCATION_DISABLED)
                sendMessage(getString(R.string.permission_location_ask_enable))
            } else
                sendForegroundServiceNotification()
        } else
            sendForegroundServiceNotification()
        if (SERVICE_IS_STARTED && isLocationPermitted) {
            geofenceManager.start()
            if (startScan) startScan()
        }
    }

    private fun stopScanAndDisconnect() {
        stopConnectionRunner()
        disconnectFromDevice()
        releasePartialWakeLock()
        baseBleRealization?.apply { stopScan() }
        flushLoggers()
        flushFieldLogger()
        clearServiceNotifications()
    }

    private fun clearServiceNotifications() {
        configuration?.notificationsDelegate?.clear(ServiceNotificationType.POSSIBLE_TO_OPEN)
    }

    private fun disconnectFromDevice() {
        clearServiceNotifications()
        baseBleRealization?.apply { disconnectFromDevice() }
    }

    private fun startScan() {
        configuration?.let { config ->
            if (baseBleRealization == null || baseBleRealization?.isInScanMode != true) {
                sendForegroundServiceNotification()
                if (baseBleRealization == null) {
                    if (!config.localStorage.bleOptions.advancedDebug) {
                        destroyFieldLogger()
                    } else
                        createFieldLogger()
                }
                val useNordic = config.localStorage.bleOptions.useNordic
                if (baseBleRealization != null) {
                    if (baseBleRealization is BlueteethRealization && useNordic) {
                        baseBleRealization?.release()
                        baseBleRealization = NordicRealization(this, bleManager, bleListener, config.localStorage)
                    } else if (baseBleRealization is NordicRealization && !useNordic) {
                        baseBleRealization?.release()
                        baseBleRealization = BlueteethRealization(this, bleManager, bleListener, config.localStorage)
                    }
                } else {
                    baseBleRealization = if (useNordic)
                        NordicRealization(this, bleManager, bleListener, config.localStorage)
                    else
                        BlueteethRealization(this, bleManager, bleListener, config.localStorage)
                }
                createLogger()
                createFieldLogger()
                acquirePartialWakeLock(INFINITE_WAKELOCK)
                baseBleRealization?.startNewScan()
            }
        }
    }

    private fun connectAndOpenGate() {
        configuration?.notificationsDelegate?.clear(ServiceNotificationType.POSSIBLE_TO_OPEN)
        if (baseBleRealization?.deviceChosen() == true) {
            baseBleRealization?.connectAndOpenGate()
        }
    }

    private fun sendGateStatus() {
        if (baseBleRealization != null) {
            with(Intent(BleReceiver.ACTION_GATE_STATUS)) {
                putExtra(BleReceiver.KEY_GATE_STATUS, baseBleRealization?.gateState)
                sendBroadcast(this)
            }
        }
    }

    private fun sendAppLogMessage(message: String) {
        with(Intent(BleReceiver.ACTION_LOG_STATUS)) {
            putExtra(BleReceiver.KEY_LOG_STATUS, message)
            sendBroadcast(this)
        }
    }

    private fun sendError(title: String? = null, msg: String) {
        baseBleRealization?.apply {
            gateState.messageTitle = title
            gateState.setError(msg)
            sendGateStatus()
        }
        clearServiceNotifications()
        sendInfoNotification(ServiceNotificationType.BARRIER_ERROR, msg)
    }

    private fun sendMessage(msg: String) {
        baseBleRealization?.apply {
            gateState.message = msg
            if (gateState.isConnected) {
                configuration?.notificationsDelegate?.clear(ServiceNotificationType.BARRIER_ERROR)
            }
            sendGateStatus()
        }
    }

    private fun getMessageWithTime(withTime: Boolean, string: String): String {
        val time = if (withTime) dateFormat.format(Date(System.currentTimeMillis())) + " " else null
        return if (time == null) string else time + string
    }

    private fun logString(withTime: Boolean, string: String) {
        val message = getMessageWithTime(withTime, string)
        sendAppLogMessage(message)

        serviceLogger?.apply {
            try {
                log(message)
            } catch (e: IOException) {
                Log.e(TAG, "IO Error: $e")
            }

        }
        fieldTestLogger?.apply {
            try {
                log(message)
            } catch (e: IOException) {
                Log.e(TAG, "IO Error: $e")
            }

        }
    }

    private fun shareLogs() {
        serviceLogger?.also {
            val uri = it.contentUri
            val intent = Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("mailto:")
                putExtra(Intent.EXTRA_SUBJECT, "App BLE operation log " + dateFormat.format(Date(System.currentTimeMillis())))
                putExtra(Intent.EXTRA_STREAM, uri)
            }
            val resInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                this.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            startActivity(Intent.createChooser(intent, "Share logs").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        }
    }

    private fun createLogger() {
        configuration?.apply {
            if (localStorage.bleOptions.advancedDebug) {
                if (serviceLogger == null) {
                    serviceLogger = loggerCreator?.createFileLogger(this@BleService, filesDir, appBleLogs, false)
                }
                logString(false, localStorage.deviceModel + " " + localStorage.osVersion + ", appVer " + localStorage.appVersion)
            }
        }
    }

    private fun createFieldLogger() {
        configuration?.apply {
            if (fieldTestLogger == null && localStorage.bleOptions.advancedDebug) {
                val baseDir = Environment.getExternalStorageDirectory().absolutePath
                val appDir = baseDir + File.separator + localStorage.appName
                val externalDir = File(appDir)
                val datePart = fieldLogFileFormat.format(Date(System.currentTimeMillis()))
                fieldTestLogger = configuration?.loggerCreator?.createFileLogger(this@BleService, externalDir, String.format(Locale.getDefault(), appFieldLogs, datePart), false)
                logString(false, localStorage.deviceModel + " " + localStorage.osVersion + ", appVer " + localStorage.appVersion)
            }
        }
    }

    private fun flushLoggers() {
        flushLogger(serviceLogger)
        flushFieldLogger()
    }

    private fun flushFieldLogger() = flushLogger(fieldTestLogger)

    private fun flushLogger(logger: Logger?) {
        logger?.apply {
            try {
                flush()
            } catch (e: IOException) {
                Log.e(TAG, "Error flushing logger: $e")
            }

        }
    }

    private fun destroyLogger() {
        serviceLogger?.apply {
            try {
                close()
            } catch (e: IOException) {
                Log.e(TAG, "Error closing logger: $e")
            }
            serviceLogger = null
        }
    }

    private fun destroyFieldLogger() {
        fieldTestLogger?.apply {
            try {
                close()
            } catch (e: IOException) {
                Log.e(TAG, "Error closing logger: $e")
            }
            fieldTestLogger = null
        }
    }

    private fun sendForegroundServiceNotification(type: ServiceNotificationType = ServiceNotificationType.SERVICE_RUNNING) {
        configuration?.notificationsDelegate?.sendForegroundServiceNotification(this, SERVICE_IS_STARTED, type)
    }

    private fun sendInfoNotification(type: ServiceNotificationType, message: String? = null) {
        wakeUpScreen()
        configuration?.notificationsDelegate?.sendInfoNotification(type, message)
    }

    private fun sendPossibleToOpenNotification() {
        wakeUpScreen()
        configuration?.notificationsDelegate?.sendPossibleToOpenNotification()
    }

    private fun sendOpeningNotification(send: Boolean) {
        if (send) configuration?.notificationsDelegate?.sendOpeningNotification()
    }

    private fun sendApproachingNotification(isTooFar: Boolean) {
        if (configuration?.localStorage?.isDebug == true) {
            wakeUpScreen()
            configuration?.notificationsDelegate?.sendApproachingNotification(isTooFar)
        }
    }

    private fun sendBatterySaverNotification() {
        configuration?.notificationsDelegate?.sendBatterySaverNotification()
    }

    override fun onBluetoothAvailable() = startScan(this.applicationContext)

    override fun onBluetoothUnavailable() {
        stopScanAndDisconnect()
        sendForegroundServiceNotification(ServiceNotificationType.SERVICE_BLUETOOTH_DISABLED)
    }

    private fun acquirePartialWakeLock(ms: Long) {
        if (ms == INFINITE_WAKELOCK) {
            if (wakeLock == null) {
                val powerManager = pmWrapper?.powerManager
                wakeLock = powerManager?.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Token:BleServiceCpuWakelock")
            }
            if (wakeLock?.isHeld != true) {
                wakeLock?.acquire(ms)
                Log.d(TAG, "Wakelock acquired, infinite")
            } else {
                releasePartialWakeLock()
                Log.d(TAG, "Wakelock is already held. Reacquiring.")
                wakeLock?.acquire(ms)
            }
        } else {
            releasePartialWakeLock()
            wakeLock?.acquire(ms)
            Log.d(TAG, "Wakelock acquired, duration(ms) = $ms")
        }
    }

    private fun releasePartialWakeLock() {
        if (wakeLock != null && wakeLock?.isHeld == true) {
            try {
                wakeLock?.release()
            } catch (t: Throwable) {
                wakeLock = null
                Log.w(TAG, "Catch exception on wakelock dispose: $t")
            }

            Log.d(TAG, "Wakelock released.")
        }
    }

    private fun resumeConnectionRunner(delay: Long = 0) {
        if (handlerThread?.isAlive == true) {
            stopConnectionRunner()
            handler?.postDelayed(connectionRunner, delay)
            Log.d(TAG, "Connection runner posted, delay(ms) = $delay")
        }
    }

    private fun stopConnectionRunner() {
        if (handlerThread?.isAlive == true) {
            handler?.removeCallbacks(connectionRunner)
            Log.d(TAG, "Connection runner removed.")
        }
    }

    private fun checkGeolocationEnabledAndUpdateNotification() {
        if (isLocationPermissionGranted) {
            if (!geofenceManager.isGeolocationEnabled) {
                sendForegroundServiceNotification(ServiceNotificationType.SERVICE_GEOLOCATION_DISABLED)
            } else {
                sendForegroundServiceNotification()
            }
        }
    }

    private fun checkPowerSaver() {
        if (pmWrapper?.isPowerSaveMode == true && pmWrapper?.isIgnoringBatteryOptimizations == false) {
            sendBatterySaverNotification()
        } else {
            configuration?.notificationsDelegate?.clear(ServiceNotificationType.POWER_SAVE_EXPOSED)
        }
    }

    private fun wakeUpScreen() {
        if (pmWrapper?.isScreenOn != true) {
            val screenWakeLock = pmWrapper?.powerManager
                    ?.newWakeLock(PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP, "Token:ScreenWakeLock")
            screenWakeLock?.acquire(WAKEUP_DURATION.toLong())
        }
    }

    companion object {
        private val TAG = BleService::class.java.simpleName
        private var configuration: BleServiceConfiguration? = null

        const val ACTION_START = "app.action_start_without_scan"
        const val ACTION_START_WITH_SCAN = "app.action_start_with_scan"
        const val ACTION_START_SCAN = "app.action_start_scan"
        const val ACTION_STOP = "app.action_stop"
        const val ACTION_STOP_SCAN = "app.action_stop_scan"
        const val ACTION_STATUS = "app.action_status"
        const val ACTION_OPEN_GATE = "app.action_open_gate"
        const val ACTION_SHARE_LOGS = "app.action_share_logs"
        const val ACTION_UPDATE_SERVICE_NOTIFICATION = "app.action.update_service_notification"

        private const val NOTIFICATION_APPROACHING_DELAY = (1.5 * BluetoothConstants.ONE_SECOND).toInt().toLong()
        private const val WAKEUP_DURATION = (1.5 * BluetoothConstants.ONE_SECOND).toInt()
        private const val INFINITE_WAKELOCK = Long.MAX_VALUE
        private var SERVICE_IS_STARTED = false

        fun setConfiguration(bleServiceConfiguration: BleServiceConfiguration) {
            configuration = bleServiceConfiguration
        }

        fun start(context: Context, startScan: Boolean) {
            if (configuration == null) throw RuntimeException("BleService is not configured")
            runService(context, getIntent(context, if (startScan) ACTION_START_WITH_SCAN else ACTION_START), true)
        }

        fun stop(context: Context) {
            runService(context, getIntent(context, ACTION_STOP))
        }

        fun startScan(context: Context) {
            if (configuration == null) throw RuntimeException("BleService is not configured")
            runService(context, getIntent(context, ACTION_START_SCAN))
        }

        fun stopScan(context: Context) {
            runService(context, getIntent(context, ACTION_STOP_SCAN))
        }

        fun checkStatus(context: Context) {
            runService(context, getIntent(context, ACTION_STATUS))
        }

        fun openGate(context: Context) {
            if (configuration == null) throw RuntimeException("BleService is not configured")
            runService(context, getIntent(context, ACTION_OPEN_GATE))
        }

        fun shareLogs(context: Context) {
            if (configuration == null) throw RuntimeException("BleService is not configured")
            runService(context, getIntent(context, ACTION_SHARE_LOGS))
        }

        fun updateServiceNotification(context: Context) {
            if (SERVICE_IS_STARTED) {
                runService(context, getIntent(context, ACTION_UPDATE_SERVICE_NOTIFICATION))
            }
        }

        private fun runService(context: Context, intent: Intent, isExplicitForeground: Boolean = false) {
            if (isExplicitForeground && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent)
            } else {
                context.startService(intent)
            }
        }

        private fun getIntent(context: Context, action: String): Intent {
            val intent = Intent(context, BleService::class.java)
            intent.action = action
            return intent
        }
    }
}