package ru.majordom.bleservice.bluetooth.device.listeners;

public interface OnRssiChangedListener {
    void onRssiChanged(int currentRssi, int previousRssi, int stashedRssi, int timesRssiRead);
}
