package ru.majordom.bleservice.bluetooth.bleService;

import java.io.Serializable;

public class GateState implements Serializable {
    private boolean connected;
    private boolean authenticated;
    private boolean isInRange;
    private boolean isOpeningInProcess;
    private boolean isOpened;
    private boolean isError;
    private String gateName = "";
    private String message;
    private String messageTitle;
    private String detectedActivity;

    public boolean isConnected() {
        return connected;
    }

    public boolean isReady() {
        return authenticated && connected;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setError(String message) {
        this.message = message;
        this.isError = true;
    }

    public void disconnect() {
        authenticated = false;
        connected = false;
        isInRange = false;
        isOpeningInProcess = false;
        isOpened = false;
        isError = false;
        message = null;
        messageTitle = null;
        gateName = "";
    }

    public boolean isInRange() {
        return isInRange;
    }

    public void setInRange(boolean inRange) {
        isInRange = inRange;
    }

    public boolean isOpeningInProcess() {
        return isOpeningInProcess;
    }

    public void setOpeningInProcess(boolean openingInProcess) {
        isOpeningInProcess = openingInProcess;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    public boolean isError() {
        return isError;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public void ready() {
        authenticated = true;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getDetectedActivity() {
        return detectedActivity;
    }

    public void setDetectedActivity(String detectedActivity) {
        this.detectedActivity = detectedActivity;
    }

    @Override
    public String toString() {
        return "GateState {" +
                "connected= " + connected +
                ", authenticated= " + authenticated +
                ", isInRange= " + isInRange +
                ", isOpeningInProcess= " + isOpeningInProcess +
                ", isOpened= " + isOpened +
                ", isError= " + isError +
                //", detectedActivity=" + detectedActivity +
                ", gateName= '" + gateName + '\'' +
                ", message= '" + message + '\'' +
                " }";
    }

    public String toStringForLogs() {
        return "conn=" + connected +
                ", auth=" + authenticated +
                ", isOpening=" + isOpeningInProcess +
                ", isOpened=" + isOpened +
                ", err=" + isError +
                //", detectedActivity=" + detectedActivity +
                ", gate" + gateName + '\'' +
                ", msg=" + message + '\'';
    }
}
