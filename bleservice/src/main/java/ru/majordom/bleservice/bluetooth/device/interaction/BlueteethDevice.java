package ru.majordom.bleservice.bluetooth.device.interaction;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import ru.majordom.bleservice.R;
import ru.majordom.bleservice.bluetooth.BluetoothConstants;
import ru.majordom.bleservice.bluetooth.device.gatt.GattOperationBundle;
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattCharacteristicReadOperation;
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattCharacteristicWriteOperation;
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattDescriptorReadOperation;
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattDescriptorWriteOperation;
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattOperation;
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattSetNotificationOperation;
import ru.majordom.bleservice.bluetooth.device.listeners.OnBondingChangedListener;
import ru.majordom.bleservice.bluetooth.device.listeners.OnConnectionChangedListener;
import ru.majordom.bleservice.bluetooth.device.listeners.OnRssiChangedListener;
import ru.majordom.bleservice.bluetooth.device.listeners.OnServicesDiscoveredListener;
import ru.majordom.bleservice.bluetooth.device.models.RssiModel;

// TODO: Make this object threadsafe and async-safe (called twice in a row, should return a failure?)
public class BlueteethDevice {
    private static final String TAG = BlueteethDevice.class.getSimpleName();
    private final String name;
    private final String macAddress;
    private BluetoothDevice bluetoothDevice;
    private Handler handler;
    private byte[] mScanRecord;
    private BlueteethManager manager;

    private Thread remoteRssiPeriodicReader;
    private volatile int rssi;
    private volatile int stashedRssi;
    private volatile int timesRssiRead;

    @Nullable
    private BaseGatt bluetoothGatt;
    @Nullable
    private OnConnectionChangedListener mConnectionChangedListener;
    @Nullable
    private OnServicesDiscoveredListener mServicesDiscoveredListener;
    @Nullable
    private OnBondingChangedListener mBondingChangedListener;
    @Nullable
    private OnRssiChangedListener onRssiChangedListener;

    private Map<String, GattSetNotificationOperation> mNotificationMap;
    private BondState mBondState = BondState.Unknown;

    private boolean isInUse;
    private boolean mIsConnected;
    private boolean isClosed;
    private BluetoothGattCallback mGattCallback;
    private BroadcastReceiver mBroadcastReceiver;

    /***
     * Default constructor with invalid values - only here to help
     * with testability and Mock extensions
     */
    BlueteethDevice() {
        name = "Invalid";
        macAddress = "00:00:00:00:00:00";
        bluetoothDevice = null;
        // TODO: Does this need to be dependency injected for testing?
        handler = new Handler();
    }

    BlueteethDevice(BlueteethManager manager, BluetoothDevice device) {
        this.manager = manager;
        bluetoothDevice = device;
        name = device.getName() == null ? "No name" : device.getName();
        macAddress = device.getAddress();
        // TODO: Does this need to be dependency injected for testing?
        handler = new Handler(Looper.getMainLooper());
    }

    BlueteethDevice(BlueteethManager manager, BluetoothDevice device, int rssi, byte[] scanRecord) {
        this(manager, device);
        this.rssi = rssi;
        mScanRecord = scanRecord;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    BlueteethDevice(BlueteethManager manager, ScanResult record) {
        this(manager, record.getDevice());
        this.rssi = record.getRssi();
        ScanRecord scanRecord = record.getScanRecord();
        if (scanRecord != null) {
            mScanRecord = scanRecord.getBytes();
        }
    }

    public String getName() {
        return name;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public int getRssi() {
        return rssi;
    }

    void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public byte[] getScanRecord() {
        return mScanRecord;
    }

    void setScanRecord(byte[] scanRecord) {
        mScanRecord = mScanRecord;
    }

    public void setManager(BlueteethManager manager) {
        this.manager = manager;
    }

    @Nullable
    public BaseGatt getBluetoothGatt() {
        return bluetoothGatt;
    }

    public BondState getBondState() {
        return mBondState;
    }

    public boolean isConnected() {
        return mIsConnected;
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    // Autoreconnect == true is a slow connection, false is fast
    // https://stackoverflow.com/questions/22214254/android-ble-connect-slowly
    public boolean connect(final boolean autoReconnect, BaseGatt gatt) {
        if (!isClosed) {
            if (mIsConnected) {
                Log.d(TAG, "connect: Already connected, returning - disregarding autoReconnect");
                if (mConnectionChangedListener != null) {
                    mConnectionChangedListener.call(this, mIsConnected);
                }
                return false;
            }

            mNotificationMap = new HashMap<>();
            createGattCallback();
            // TODO: Passing in a null context seems to work, but what are the consequences?
            // TODO: Should I grab the application context from the BlueteethManager? Seems odd...
            bluetoothGatt = gatt.connect(manager.getContext(), autoReconnect, this, mGattCallback);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // TODO: 6/27/2017 move to the upper levels to avoid coherence
                int connPriority;
                switch (BluetoothConstants.getConnPriority()) {
                    case BluetoothConstants.CONN_PRIORITY_LOW: {
                        connPriority = BluetoothGatt.CONNECTION_PRIORITY_LOW_POWER;
                        break;
                    }
                    case BluetoothConstants.CONN_PRIORITY_MID: {
                        connPriority = BluetoothGatt.CONNECTION_PRIORITY_BALANCED;
                        break;
                    }
                    case BluetoothConstants.CONN_PRIORITY_HIGH: {
                        connPriority = BluetoothGatt.CONNECTION_PRIORITY_HIGH;
                        break;
                    }
                    default: connPriority = BluetoothGatt.CONNECTION_PRIORITY_LOW_POWER;
                }
                bluetoothGatt.gatt.requestConnectionPriority(connPriority);
                //bluetoothGatt.gatt.requestMtu(10 * 23);
            }
            return true;
        } else {
            Log.e(TAG, "Trying to connect closed device!");
            return false;
        }
    }

    public boolean connect(boolean autoReconnect, BaseGatt gatt, OnConnectionChangedListener onConnectionChangedListener) {
        mConnectionChangedListener = onConnectionChangedListener;
        return connect(autoReconnect, gatt);
    }

    /***
     * This connect call is only useful if the user is interested in Pairing/Bonding
     *
     * @param autoReconnect
     * @param onConnectionChangedListener
     * @param onBondingChangedListener
     * @return
     */
    public boolean connect(boolean autoReconnect, BaseGatt gatt, OnConnectionChangedListener onConnectionChangedListener, OnBondingChangedListener onBondingChangedListener) {
        mBondingChangedListener = onBondingChangedListener;
        return connect(autoReconnect, gatt, onConnectionChangedListener);
    }

    public void disconnect(@Nullable OnConnectionChangedListener onConnectionChangedListener) {
        mConnectionChangedListener = onConnectionChangedListener;
        disconnect();
    }

    public void disconnect() {
        if (bluetoothGatt == null) {
            //Log.w(TAG, "disconnect: Cannot disconnect - GATT is null");
            if (mConnectionChangedListener != null) {
                mConnectionChangedListener.call(this, false);
            }
            //close();
        } else {
            bluetoothGatt.disconnect();
        }
    }

    public boolean discoverServices(OnServicesDiscoveredListener onServicesDiscoveredListener) {
        Log.d(TAG, "discoverServices: Attempting to discover services");
        if (!mIsConnected || bluetoothGatt == null) {
            Log.e(TAG, "discoverServices: Device is not connected, or GATT is null");
            return false;
        }

        mServicesDiscoveredListener = onServicesDiscoveredListener;
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (bluetoothGatt != null) {
                    bluetoothGatt.discoverServices();
                }
            }
        });
        return true;
    }

    public void queueBundle(GattOperationBundle bundle) {
        Log.d(TAG, "Attempting to queue " + bundle.toString());
        manager.queue(bundle);
    }

    public void registerNotification(@NonNull GattSetNotificationOperation operation, @NonNull UUID characteristic, boolean enable) {
        if (enable) {
            mNotificationMap.put(characteristic.toString(), operation);
        } else {
            mNotificationMap.remove(characteristic.toString());
        }
    }

    /*public boolean addIndication(@NonNull UUID characteristic, @NonNull UUID service, OnCharacteristicReadListener characteristicReadListener) {
        Log.d(TAG, "addIndication: Adding Notification listener to " + characteristic.toString());

        if (bluetoothGatt == null) {
            Log.e(TAG, "addIndication: GATT is null");
            return false;
        }

        BluetoothGattService gattService = bluetoothGatt.getService(service);
        if (gattService == null) {
            Log.e(TAG, "addIndication: Service not available - " + service.toString());
            return false;
        }

        BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(characteristic);
        if (gattCharacteristic == null) {
            Log.e(TAG, "addIndication: Characteristic not available - " + characteristic.toString());
            return false;
        }

        bluetoothGatt.setCharacteristicNotification(gattCharacteristic, true);
        BluetoothGattDescriptor gattDescriptor = gattCharacteristic.getDescriptor(characteristic);
        gattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
        bluetoothGatt.writeDescriptor(gattDescriptor);
        return true;
    }*/

    public void enablePeriodicRssiUpdate(final int interval, @Nullable OnRssiChangedListener listener) {
        disablePeriodicRssiUpdate();
        onRssiChangedListener = listener;
        Log.d(TAG, "RSSI update enabled.");
        remoteRssiPeriodicReader = new Thread(new Runnable() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void run() {
                stashedRssi = 0;
                timesRssiRead = 0;
                while (remoteRssiPeriodicReader != null && !remoteRssiPeriodicReader.isInterrupted()) {
                    bluetoothGatt.readRemoteRssi();
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException e) {
                        Log.d(TAG, "RSSI update disabled.");
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });
        remoteRssiPeriodicReader.start();
    }

    public RssiModel disablePeriodicRssiUpdate() {
        if (remoteRssiPeriodicReader != null && remoteRssiPeriodicReader.isAlive()) {
            remoteRssiPeriodicReader.interrupt();
        }
        onRssiChangedListener = null;
        return new RssiModel(rssi, stashedRssi, timesRssiRead);
    }

    public void cancelCurrentOperationBundle(@Nullable String errorMsg) {
        manager.cancelCurrentOperationBundle(this, errorMsg);
    }

    @Override
    public int hashCode() {
        return macAddress.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlueteethDevice)) return false;

        BlueteethDevice that = (BlueteethDevice) o;
        return macAddress.equals(that.macAddress);
    }

    public boolean isClosed() {
        return isClosed;
    }

    public boolean isInUse() {
        return isInUse;
    }

    public void setInUse(boolean inUse) {
        isInUse = inUse;
    }

    @Override
    public String toString() {
        return "BluetoothDevice - Name: " + getName() + ", isClosed: " + isClosed + ", isInUse: " + isInUse + ", macAddress: " + bluetoothDevice.toString() + "\n"
                + "RSSI: " + getRssi() + "\n"
                + "ScanRecord: " + Arrays.toString(mScanRecord) + "\n";
    }

    /***
     * Should never really get here, only if someone forgets to call close() explicitly
     *
     * @throws Throwable
     */
    protected void finalize() throws Throwable {
        try {
            close();
        } finally {
            super.finalize();
        }
    }

    public void close() {
        if (!isClosed) {
            disablePeriodicRssiUpdate();
            this.remoteRssiPeriodicReader = null;
            mConnectionChangedListener = null;
            if (bluetoothGatt != null) {
                bluetoothGatt.close();
                bluetoothGatt = null;
            }
            this.mScanRecord = null;
            this.mServicesDiscoveredListener = null;
            this.mBondingChangedListener = null;
            this.bluetoothDevice = null;
            this.manager = null;
            this.handler = null;
            this.mBroadcastReceiver = null;
            this.mGattCallback = null;
            if (mNotificationMap != null) {
                this.mNotificationMap.clear();
                this.mNotificationMap = null;
            }
            isClosed = true;
            mIsConnected = false;
            isInUse = false;
        }
    }

    private void createBondReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();
                if (action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                    final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);

                    switch (state) {
                        case BluetoothDevice.BOND_BONDING:
                            Log.d(TAG, "onReceive - BONDING");
                            mBondState = BondState.Bonding;
                            break;

                        case BluetoothDevice.BOND_BONDED:
                            Log.d(TAG, "onReceive - BONDED");
                            mBondState = BondState.Bonded;
                            if (mBondingChangedListener != null) {
                                mBondingChangedListener.call(true);
                            }
                            break;

                        case BluetoothDevice.BOND_NONE:
                            Log.d(TAG, "onReceive - NONE");
                            mBondState = BondState.Unknown;
                            break;
                    }
                }
            }
        };
    }

    private void createGattCallback() {
        mGattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                super.onConnectionStateChange(gatt, status, newState);
                Log.d(TAG, String.format("onConnectionStateChange - gatt: %s, status: %s, newState: %s ", gatt.toString(), status, newState));

                // Removed check for GATT_SUCCESS - do we care? I think the current state is all that matters...

                switch (newState) {
                    case BluetoothProfile.STATE_CONNECTED:
//                    mBondState = BondState.fromInteger(bluetoothDevice.getBondState());
                        Log.d(TAG, "onConnectionStateChange - Connected - Bonding=" + mBondState);
                        mIsConnected = true;

                        createBondReceiver();
                        // Register for Bonding notifications
                        manager.getContext().registerReceiver(mBroadcastReceiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));

                        if (mConnectionChangedListener != null) {
                            mConnectionChangedListener.call(BlueteethDevice.this, true);
//                            mConnectionChangedListener = null;
                        }

                        if (mBondingChangedListener != null && mBondState == BondState.Unknown) {
                            mBondingChangedListener.call(BondState.fromInteger(bluetoothDevice.getBondState()) == BondState.Bonded);
                        }

                        break;

                    case BluetoothProfile.STATE_DISCONNECTED:
                        Log.d(TAG, "onConnectionStateChange - Disconnected");
//                    mBondState = BondState.Unknown;
                        mIsConnected = false;
                        //manager.clearQueue();

                        // Unregister for Bonding notifications
                        try {
                            if (mBroadcastReceiver != null) {
                                manager.getContext().unregisterReceiver(mBroadcastReceiver);
                            }
                        } catch (Exception e) {
                            Log.w(TAG, e.toString());
                        }

                        if (mConnectionChangedListener != null) {
                            mConnectionChangedListener.call(BlueteethDevice.this, false);
                        }
                        break;
                }
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                super.onServicesDiscovered(gatt, status);
                Log.d(TAG, String.format("onServicesDiscovered - gatt: %s, status: %s", gatt.toString(), status));

                if (mServicesDiscoveredListener != null) {
                    BlueteethResponse response = BlueteethResponse.NO_ERROR;
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        Log.d(TAG, "onServicesDiscovered - Success");
                    } else {
                        Log.e(TAG, "onServicesDiscovered - Failed with status: " + status);
                        response = BlueteethResponse.ERROR;
                    }
                    mServicesDiscoveredListener.call(response);
                    mServicesDiscoveredListener = null;
                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onCharacteristicRead(gatt, characteristic, status);
                Log.d(TAG, String.format("onCharacteristicRead - gatt: %s, status: %s, value: %s ", gatt.toString(), status, Arrays.toString(characteristic.getValue())));
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    GattOperation op = manager.getCurrentOperation();
                    if (op != null && op instanceof GattCharacteristicReadOperation) {
                        GattCharacteristicReadOperation operation = (GattCharacteristicReadOperation) op;
                        Log.d(TAG, "onCharacteristicRead - Success");
                        if (operation.isDataLengthCorrect(characteristic.getValue())) {
                            operation.onRead(characteristic);
                            manager.nullCurrentOperationAndDrive();
                        } else {
                            manager.cancelCurrentOperationBundle(BlueteethDevice.this, operation.getLastInternalCheckError());
                        }
                    }
                } else {
                    Log.e(TAG, "onCharacteristicRead - Failed with status: " + status);
                    manager.cancelCurrentOperationBundle(BlueteethDevice.this, manager.getContext().getString(R.string.bluetooth_read_operation_failed));
                }
            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onCharacteristicWrite(gatt, characteristic, status);
                Log.d(TAG, String.format("onCharacteristicWrite - gatt: %s, status: %s, characteristic: %s ", gatt.toString(), status, characteristic.getUuid().toString()));
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    GattOperation op = manager.getCurrentOperation();
                    if (op != null && op instanceof GattCharacteristicWriteOperation) {
                        GattCharacteristicWriteOperation operation = (GattCharacteristicWriteOperation) op;
                        Log.d(TAG, "onCharacteristicWrite - Success");
                        operation.onWrite();
                        manager.nullCurrentOperationAndDrive();
                    }
                } else {
                    Log.e(TAG, "onCharacteristicWrite - Failed with status: " + status);
                    manager.cancelCurrentOperationBundle(BlueteethDevice.this, manager.getContext().getString(R.string.bluetooth_write_operation_failed));
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);
                Log.d(TAG, String.format("onCharacteristicChanged - gatt: %s, value: %s ", gatt.toString(), Arrays.toString(characteristic.getValue())));
                GattSetNotificationOperation operation = mNotificationMap.get(characteristic.getUuid().toString());
                if (operation != null) {
                    operation.getCallback().onRead(characteristic.getValue());
                } else {
                    Log.d(TAG, "No notification registered for " + characteristic.getUuid().toString());
                }
            }

            @Override
            public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                super.onDescriptorRead(gatt, descriptor, status);
                Log.d(TAG, String.format("onDescriptorRead - gatt: %s, status: %s, characteristic: %s ", gatt.toString(), status, descriptor.toString()));
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    Log.d(TAG, "onDescriptorRead - Success");
                    GattOperation op = manager.getCurrentOperation();
                    if (op != null && op instanceof GattDescriptorWriteOperation) { // reading descriptor in notification operation, so must handle this case
                        GattDescriptorReadOperation operation = (GattDescriptorReadOperation) op;
                        operation.onRead(descriptor);
                        manager.nullCurrentOperationAndDrive();
                    }
                } else {
                    Log.e(TAG, "onDescriptorRead - Failed with status: " + status);
                    manager.cancelCurrentOperationBundle(BlueteethDevice.this, manager.getContext().getString(R.string.bluetooth_read_operation_failed));
                }
            }

            @Override
            public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                super.onDescriptorWrite(gatt, descriptor, status);
                Log.d(TAG, String.format("onDescriptorWrite - gatt: %s, status: %s, characteristic: %s ", gatt.toString(), status, descriptor.toString()));
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    Log.d(TAG, "onDescriptorWrite - Success");
                    GattOperation op = manager.getCurrentOperation();
                    if (op != null && op instanceof GattDescriptorWriteOperation) { // writing descriptor in notification operation, so must handle this case
                        GattDescriptorWriteOperation operation = (GattDescriptorWriteOperation) op;
                        operation.onWrite();
                        manager.nullCurrentOperationAndDrive();
                    } else if (op instanceof GattSetNotificationOperation) {
                        manager.nullCurrentOperationAndDrive();
                    }
                } else {
                    Log.e(TAG, "onDescriptorWrite - Failed with status: " + status);
                    manager.cancelCurrentOperationBundle(BlueteethDevice.this, manager.getContext().getString(R.string.bluetooth_write_operation_failed));
                }
            }

            @Override
            public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
                if (rssi != 0) { // 0 is impossible, skip it
                    stashedRssi += rssi;
                    timesRssiRead++;
                    Log.d(TAG, "Updating RSSI value. Old = " + BlueteethDevice.this.rssi + ", new = " + rssi + ", stashed = " + stashedRssi + ", times read = " + timesRssiRead);
                    if (onRssiChangedListener != null) {
                        onRssiChangedListener.onRssiChanged(rssi, BlueteethDevice.this.rssi, stashedRssi, timesRssiRead);
                    }
                    BlueteethDevice.this.rssi = rssi;
                } else {
                    Log.d(TAG, "RSSI was 0, skipped it.");
                }
            }

            @Override
            public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
                Log.d(TAG, "Mtu changed: " + String.valueOf(status == BluetoothGatt.GATT_SUCCESS) +
                        ", new MTU size = " + String.valueOf(mtu));
            }
        };
    }

    public enum BondState {
        Unknown,
        UnBonded,
        Bonding,
        Bonded;

        public static BondState fromInteger(int x) {
            switch (x) {
                case 10:
                    return UnBonded;
                case 11:
                    return Bonding;
                case 12:
                    return Bonded;
            }
            return Unknown;
        }
    }

    // TODO: Fill this out from https://android.googlesource.com/platform/external/bluetooth/bluedroid/+/android-4.3_r1.1/stack/include/gatt_api.h
    // TODO: Expose underlying GATT errors
    public enum BlueteethResponse {
        NO_ERROR,
        NOT_CONNECTED,
        BUSY,
        ERROR
    }
}