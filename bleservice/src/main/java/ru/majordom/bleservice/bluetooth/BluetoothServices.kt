package ru.majordom.bleservice.bluetooth

import java.util.*

class BluetoothServices {

    object BarrierControl {
        private val SERVICE_UUID_PART = "0000162"
        val SERVICE_UUID = UUID.fromString(SERVICE_UUID_PART + "3" + BluetoothConstants.UUID_COMMON_PART)

        val RESPONSE = UUID.fromString(SERVICE_UUID_PART + "4" + BluetoothConstants.UUID_COMMON_PART)
        val RESPONSE_BYTES_COUNT = 1

        val COMMAND = UUID.fromString(SERVICE_UUID_PART + "5" + BluetoothConstants.UUID_COMMON_PART)
        val COMMAND_BYTES_COUNT = 7

        val COMMAND_SET_TX = 0xA0
        val COMMAND_READ_TEX = 0xA1

        val COMMAND_REBOOT = 0xB0

        val COMMAND_READ_FIRMWARE = 0xC0

        val COMMAND_SET_MOVEMENT_TIMEOUT = 0xD0
        val COMMAND_READ_MOVEMENT_TIMEOUT = 0xD1

        val OK_RESPONSE = 0x4B4F

        fun getCommandBytes(command: Int): ByteArray {
            val bytesToWrite = ByteArray(BluetoothServices.BarrierControl.COMMAND_BYTES_COUNT)
            val commandBytes = BluetoothUtils.intToSingleByteArray(command)
            System.arraycopy(commandBytes, 0, bytesToWrite, 0, commandBytes.size)
            return bytesToWrite
        }

        fun prepareSetTxBytes(readerPower: Int): ByteArray {
            val bytesToWrite = getCommandBytes(BluetoothServices.BarrierControl.COMMAND_SET_TX)
            val readerPowerBytes = BluetoothUtils.intToSingleByteArray(readerPower)
            System.arraycopy(readerPowerBytes, 0, bytesToWrite, 1, readerPowerBytes.size)
            return bytesToWrite
        }

        fun prepareSetMovementTimeoutBytes(timeout: Int): ByteArray {
            val bytesToWrite = getCommandBytes(BluetoothServices.BarrierControl.COMMAND_SET_MOVEMENT_TIMEOUT)
            val readerPowerBytes = BluetoothUtils.intToSingleByteArray(timeout)
            System.arraycopy(readerPowerBytes, 0, bytesToWrite, 1, readerPowerBytes.size)
            return bytesToWrite
        }

        fun prepareCodeCommand(code: Long, wiegandVersion: Int): ByteArray {
            val bytesToWrite = ByteArray(BluetoothServices.BarrierControl.COMMAND_BYTES_COUNT)
            val wiegandArray = BluetoothUtils.intToSingleByteArray(wiegandVersion)
            val codeArray = BluetoothUtils.removeZeroes(BluetoothUtils.longToBytes(code, false), false)
            var newCodeArrayLength = 0
            when (wiegandVersion) {
                BluetoothConstants.WIEGAND_26 -> newCodeArrayLength = 3
                BluetoothConstants.WIEGAND_34 -> newCodeArrayLength = 4
                BluetoothConstants.WIEGAND_42 -> newCodeArrayLength = 5
            }
            val newCodeArray = ByteArray(newCodeArrayLength)
            var i = codeArray.size - 1
            var j = newCodeArray.size - 1
            while (j >= 0 && i >= 0) {
                newCodeArray[j] = codeArray[i]
                i--
                j--
            }
            System.arraycopy(wiegandArray, 0, bytesToWrite, 0, wiegandArray.size)
            System.arraycopy(newCodeArray, 0, bytesToWrite, wiegandArray.size, newCodeArray.size)
            return bytesToWrite
        }

        fun readTxBytes(bytes: ByteArray?): Int = bytes?.get(1)?.toInt() ?: 0

        fun readMovementTimeoutBytes(bytes: ByteArray?): Int = bytes?.get(1)?.toInt() ?: 0

        fun readFirmwareBytes(bytes: ByteArray): Array<Int> {
            val major = bytes[0].toInt()
            val minor = bytes[1].toInt()
            return arrayOf(major, minor)
        }

        fun isResponseOk(data: ByteArray?) =
                BluetoothUtils.bytesToInt(data) == BluetoothServices.BarrierControl.OK_RESPONSE
    }
}