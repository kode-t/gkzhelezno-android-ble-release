package ru.majordom.bleservice.bluetooth.device.gatt.callbacks;


public interface GattErrorListener {
    void onTimeout();
    void onError(String errMessage);
}
