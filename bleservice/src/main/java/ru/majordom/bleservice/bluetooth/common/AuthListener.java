package ru.majordom.bleservice.bluetooth.common;

public interface AuthListener extends ErrorListener {
    void onAuthResponse(Enums.GateStatus status);
}
