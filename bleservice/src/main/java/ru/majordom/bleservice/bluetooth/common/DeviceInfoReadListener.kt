package ru.majordom.bleservice.bluetooth.common

interface DeviceInfoReadListener : ErrorListener {
    fun onTxRead(txLevel: Int)
    fun onTimeoutByMovementRead(timeout: Int)
    fun onFirmwareVersionRead(major: Int, minor: Int)
    fun onCompele()
}