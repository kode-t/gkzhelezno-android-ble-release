package ru.majordom.bleservice.bluetooth.device.interaction;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

public abstract class BaseBleAdapter {
    public boolean startLeScan(BluetoothAdapter.LeScanCallback callback) {
        return true;
    }

    public void stopLeScan(BluetoothAdapter.LeScanCallback callback) {
        //
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startScan(ScanCallback callback) {
        //
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startScan(@Nullable List<ScanFilter> filters, @NonNull ScanSettings.Builder scanSettings, ScanCallback callback) {
        //
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void stopScan(ScanCallback callback) {
        //
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startAdvertising(@NonNull AdvertiseSettings settings, @NonNull AdvertiseData advertiseData, @NonNull AdvertiseCallback callback) {
        //
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startAdvertising(@NonNull AdvertiseSettings settings, @NonNull AdvertiseData advertiseData, @Nullable AdvertiseData scanResponse, @NonNull AdvertiseCallback callback) {
        //
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void stopAdvertising(@NonNull AdvertiseCallback callback) {
        //
    }

    public boolean canAdvertise() {
        return false;
    }

    public BlueteethDevice getRemoteDevice(BlueteethManager manager, String address) {
        return null;
    }

    public boolean isEnabled() {
        return true;
    }
}