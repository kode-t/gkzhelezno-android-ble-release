package ru.majordom.bleservice.bluetooth.device.gatt.callbacks;


public abstract class BaseWriteListener implements GattCharacteristicWriteCallback {
    @Override
    public void onWrite() {
        //
    }

    @Override
    public void onTimeout() {
        //
    }

    @Override
    public void onError(String errMessage) {
        //
    }
}
