package ru.majordom.bleservice.bluetooth.device.gatt.operations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import java.util.UUID;

import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.GattCharacteristicReadCallback;
import ru.majordom.bleservice.bluetooth.device.interaction.BaseGatt;
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;


public class GattCharacteristicReadOperation extends GattDataOperation {
    private final GattCharacteristicReadCallback callback;

    GattCharacteristicReadOperation(BlueteethDevice device, UUID service, UUID characteristic, int bytesCount, GattCharacteristicReadCallback callback) {
        super(device, service, characteristic, bytesCount);
        this.callback = callback;
        hasAvailableCompletionCallback = true;
    }

    @Override
    public void execute() {
        Log.d(TAG, "Reading from {" + characteristic + "}");
        BaseGatt gatt = device.getBluetoothGatt();
        BluetoothGattCharacteristic gattCharacteristic = getCharacteristic(gatt);
        if (gattCharacteristic != null) {
            gatt.readCharacteristic(gattCharacteristic);
        }
    }

    @Override
    public void onTimeout() {
        super.onTimeout();
        if (callback != null) {
            callback.onTimeout();
        }
    }

    @Override
    public void onError(String errorMsg) {
        super.onError(errorMsg);
        if (callback != null) {
            callback.onError(errorMsg);
        }
    }

    public void onRead(BluetoothGattCharacteristic characteristic) {
        if (callback != null) {
            callback.onRead(characteristic.getValue());
        }
        super.onOperationCompleted();
    }
}