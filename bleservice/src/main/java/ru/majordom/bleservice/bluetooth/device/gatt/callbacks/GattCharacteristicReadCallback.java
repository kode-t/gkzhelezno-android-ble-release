package ru.majordom.bleservice.bluetooth.device.gatt.callbacks;

public interface GattCharacteristicReadCallback extends GattErrorListener {
    void onRead(byte[] characteristic);
}
