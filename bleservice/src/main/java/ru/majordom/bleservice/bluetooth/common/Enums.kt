package ru.majordom.bleservice.bluetooth.common

class Enums {

    enum class GateStatus constructor(val code: Int) {
        SUCCESS(1),
        DATA_SET(36)
    }
}
