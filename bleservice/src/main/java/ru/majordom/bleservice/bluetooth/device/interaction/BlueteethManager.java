package ru.majordom.bleservice.bluetooth.device.interaction;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import ru.majordom.bleservice.bluetooth.BluetoothUtils;
import ru.majordom.bleservice.bluetooth.device.gatt.GattOperationBundle;
import ru.majordom.bleservice.bluetooth.device.gatt.operations.GattOperation;
import ru.majordom.bleservice.bluetooth.device.listeners.OnAdvertisingListener;
import ru.majordom.bleservice.bluetooth.device.listeners.OnDeviceDiscoveredListener;
import ru.majordom.bleservice.bluetooth.device.listeners.OnScanCompletedListener;

// TODO: Fix support for pre-Lollipop vs post
public class BlueteethManager {
    private final static String TAG = BlueteethManager.class.getSimpleName();

    static volatile BlueteethManager singleton = null;
    protected GattOperation currentOperation;
    private Context context;
    private BaseBleAdapter bleAdapter;
    private Handler handler;
    private boolean isScanning;
    private List<BlueteethDevice> scannedPeripherals = new ArrayList<>();
    private ConcurrentLinkedQueue<GattOperation> queue;
    private AsyncTask<Void, Void, Void> currentOperationTimeout;

    @Nullable private OnScanCompletedListener onScanCompletedListener;
    @Nullable private OnDeviceDiscoveredListener onDeviceDiscoveredListener;
    @Nullable private OnAdvertisingListener onAdvertisingListener;

    private BluetoothAdapter.LeScanCallback leScanCallback;
    private ScanCallback scanCallback;
    private AdvertiseCallback advertiseCallback;

    /***
     * Explicitly specified, package-protected default constructor
     */
    BlueteethManager() {
    }

    BlueteethManager(Context applicationContext, BaseBleAdapter adapter, Handler handler) {
        // Grab the application context in case an activity context was passed in
        context = applicationContext.getApplicationContext();
        bleAdapter = adapter;
        this.handler = handler;

        if (!bleAdapter.isEnabled()) {
            Log.w(TAG, "Bluetooth is not enabled.");
            //throw new RuntimeException();
        }

        queue = new ConcurrentLinkedQueue<>();
        currentOperation = null;
    }

    public static BlueteethManager with(Context context, BaseBleAdapter adapter, Handler handler) {
        if (singleton == null) {
            synchronized (BlueteethManager.class) {
                if (singleton == null) {
                    singleton = new Builder(context, adapter, handler).build();
                }
            }
        }
        return singleton;
    }

    private Runnable stopScanRunnable = new Runnable() {
        @Override
        public void run() {
            stopScanForPeripherals();
        }
    };

    /**
     * Set the global instance returned from {@link #with}.
     * <p/>
     * This method must be called before any calls to {@link #with} and may only be called once.
     */
    public static void setSingletonInstance(BlueteethManager blueteethManager) {
        if (blueteethManager == null) {
            throw new IllegalArgumentException("Picasso must not be null.");
        }
        synchronized (BlueteethManager.class) {
            if (singleton != null) {
                throw new IllegalStateException("Singleton instance already exists.");
            }
            singleton = blueteethManager;
        }
    }

    public boolean isScanning() {
        return isScanning;
    }

    public boolean canAdvertise() {
        return bleAdapter.canAdvertise();
    }

    public boolean isAdvertising() {
        return advertiseCallback != null;
    }

    /***
     * Returns a list of the stored peripherals
     *
     * @return List of all the scanned for devices
     */
    @NonNull
    public List<BlueteethDevice> getPeripherals() {
        return scannedPeripherals;
    }

    /***
     * Returns a BlueteethDevice directly from a non-null macAddress
     *
     * @return List of all the scanned for devices
     */
    @NonNull
    public BlueteethDevice getPeripheral(@NonNull String macAddress) {
        if (!BluetoothAdapter.checkBluetoothAddress(macAddress)) {
            throw new IllegalArgumentException("MacAddress is null or ill-formed");
        }
        return bleAdapter.getRemoteDevice(this, macAddress);
    }

    @NonNull
    public boolean removePeripheral(@NonNull String macAddress, boolean withConnectionCallback) {
        BlueteethDevice device = this.getPeripheral(macAddress);
        return this.removePeripheral(device, withConnectionCallback);
    }

    @NonNull
    public boolean removePeripheral(@NonNull BlueteethDevice device, boolean withConnectionCallback) {
        if (withConnectionCallback) {
            device.disconnect();
        } else {
            device.close();
        }
        return scannedPeripherals.remove(device);
    }

    public BaseBleAdapter getBleAdapter() {
        return bleAdapter;
    }

    @NonNull
    public boolean removePeripheral(@NonNull BlueteethDevice device) {
        return removePeripheral(device, false);
    }

    public Context getContext() {
        return context;
    }

    public boolean isBleEnabled() {
        return bleAdapter.isEnabled();
    }

    /**
     * Scans for nearby peripherals and fills the scannedPeripherals ArrayList.
     * Scan will be stopped after input timeout.
     *
     * @param deviceDiscoveredListener callback will be called after each new device discovery
     */
    public void scanForPeripherals(OnDeviceDiscoveredListener deviceDiscoveredListener) {
        Log.d(TAG, "scanForPeripherals");
        onDeviceDiscoveredListener = deviceDiscoveredListener;
        scanForPeripherals();
    }

    /**
     * Scans for nearby peripherals and fills the scannedPeripherals ArrayList.
     * Scan will be stopped after input timeout.
     *
     * @param scanTimeoutMillis        timeout in milliseconds after which scan will be stopped
     * @param deviceDiscoveredListener callback will be called after each new device discovery
     * @param scanCompletedListener    callback will be called after scanTimeoutMillis,
     *                                 filled with nearby peripherals
     */
    public void scanForPeripherals(int scanTimeoutMillis, OnDeviceDiscoveredListener deviceDiscoveredListener, OnScanCompletedListener scanCompletedListener) {
        Log.d(TAG, "scanForPeripherals");
        onDeviceDiscoveredListener = deviceDiscoveredListener;
        scanForPeripherals(scanTimeoutMillis, scanCompletedListener);
    }

    /**
     * Scans for nearby peripherals and fills the scannedPeripherals ArrayList.
     * Scan will be stopped after input timeout.
     *
     * @param scanTimeoutMillis     timeout in milliseconds after which scan will be stoped
     * @param scanCompletedListener callback will be called after scanTimeoutMillis,
     *                              filled with nearby peripherals
     */
    public void scanForPeripherals(int scanTimeoutMillis, OnScanCompletedListener scanCompletedListener) {
        Log.d(TAG, "scanForPeripheralsWithTimeout");
        onScanCompletedListener = scanCompletedListener;
        scanForPeripherals();
        handler.postDelayed(stopScanRunnable, scanTimeoutMillis);
    }

    /**
     * Scans for nearby peripherals (no timeout) and fills the scannedPeripherals ArrayList.
     */
    public void scanForPeripherals() {
        Log.d(TAG, "scanForPeripherals");
        if (!isScanning) {
            clearPeripherals();
            isScanning = true;
            if (bleAdapter.isEnabled()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    scanCallback = createBleScanCallback();
                    bleAdapter.startScan(scanCallback);
                } else {
                    leScanCallback = createLeScanCallback();
                    bleAdapter.startLeScan(leScanCallback);
                }
            } else {
                Log.w(TAG, "scanForPeripherals: called when BLE is off.");
            }
        } else {
            Log.w(TAG, "scanForPeripherals: called when there is a scan in process.");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startAdvertising(@NonNull AdvertiseSettings settings, @NonNull AdvertiseData advertiseData, @NonNull OnAdvertisingListener callback) {
        startAdvertising(settings, advertiseData, null, callback);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startAdvertising(@NonNull AdvertiseSettings settings, @NonNull AdvertiseData advertiseData, @Nullable AdvertiseData scanResponse, @NonNull OnAdvertisingListener callback) {
        if (!isAdvertising()) {
            advertiseCallback = createAdvertiseCallback();
            onAdvertisingListener = callback;
            bleAdapter.startAdvertising(settings, advertiseData, scanResponse, advertiseCallback);
        } else {
            Log.e(TAG, "Trying to advertise while advertising");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void stopAdvertising() {
        if (isAdvertising()) {
            bleAdapter.stopAdvertising(advertiseCallback);
            if (onAdvertisingListener != null) {
                onAdvertisingListener.onStop();
            }
            advertiseCallback = null;
            onAdvertisingListener = null;
        } else {
            Log.e(TAG, "Trying to stop advertise while not advertising");
        }
    }

    private void clearPeripherals() {
        // TODO: Need to be a bit clever about how these are handled
        // TODO: If this is the last reference, close it, otherwise don't?
        // fix concurrency error
        BlueteethDevice[] devices = scannedPeripherals.toArray(new BlueteethDevice[scannedPeripherals.size()]);
        for (BlueteethDevice blueteethDevice : devices) {
            if (blueteethDevice != null && !blueteethDevice.isInUse()) {
                this.removePeripheral(blueteethDevice);
            }
        }
    }

    /**
     * Stops ongoing scan process
     */
    public void stopScanForPeripherals() {
        Log.d(TAG, "stopScanForPeripherals");
        if (isScanning) {
            handler.removeCallbacks(stopScanRunnable);
            isScanning = false;
            if (bleAdapter.isEnabled()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    bleAdapter.stopScan(scanCallback);
                    scanCallback = null;
                } else {
                    bleAdapter.stopLeScan(leScanCallback);
                    leScanCallback = null;
                }
            } else {
                Log.w(TAG, "stopScanForPeripherals: called when BLE is off.");
            }

            if (onDeviceDiscoveredListener != null) {
                onDeviceDiscoveredListener = null;
            }

            if (onScanCompletedListener != null) {
                onScanCompletedListener.call(scannedPeripherals);
                onScanCompletedListener = null;
            }
        } else {
            Log.w(TAG, "stopScanForPeripherals: called when there is no scan going.");
        }
    }

    /**
     * Stops ongoing scan process
     */
    public void stopScanForPeripheralsWithoutCallbacks() {
        Log.d(TAG, "stopScanForPeripheralsWithoutCallbacks");
        isScanning = false;
        if (bleAdapter.isEnabled()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bleAdapter.stopScan(scanCallback);
                scanCallback = null;
            } else {
                bleAdapter.stopLeScan(leScanCallback);
                leScanCallback = null;
            }
        } else {
            Log.w(TAG, "stopScanForPeripheralsWithoutCallbacks: called when BLE is off.");
        }

        if (onDeviceDiscoveredListener != null) {
            onDeviceDiscoveredListener = null;
        }

        if (onScanCompletedListener != null) {
            onScanCompletedListener = null;
        }
    }

    /**
     * @param device need to check whether current operation executes on it
     * @param errorMsg pass null to call bundle's onTimeout
     */
    public synchronized void cancelCurrentOperationBundle(@NonNull BlueteethDevice device, @Nullable String errorMsg) {
        if (currentOperation != null
                && currentOperation.getDevice().equals(device)) {
            cancelCurrentOperationBundleInternal(errorMsg);
        }
    }

    private BluetoothAdapter.LeScanCallback createLeScanCallback() {
        return new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                BlueteethDevice blueteethDevice = new BlueteethDevice(BlueteethManager.this, device, rssi, scanRecord);
                scannedPeripherals.add(blueteethDevice);
                if (onDeviceDiscoveredListener != null) {
                    onDeviceDiscoveredListener.call(blueteethDevice);
                }
            }
        };
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private ScanCallback createBleScanCallback() {
        return new ScanCallback() {

            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                BlueteethDevice blueteethDevice = new BlueteethDevice(BlueteethManager.this, result);
                scannedPeripherals.add(blueteethDevice);
                if (onDeviceDiscoveredListener != null) {
                    onDeviceDiscoveredListener.call(blueteethDevice);
                }
            }
        };
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private AdvertiseCallback createAdvertiseCallback() {
        return new AdvertiseCallback() {
            @Override
            public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                if (onAdvertisingListener != null) {
                    onAdvertisingListener.onSuccessStart();
                }
            }

            @Override
            public void onStartFailure(int errorCode) {
                if (onAdvertisingListener != null) {
                    Log.e(TAG, "Failed to start, error code: " + errorCode);
                    onAdvertisingListener.onError("Failed to start advertise.");
                }
            }
        };
    }

    private synchronized void cancelCurrentOperationBundleInternal(@Nullable String errorMsg) {
        Log.v(TAG, "Cancelling current operation. Queue size before: " + queue.size());
        if (currentOperation != null && currentOperation.getBundle() != null) {
            GattOperationBundle bundle = currentOperation.getBundle();
            for (GattOperation op : bundle.getOperations()) {
                queue.remove(op);
            }
            if (errorMsg == null) {
                bundle.onTimeout();
            } else {
                bundle.onError(errorMsg);
            }
        }
        Log.v(TAG, "Queue size after: " + queue.size());
        nullCurrentOperationAndDrive();
    }

    private synchronized void queue(GattOperation gattOperation) {
        queue.add(gattOperation);
        Log.d(TAG, "Queueing Gatt operation, size will now become: " + queue.size());
        drive();
    }

    private synchronized void drive() {
        if (currentOperation != null) {
            Log.i(TAG, "Tried to drive, but currentOperation was not null, " + currentOperation);
            return;
        }
        if (currentOperationTimeout != null) {
            currentOperationTimeout.cancel(true);
        }
        if (queue.size() == 0) {
            Log.v(TAG, "Queue empty, drive loop stopped.");
            currentOperation = null;
            return;
        }

        final GattOperation operation = queue.poll();
        Log.v(TAG, "Driving Gatt queue, size will now become: " + queue.size());
        setCurrentOperation(operation);

        currentOperationTimeout = new AsyncTask<Void, Void, Void>() {
            @Override
            protected synchronized Void doInBackground(Void... voids) {
                try {
                    Log.v(TAG, "Starting to do a background timeout");
                    wait(GattOperation.DEFAULT_TIMEOUT_IN_MILLS + operation.getDelay());
                } catch (InterruptedException e) {
                    Log.v(TAG, "was interrupted out of the timeout");
                }
                if (isCancelled()) {
                    Log.v(TAG, "The timeout was cancelled, so we do nothing.");
                    return null;
                }
                Log.v(TAG, "Timeout ran to completion, time to cancel the entire operation bundle. Abort, abort!");
                cancelCurrentOperationBundleInternal(null);
                return null;
            }

            @Override
            protected synchronized void onCancelled() {
                super.onCancelled();
                notify();
            }
        }.execute();

        execute(operation);
    }

    protected void execute(final GattOperation operation) {
        if (operation != currentOperation) {
            return;
        }
        if (operation.canExecute()) {
            Runnable opExecution = new Runnable() {
                @Override
                public void run() {
                    operation.execute();
                    if (!operation.hasAvailableCompletionCallback()) {
                        nullCurrentOperationAndDrive();
                    }
                }
            };
            if (operation.getDelay() > 0) {
                Log.d(TAG, "Execution operation with delay: " + operation.getDelay());
                handler.postDelayed(opExecution, operation.getDelay());
            } else {
                handler.post(opExecution);
            }
        } else {
            String operationError = operation.getLastInternalCheckError();
            if (operationError == null || operationError.isEmpty()) {
                Log.w(TAG, "Operation without a device or GATT skipped: " + operation);
                nullCurrentOperationAndDrive();
            } else {
                // trying to write wrong bytes count
                cancelCurrentOperationBundleInternal(operation.getLastInternalCheckError());
            }
        }
    }

    public void nullCurrentOperationAndDrive() {
        setCurrentOperation(null);
        drive();
    }

    public GattOperation getCurrentOperation() {
        return currentOperation;
    }

    public synchronized void setCurrentOperation(GattOperation currentOperation) {
        this.currentOperation = currentOperation;
    }

    public void queue(GattOperationBundle bundle) {
        queue.addAll(bundle.getOperations());
        /*for (GattOperation operation : bundle.getOperations()) {
            queue(operation);
        }*/
        Log.d(TAG, "Size of queue: " + queue.size());
        drive();
    }

    static class Builder {
        private final Context context;
        private final BaseBleAdapter adapter;
        private final Handler handler;

        /**
         * Start building a new {@link BlueteethManager} instance.
         */
        public Builder(@NonNull Context context, @NonNull BaseBleAdapter adapter, @NonNull Handler handler) {
            this.context = context.getApplicationContext();
            this.adapter = adapter;
            this.handler = handler;
        }

        /**
         * Create the {@link BlueteethManager} instance.
         */
        public BlueteethManager build() {
            return new BlueteethManager(context, adapter, handler);
        }
    }
}