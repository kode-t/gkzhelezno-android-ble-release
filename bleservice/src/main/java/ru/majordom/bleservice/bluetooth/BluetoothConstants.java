package ru.majordom.bleservice.bluetooth;

import android.os.Build;
import android.support.annotation.IntRange;

import java.util.HashMap;
import java.util.Map;

public class BluetoothConstants {
    public static final int ONE_SECOND = 1000;

    //
    public static final String HASH_SALT = "26032016";
    public static final String GATE_GENERAL_NAME = "MAJORBLE";
    public static final String UUID_COMMON_PART = "-1212-EFDE-1523-785FEABCD123";
    public static final String GATE_ADVERTISING_MARKER = "00006D62-0000-1000-8000-00805F9B34FB";

    // ble device behaviour constants
    private static final int L_TIME = 4;
    private static final int M_TIME = 5;
    private static final int N_TIME = 6;

    public static final int SCAN_POWER_LOW = 0;
    public static final int SCAN_POWER_MID = 1;
    public static final int SCAN_POWER_HIGH = 2;

    public static final int CONN_PRIORITY_LOW = 0;
    public static final int CONN_PRIORITY_MID = 1;
    public static final int CONN_PRIORITY_HIGH = 2;

    public static final int WIEGAND_26 = 26;
    public static final int WIEGAND_34 = 34;
    public static final int WIEGAND_42 = 42;

    public static final int MIN_RSSI_LEVEL = -100;
    public static final long CONNECTION_TIMEOUT = 3 * ONE_SECOND;
    public static final long DISCOVERING_TIMEOUT = 5 * ONE_SECOND;
    public static final long GATE_STATUS_EXPLICIT_READ_AFTER = 5 * ONE_SECOND;
    public static final long PAUSE_DEFAULT = 5 * BluetoothConstants.ONE_SECOND;
    public static final long PAUSE_CONN_AUTH_ERROR = BluetoothConstants.ONE_SECOND;
    public static final long PAUSE_AFTER_TECHNICAL_ISSUE = 4 * BluetoothConstants.ONE_SECOND;
    public static final long PAUSE_AFTER_NETWORK_ISSUE = 14 * BluetoothConstants.ONE_SECOND;
    public static final int SCAN_MODE = SCAN_POWER_HIGH;
    public static final int CONN_PRIORITY = CONN_PRIORITY_HIGH;
    public static final int DEFAULT_DISTANCE_LIMIT = 3;
    public static final int DEFAULT_K_COEFFICIENT = 60;
    public static final int DEFAULT_WIEGAND = WIEGAND_26;
    public static final long DEFAULT_CODE = 5156418;
    public static final int DEFAULT_READER_POWER = 7;
    public static final Map<Integer, Integer> READER_POWER_TABLE = new HashMap<Integer, Integer>(9, 1.0f) {
        {
            put(1, -40);
            put(2, -20);
            put(3, -16);
            put(4, -12);
            put(5, -8);
            put(6, -4);
            put(7, 0);
            put(8, 3);
            put(9, 4);
        }
    };

    private static volatile int minRssiLevel = MIN_RSSI_LEVEL;
    private static volatile long connectionTimeout = CONNECTION_TIMEOUT;
    private static volatile long discoveringTimeout = DISCOVERING_TIMEOUT;
    private static volatile long gateStatusReadAfter = GATE_STATUS_EXPLICIT_READ_AFTER;
    private static volatile long scanTimeout = getStandardScanTimeoutBasedOnApi();
    private static volatile long pauseDefault = PAUSE_DEFAULT;
    private static volatile long pauseConnAuthError = PAUSE_CONN_AUTH_ERROR;
    private static volatile long pauseAfterTech = PAUSE_AFTER_TECHNICAL_ISSUE;
    private static volatile long pauseAfterNetwork = PAUSE_AFTER_NETWORK_ISSUE;
    private static volatile int scanMode = SCAN_MODE;
    private static volatile int connPriority = CONN_PRIORITY;
    private static volatile int distanceLimit = DEFAULT_DISTANCE_LIMIT;
    private static volatile int kCoefficient = DEFAULT_K_COEFFICIENT;
    private static volatile int wiegandVersion = DEFAULT_WIEGAND;
    private static volatile long readerCode = DEFAULT_CODE;
    private static volatile int readerPower = DEFAULT_READER_POWER;

    // DO NOT CALL THESE DIRECTLY, RATHER USE Prefs.BleOptions instance

    public static int getMinRssiLevel() {
        return minRssiLevel;
    }

    public static void setMinRssiLevel(int level) {
        minRssiLevel = level;
    }

    public static long getConnectionTimeout() {
        return connectionTimeout;
    }

    public static void setConnectionTimeout(long timeout) {
        connectionTimeout = timeout;
    }

    public static long getGateStatusReadAfter() {
        return gateStatusReadAfter;
    }

    public static void setGateStatusReadAfter(long timeout) {
        gateStatusReadAfter = timeout;
    }

    public static long getScanTimeout() {
        return scanTimeout;
    }

    public static void setScanTimeout(long timeout) {
        scanTimeout = timeout;
    }

    public static long getDiscoveringTimeout() {
        return discoveringTimeout;
    }

    public static void setDiscoveringTimeout(long timeout) {
        discoveringTimeout = timeout;
    }

    public static long getStandardScanTimeoutBasedOnApi() {
        int standardTime = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            standardTime = L_TIME;
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            standardTime = M_TIME;
        } else {
            standardTime = N_TIME;
        }
        return standardTime * ONE_SECOND;
    }

    public static long getPauseDefault() {
        return pauseDefault;
    }

    public static void setPauseDefault(long pause) {
        pauseDefault = pause;
    }

    public static long getPauseConnAuthError() {
        return pauseConnAuthError;
    }

    public static void setPauseConnAuthError(long pause) {
        pauseConnAuthError = pause;
    }

    public static long getPauseAfterTechnicalIssue() {
        return pauseAfterTech;
    }

    public static void setPauseAfterTechnicalIssue(long pause) {
        pauseAfterTech = pause;
    }

    public static long getPauseAfterNetwork() {
        return pauseAfterNetwork;
    }

    public static void setPauseAfterNetwork(long pause) {
        pauseAfterNetwork = pause;
    }

    public static int getScanMode() {
        return scanMode;
    }

    public static void setScanMode(@IntRange(from = SCAN_POWER_LOW, to = SCAN_POWER_HIGH) int mode) {
        scanMode = mode;
    }

    public static int getConnPriority() {
        return connPriority;
    }

    public static void setConnPriority(@IntRange(from = CONN_PRIORITY_LOW, to = CONN_PRIORITY_HIGH) int priority) {
        connPriority = priority;
    }

    public static int getDistanceLimit() {
        return distanceLimit;
    }

    public static void setDistanceLimit(int distanceLimit) {
        BluetoothConstants.distanceLimit = distanceLimit;
    }

    public static int getkCoefficient() {
        return kCoefficient;
    }

    public static void setkCoefficient(int kCoefficient) {
        BluetoothConstants.kCoefficient = kCoefficient;
    }

    public static int getWiegandVersion() {
        return wiegandVersion;
    }

    public static void setWiegandVersion(int wiegandVersion) {
        BluetoothConstants.wiegandVersion = wiegandVersion;
    }

    public static long getReaderCode() {
        return readerCode;
    }

    public static void setReaderCode(long readerCode) {
        BluetoothConstants.readerCode = readerCode;
    }

    public static int getReaderPower() {
        return readerPower;
    }

    public static void setReaderPower(int readerPower) {
        BluetoothConstants.readerPower = readerPower;
    }
}