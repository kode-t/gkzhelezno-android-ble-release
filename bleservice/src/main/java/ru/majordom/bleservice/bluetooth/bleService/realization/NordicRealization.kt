package ru.majordom.bleservice.bluetooth.bleService.realization


import android.bluetooth.BluetoothDevice
import android.content.Context
import android.util.Log
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat
import no.nordicsemi.android.support.v18.scanner.ScanCallback
import no.nordicsemi.android.support.v18.scanner.ScanResult
import ru.majordom.bleservice.R
import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.bluetooth.common.CommonBleDevice
import ru.majordom.bleservice.bluetooth.device.interaction.BaseBleAdapter
import ru.majordom.bleservice.bluetooth.device.interaction.RealBleAdapter
import ru.majordom.bleservice.bluetooth.nordic.interaction.DoorDeviceModule
import ru.majordom.bleservice.bluetooth.nordic.interaction.searchstrategy.FirsDeviceSearchStrategy
import ru.majordom.bleservice.common.LocalStorage

class NordicRealization(
        context: Context,
        bluetoothManager: BluetoothManagerContract,
        listener: Listener,
        localStorage: LocalStorage
) : BaseRealization<BaseBleAdapter, ScanResult>(context, bluetoothManager, listener, localStorage) {

    private val gateModule: DoorDeviceModule
    private val scanner: BluetoothLeScannerCompat
    private var devicesScanCallback: ScanCallback? = null

    override val realizationInfo: String
        get() = "BLE works through Nordic SDK."

    init {
        bleManager = RealBleAdapter(bluetoothManager.bleAdapter)
        gateModule = DoorDeviceModule(context, localStorage.bleOptions)
        scanner = BluetoothLeScannerCompat.getScanner()
    }

    override fun onSearchForGate(listener: BaseRealization.Listener) {
        devicesScanCallback = gateModule.searchForGate(
                scanner,
                bluetoothManager,
                FirsDeviceSearchStrategy(localStorage.bleOptions),
                object : DoorDeviceModule.SearchListener {
                    private var isAlive = true

                    override fun onSearchCompleted(device: ScanResult) {
                        if (isAlive)
                            isAlive = false
                        else
                            return
                        //failedSearchAttempts = 0;
                        if (!gateState.isOpeningInProcess) {
                            stopScanInternal()
                            listener.onSearchCompleted()
                            this@NordicRealization.onDeviceFound(device.device.name)
                            processFoundDevice(CommonBleDevice.from(device))
                        }
                    }

                    override fun onNoDeviceFound() {
                        if (isAlive)
                            isAlive = false
                        else
                            return

                        if (!gateState.isOpeningInProcess) {
                            listener.apply {
                                onSendMessage(R.string.bluetooth_search_no_device)
                                this@NordicRealization.onNoDeviceFoundInternal()
                                onDisconnectFromDevice()
                                onResumeConnection(0)
                            }
                        }
                    }

                    override fun onLogInfo(message: String, isError: Boolean) {
                        if (isError) {
                            gateState.disconnect()
                            listener.onSendGateStatus()
                        }
                        listener.onLogGlobal(true, message)
                    }

                    override fun onError(errorMsg: String) {
                        if (isAlive)
                            isAlive = false
                        else
                            return

                        if (!gateState.isOpeningInProcess) {
                            listener.apply {
                                onSendMessage(errorMsg)
                                onScanError(errorMsg)
                                onDisconnectFromDevice()
                                onResumeConnection(PAUSE_CONN_AUTH_ERROR)
                            }
                        }
                    }

                    override fun onTimeout(errorMsg: String) {
                        this.onError(errorMsg)
                    }
                })
    }

    override fun stopScan() {
        super.stopScan()
        stopScanInternal()
    }

    override fun release() {
        super.release()
        gateModule.close()
    }

    override fun onConnectAndOpenGate() {
        connect { authGateUser() }
    }

    override fun onAuthProceed() {
        gateModule.authenticate(
                deviceWrapper?.device?.device,
                defaultAuthListener)
    }

    override fun readDeviceSettings() {
        connect {
            gateModule.readDeviceInfo(
                    deviceWrapper?.device?.device,
                    defaultDeviceReadListener)
        }
    }

    override fun onDisconnectFromDevice(): Boolean {
        gateModule.disconnect()
        deviceWrapper = null
        return true
    }

    private fun stopScanInternal() {
        devicesScanCallback?.let {
            if (bluetoothManager.isBluetoothEnabled) scanner.stopScan(it)
        }
        devicesScanCallback = null
    }

    private fun connect(onDone: () -> Unit) {
        gateModule.connectToTheGate(
                deviceWrapper?.device?.device,
                object : DoorDeviceModule.ConnectionListener {

                    override fun onGateConnection(device: BluetoothDevice, isConnected: Boolean) {
                        gateState.isConnected = isConnected
                        if (isConnected) {
                            currentDisconnectCount = 0
                            val connectedMessage = context.getString(R.string.gate_msg_connected)
                            listener.onSendMessage(connectedMessage)
                            listener.onLogGlobal(true, "$connectedMessage.")
                        } else {
                            if (!gateWasOpened && !gateErrorCodeReceived) { // gate found, but connection failed for some reason
                                Log.d(TAG, "Disconnected: gate found, but connection failed for some reason.")
                                listener.onLogGlobal(true, "Connection failed for some reason.")
                                processConnPriorityLogic()
                                disconnectInternal()
                                listener.onResumeConnection(PAUSE_CONN_AUTH_ERROR)
                            }
                        }
                    }

                    override fun onServicesDiscovered(device: BluetoothDevice) {
                        listener.onLogGlobal(true, "Services discovered.")
                        onDone.invoke()
                    }

                    override fun onError(errorMsg: String) {
                        listener.apply {
                            onFailedConnectAndOpen(errorMsg)
                            onSendMessage(errorMsg)
                            processConnPriorityLogic()
                            onDisconnectFromDevice()
                            onResumeConnection(PAUSE_CONN_AUTH_ERROR)
                        }
                    }

                    override fun onTimeout(errorMsg: String) = this.onError(errorMsg)
                })
    }
}