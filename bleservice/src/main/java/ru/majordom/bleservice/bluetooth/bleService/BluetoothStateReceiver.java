package ru.majordom.bleservice.bluetooth.bleService;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;


public class BluetoothStateReceiver extends BroadcastReceiver {

    private final BluetoothStateListener listener;

    public BluetoothStateReceiver(@NonNull BluetoothStateListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
            switch (bluetoothState) {
                case BluetoothAdapter.STATE_OFF:
                    listener.onBluetoothUnavailable();
                    break;
                case BluetoothAdapter.STATE_ON:
                    listener.onBluetoothAvailable();
                    break;
            }
        }
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        return intentFilter;
    }

    public interface BluetoothStateListener {
        void onBluetoothAvailable();
        void onBluetoothUnavailable();
    }
}
