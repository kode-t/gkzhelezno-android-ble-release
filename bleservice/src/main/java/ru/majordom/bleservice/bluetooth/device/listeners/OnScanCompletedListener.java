package ru.majordom.bleservice.bluetooth.device.listeners;


import java.util.List;

import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

public interface OnScanCompletedListener {
    void call(List<BlueteethDevice> blueteethDevices);
}
