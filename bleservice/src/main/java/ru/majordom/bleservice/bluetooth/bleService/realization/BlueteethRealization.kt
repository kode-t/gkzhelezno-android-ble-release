package ru.majordom.bleservice.bluetooth.bleService.realization

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import ru.majordom.bleservice.R
import ru.majordom.bleservice.bluetooth.BluetoothManagerContract
import ru.majordom.bleservice.bluetooth.common.CommonBleDevice
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice
import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethManager
import ru.majordom.bleservice.bluetooth.device.interaction.RealBleAdapter
import ru.majordom.bleservice.bluetooth.device.interaction.RealGatt
import ru.majordom.bleservice.bluetooth.device.listeners.OnConnectionChangedListener
import ru.majordom.bleservice.bluetooth.interaction.DoorDeviceModule
import ru.majordom.bleservice.bluetooth.interaction.search_strategy.FirsDeviceSearchStrategy
import ru.majordom.bleservice.common.LocalStorage

class BlueteethRealization(
        context: Context,
        bluetoothManager: BluetoothManagerContract,
        listener: BaseRealization.Listener,
        localStorage: LocalStorage
) : BaseRealization<BlueteethManager, BlueteethDevice>(context, bluetoothManager, listener, localStorage) {

    private val doorModule = DoorDeviceModule(context, localStorage.bleOptions)

    override val realizationInfo: String
        get() = "BLE works through Vanilla."

    init {
        val adapter = RealBleAdapter(bluetoothManager.bleAdapter)
        bleManager = BlueteethManager.with(context, adapter, Handler(Looper.getMainLooper()))
    }

    override fun onSearchForGate(listener: BaseRealization.Listener) {
        doorModule.searchForGate(
                bleManager,
                FirsDeviceSearchStrategy(bleManager, localStorage.bleOptions),
                object : DoorDeviceModule.SearchListener {
                    override fun onSearchCompleted(device: BlueteethDevice?) {
                        if (device != null) {
                            if (isAlive)
                                isAlive = false
                            else
                                return
                            if (!gateState.isOpeningInProcess) {
                                listener.onSearchCompleted()
                                this@BlueteethRealization.onDeviceFound(device.name)
                                processFoundDevice(CommonBleDevice.from(device))
                            }
                        } else onNoDeviceFound()
                    }

                    private var isAlive = true

                    override fun onNoDeviceFound() {
                        if (isAlive)
                            isAlive = false
                        else
                            return

                        if (!gateState.isOpeningInProcess) listener.apply {
                            onSendMessage(R.string.bluetooth_search_no_device)
                            this@BlueteethRealization.onNoDeviceFoundInternal()
                            onDisconnectFromDevice()
                            onResumeConnection(0)
                        }
                    }

                    override fun onError(errorMsg: String) {
                        if (isAlive)
                            isAlive = false
                        else
                            return

                        if (!gateState.isOpeningInProcess) listener.apply {
                            onSendMessage(errorMsg)
                            onScanError(errorMsg)
                            onDisconnectFromDevice()
                            onResumeConnection(PAUSE_CONN_AUTH_ERROR)
                        }
                    }

                    override fun onTimeout(errorMsg: String) {
                        this.onError(errorMsg)
                    }
                })
    }

    override fun stopScan() {
        super.stopScan()
        bleManager.stopScanForPeripherals()
    }

    override fun onConnectAndOpenGate() {
        connect { authGateUser() }
    }

    override fun onAuthProceed() {
        doorModule.authenticate(
                deviceWrapper?.device,
                defaultAuthListener)
    }

    override fun onDisconnectFromDevice(): Boolean {
        val isConnected = deviceWrapper?.device?.isConnected == true
        deviceWrapper?.device?.let {
            bleManager.removePeripheral(it)
        }
        deviceWrapper = null
        return isConnected
    }

    override fun readDeviceSettings() {
        connect {
            doorModule.readDeviceInfo(
                    deviceWrapper?.device,
                    defaultDeviceReadListener)
        }
    }

    private fun connect(onDone: () -> Unit) {
        doorModule.connectToTheGate(
                deviceWrapper?.device,
                RealGatt(),
                OnConnectionChangedListener { _, _ -> },
                object : DoorDeviceModule.ConnectionListener {

                    override fun onGateConnection(device: BlueteethDevice, isConnected: Boolean) {
                        gateState.isConnected = isConnected
                        if (isConnected) {
                            currentDisconnectCount = 0
                            val connectedMessage = context.getString(R.string.gate_msg_connected)
                            listener.onSendMessage(connectedMessage)
                            listener.onLogGlobal(true, "$connectedMessage.")
                        } else {
                            if (!gateWasOpened && !gateErrorCodeReceived) { // gate found, but connection failed for some reason
                                Log.d(TAG, "Disconnected: gate found, but connection failed for some reason.")
                                listener.onLogGlobal(true, "Connection failed for some reason.")
                                processConnPriorityLogic()
                                disconnectInternal()
                                listener.onResumeConnection(PAUSE_CONN_AUTH_ERROR)
                            }
                        }
                    }

                    override fun onServicesDiscovered(device: BlueteethDevice) {
                        listener.onLogGlobal(true, "Services discovered.")
                        onDone.invoke()
                    }

                    override fun onError(errorMsg: String) {
                        listener.apply {
                            onFailedConnectAndOpen(errorMsg)
                            onSendMessage(errorMsg)
                            processConnPriorityLogic()
                            onDisconnectFromDevice()
                            onResumeConnection(PAUSE_CONN_AUTH_ERROR)
                        }
                    }

                    override fun onTimeout(errorMsg: String) = this.onError(errorMsg)
                })
    }
}