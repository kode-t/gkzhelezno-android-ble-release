package ru.majordom.bleservice.bluetooth.device.listeners;

public interface OnAdvertisingListener {
    void onSuccessStart();
    void onStop();
    void onError(String errorMsg);
}
