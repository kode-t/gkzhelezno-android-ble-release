package ru.majordom.bleservice.bluetooth.interaction

import android.content.Context
import android.os.Handler

import ru.majordom.bleservice.R
import ru.majordom.bleservice.bluetooth.common.ErrorListener
import ru.majordom.bleservice.bluetooth.device.gatt.callbacks.BaseBundleListener


class SimpleBundleListener(
        private val context: Context,
        private val uiHandler: Handler,
        private val errorListener: ErrorListener?
) : BaseBundleListener() {

    override fun onTimeout() {
        uiHandler.post {
            errorListener?.onTimeout(context.getString(R.string.bluetooth_operation_timeout))
        }
    }

    override fun onError(errMessage: String) {
        uiHandler.post {
            errorListener?.onError(errMessage)
        }
    }
}
