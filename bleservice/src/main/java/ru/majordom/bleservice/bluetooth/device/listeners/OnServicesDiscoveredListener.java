package ru.majordom.bleservice.bluetooth.device.listeners;


import ru.majordom.bleservice.bluetooth.device.interaction.BlueteethDevice;

public interface OnServicesDiscoveredListener {
    void call(BlueteethDevice.BlueteethResponse response);
}
