<resources>

    <!-- Common -->
    <string name="app_name">BLE Test</string>
    <string name="yes">Yes</string>
    <string name="no">No</string>

    <!-- Home -->
    <string name="open_gate">Open Gate</string>
    <string name="settings">Settings</string>
    <string name="power_save_disable_description">Your device has Power Saving Mode enabled. Please put BLE Test app to white list or this may affect gate opening performance.</string>
    <string name="power_save_check">Please make sure that BLE Test App can run in background and allowed to autostart. Disable all auto power management for it. Ignoring this will degrade performance in background.</string>

    <!-- Non-automatic Opening (openGate) -->
    <string name="close">Close</string>
    <string name="gate_detected">Reader detected nearby</string>
    <string name="no_gate_detected">No reader detected nearby</string>
    <string name="move_closer_to_the_gate">Move closer to the reader</string>
    <string name="push_the_button_to_open_the_gate">Push the button to open the reader.</string>
    <string name="non_automatic_opening_settings_hint">This manual function can be overwritten in your Settings menu to open the readers automatically.</string>
    <string name="error_template" formatted="false">Feedback: %s</string>

    <!-- Non-automatic Opening - gate responses -->
    <string name="gate_msg_opened">Code was sent.</string>
    <string name="gate_msg_tx_set" translatable="false">Reader TX Power was changed.</string>
    <string name="gate_msg_connecting">Interacting with Reader (Connecting)…</string>
    <string name="gate_msg_connected">Interacting with Reader (Connected)…</string>
    <string name="gate_msg_authenticating">Interacting with Reader (Sending)…</string>
    <string name="gate_msg_authenticated">Interacting with Reader (Sent)…</string>
    <string name="gate_msg_searching">Searching readers nearby…</string>

    <!-- Settings -->
    <string name="automatic_gate_opening">Open Reader Automatically</string>

    <!-- Advanced BLE Parameters - Field Test Mode -->
    <string name="ble_tuning_title">BLE Tuning</string>
    <string name="ble_tuning">Advanced BLE Parameters</string>
    <string name="signal_threshold">Signal Threshold</string>
    <string name="signal_power" translatable="false">Reader Signal Power</string>
    <string name="movement_timeout">Reader Movement Timeout</string>
    <string name="switch_ble_method">Use Nordic</string>
    <string name="menu_to_defaults">defaults</string>
    <string name="msg_reset_parameters">Reset all parameter to default values?</string>
    <string name="switch_field">Field Testing Mode</string>
    <string name="connection_priority">Connection Priority</string>
    <string name="wiegand_version" translatable="false">Wiegand Protocol</string>
    <string name="scan_mode">Scan Power</string>
    <string name="low">low</string>
    <string name="med">med</string>
    <string name="high">high</string>
    <string name="wiegand_26" translatable="false">26</string>
    <string name="wiegand_34" translatable="false">34</string>
    <string name="wiegand_42" translatable="false">42</string>
    <string name="connection_timeout">Connection Timeout; ms</string>
    <string name="discovering_timeout">Service Discovering Timeout; ms</string>
    <string name="notification_read_timeout">Notification Timeout; ms</string>
    <string name="scan_code" translatable="false">Reader Code; number</string>
    <string name="scan_duration">Scan Duration; ms</string>
    <string name="pause_default">Default Pause; ms</string>
    <string name="pause_default_desc">Used after: a) device is found but auto open is disabled; b) successful receive of reader response.</string>
    <string name="pause_conn_auth">Conn/Auth error Pause; ms</string>
    <string name="pause_conn_auth_desc">Used after error during connection/auth/notification processes.</string>
    <string name="pause_tech_issue">Pause, Tech Issue; ms</string>
    <string name="pause_network_issue">Pause, Network Issue; ms</string>
    <string name="distance_limit">Distance Limit</string>
    <string name="distance_units">meters:</string>
    <string name="coefficient_k">K coefficient</string>
    <string name="coefficient_k_desc">used in (TxPower - K):</string>

    <!-- Bluetooth overlay -->
    <string name="bluetooth_required">BLE Test requires Bluetooth enabled.</string>
    <string name="bluetooth_required_hint">Bluetooth must be turned on to allow BLE Test to open reader automatically! This will have very little effect on your phone battery.</string>
    <string name="bluetooth_unavailable">Bluetooth is turned off, please turn it on.</string>
    <string name="bluetooth_dialog_go_back">Close this screen</string>

    <!-- Alerts -->
    <string name="alert_notifications_turned_off">You will need to open the readers manually.</string>

    <!-- Permissions -->
    <string name="permission_location_ble_desc">Please grant location permission in order to initiate bluetooth scanning.\nWithout this you\'ll not be able to open readers.</string>
    <string name="permission_location_ask_enable">Please grant location permission</string>
    <string name="permission_write_external_desc">Please grant external storage permission in order to enable field test mode.</string>
    <string name="permission_write_external_ask_enable">Please grant external storage permission</string>
    <string name="action_settings">Settings</string>
    <string name="error_no_activity_found">No activity found to handle this intent.</string>

    <!-- Geofences -->
    <string name="enable_location_ble_desc">Please enable geolocation to ensure your device could find a reader.</string>

    <!-- Logging -->
    <string name="dir_log">logs/</string>
    <string name="btn_share_logs">Share logs</string>

    <!-- Notifications -->
    <string name="notif_bluetooth_service_is_running">BLE Test service is running.</string>
    <string name="notif_gate_tap_or_expand">Tap or expand to open the reader.</string>
    <string name="notif_action_open_the_gates">Open Reader</string>
    <string name="notif_gate_detected">Reader detected nearby</string>
    <string name="notif_approaching_gate_low_rssi">Approaching the reader, stay tuned!</string>
    <string name="notif_initiating_parking_process">Initiating open process…</string>
    <string name="notif_battery_saver">Your device has Power Saving Mode enabled. This may affect reader opening performance.</string>
    <string name="notif_geolocation_is_not_enabled">Location service isn\'t enabled.</string>
    <string name="notif_title_bluetooth_enabled_due_to_geofence">Bluetooth is enabled.</string>
    <string name="notif_bluetooth_enabled_due_to_geofence">You have entered a reader zone, Bluetooth is automatically enabled.</string>

    <!-- Notifications - Field Test Mode -->
    <string name="notif_approaching_gate_too_far">Approaching reader, it\'s in ~%1.2f meters!</string>


</resources>