package ru.majordom.bletest.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.view.View

class OptionsDialog : DialogFragment() {

    private var dismissListener: DialogInterface.OnDismissListener? = null

    private var messageText: String? = null
    private var title = -1
    private var okListener: View.OnClickListener? = null
    private var cancelListener: View.OnClickListener? = null
    private var textBtnOk: String? = null
    private var textBtnCancel: String? = null

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, "dialog")

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(requireContext())
                .setTitle(if (title > 0) title else ru.majordom.bletest.R.string.app_name)
                .setPositiveButton(if (textBtnOk != null) textBtnOk else resources.getString(android.R.string.ok)
                ) { _, _ ->
                    dismiss()
                    okListener?.onClick(view)
                }
                .setNegativeButton(if (textBtnCancel != null) textBtnCancel else resources.getString(android.R.string.cancel)
                ) { _, _ ->
                    dismiss()
                    cancelListener?.onClick(view)
                }
                .setMessage(messageText)
                .create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        if (dismissListener != null) {
            dismissListener!!.onDismiss(dialog)
        }
        super.onDismiss(dialog)
    }

    fun textBtnOk(resId: Int): OptionsDialog {
        this.textBtnOk = resources.getString(resId)
        return this
    }

    fun textBtnCancel(resId: Int): OptionsDialog {
        this.textBtnCancel = resources.getString(resId)
        return this
    }

    fun title(title: Int): OptionsDialog {
        this.title = title
        return this
    }

    fun okListener(listener: View.OnClickListener): OptionsDialog {
        okListener = listener
        return this
    }

    fun cancelListener(listener: View.OnClickListener): OptionsDialog {
        cancelListener = listener
        return this
    }

    fun cancelable(cancelable: Boolean): OptionsDialog {
        this.isCancelable = cancelable
        return this
    }

    fun message(msgId: Int): OptionsDialog {
        messageText = resources.getString(msgId)
        return this
    }

    fun message(msg: String): OptionsDialog {
        messageText = msg
        return this
    }

    fun setDismissListener(listener: DialogInterface.OnDismissListener): OptionsDialog {
        this.dismissListener = listener
        return this
    }
}
