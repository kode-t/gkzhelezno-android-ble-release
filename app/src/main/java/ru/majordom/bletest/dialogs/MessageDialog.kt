package ru.majordom.bletest.dialogs


import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View.OnClickListener

class MessageDialog : DialogFragment() {

    private var dismissListener: DialogInterface.OnDismissListener? = null

    private var messageText: String? = null
    private var title: String? = null
    private var okListener: OnClickListener? = null

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, "dialog")

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(requireContext())
                .setTitle(if (!TextUtils.isEmpty(title)) title else resources.getString(ru.majordom.bletest.R.string.app_name))
                .setPositiveButton(android.R.string.ok
                ) { _, _ ->
                    dismiss()
                    if (okListener != null)
                        okListener!!.onClick(view)
                }
                .setMessage(messageText)
                .create()
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        if (dismissListener != null) {
            dismissListener!!.onDismiss(dialog)
        }
        super.onDismiss(dialog)
    }

    fun title(@StringRes title: Int): MessageDialog {
        this.title = resources.getString(title)
        return this
    }

    fun title(title: String): MessageDialog {
        this.title = title
        return this
    }

    fun message(@StringRes msgId: Int): MessageDialog {
        messageText = resources.getString(msgId)
        return this
    }

    fun message(msg: String): MessageDialog {
        messageText = msg
        return this
    }

    fun okListener(listener: OnClickListener): MessageDialog {
        okListener = listener
        return this
    }

    fun cancelable(cancelable: Boolean): MessageDialog {
        this.isCancelable = cancelable
        return this
    }

    fun setDismissListener(listener: DialogInterface.OnDismissListener): MessageDialog {
        this.dismissListener = listener
        return this
    }
}