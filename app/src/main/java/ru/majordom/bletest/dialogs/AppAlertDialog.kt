package ru.majordom.bletest.dialogs

import android.content.Context
import android.content.DialogInterface
import android.support.annotation.StyleRes
import android.support.v7.app.AlertDialog


class AppAlertDialog : AlertDialog {

    protected constructor(context: Context) : super(context) {}

    protected constructor(context: Context, @StyleRes themeResId: Int) : super(context, themeResId) {}

    protected constructor(context: Context, cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener?) : super(context, cancelable, cancelListener)

    companion object {

        fun getBuilderWithDefaults(context: Context, message: String): AlertDialog.Builder {
            val info = context.applicationInfo
            return AlertDialog.Builder(context)
                    .setIcon(info.icon)
                    .setTitle(ru.majordom.bletest.R.string.app_name)
                    .setCancelable(false)
                    .setMessage(message)
        }

        fun getBuilderWithDefaults(context: Context, message: String, title: String): AlertDialog.Builder {
            val builder = getBuilderWithDefaults(context, message)
            builder.setTitle(title)
            return builder
        }
    }
}