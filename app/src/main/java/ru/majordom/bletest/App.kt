package ru.majordom.bletest

import android.app.Application
import android.content.Context
import ru.majordom.bleservice.BleServiceConfiguration
import ru.majordom.bleservice.BleServiceInitializer
import ru.majordom.bleservice.common.Logger
import ru.majordom.bleservice.common.LoggerCreator
import ru.majordom.bletest.bleintegration.AppServiceNotificationsDelegate
import ru.majordom.bletest.bleintegration.AppZonesNotificationsDelegate
import ru.majordom.bletest.bleintegration.DumbZonesProvider
import ru.majordom.bletest.util.FileLogger
import ru.majordom.bletest.util.NotificationManagerHelper
import ru.majordom.bletest.util.Prefs
import java.io.File


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        app = this
        initInfrastructure()
    }

    private fun initInfrastructure() {
        NotificationManagerHelper.prepareWith(this)
        val configuration = BleServiceConfiguration(
                AppServiceNotificationsDelegate(this, Prefs.instance),
                AppZonesNotificationsDelegate(),
                DumbZonesProvider(),
                object : LoggerCreator {
                    override fun createFileLogger(
                            context: Context,
                            filesDir: File,
                            fileName: String,
                            append: Boolean): Logger = FileLogger(context, filesDir, fileName, append)
                },
                Prefs.instance
        )
        BleServiceInitializer.setupBleService(this, configuration)
    }

    companion object {
        lateinit var app: App
            private set

        fun runService() = BleServiceInitializer.runServices()
        fun stopService() = BleServiceInitializer.stopServices()
    }
}