package ru.majordom.bletest.activities;


public interface OnBackClickListener {
    boolean onBackClick();
}
