package ru.majordom.bletest.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_advanced_ble.*
import kotlinx.android.synthetic.main.part_card_view_toolbar.*
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
import permissions.dispatcher.*
import ru.majordom.bleservice.InternalsFacade
import ru.majordom.bleservice.bluetooth.BluetoothConstants
import ru.majordom.bleservice.common.BleOptions
import ru.majordom.bletest.App
import ru.majordom.bletest.R
import ru.majordom.bletest.dialogs.OptionsDialog
import ru.majordom.bletest.util.*

@RuntimePermissions
class AdvancedBleActivity : AppCompatActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, AdvancedBleActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_advanced_ble)
        initViews()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_adv_ble, menu)
        return true
    }

    private fun initViews() {
        toolbar?.also {
            ToolbarUtils.setDisplayHomeAsUpEnabled(this, it, R.drawable.ic_back_button)
            it.setNavigationOnClickListener { this.onBackPressed() }
        }
        initFieldTestingControls()
        bindValues()
        App.stopService()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.item_defaults -> {
                OptionsDialog()
                        .message(getString(ru.majordom.bletest.R.string.msg_reset_parameters))
                        .okListener(View.OnClickListener {
                            Prefs.instance.bleOptions.resetToDefaults()
                            bindValues()
                        }).show(supportFragmentManager)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        saveValues()
        if (InternalsFacade.bleManager.isBluetoothEnabled) App.runService()
        KeyboardUtils.hideSoftKeyboard(this)
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        if (checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            setFieldTestingVisible(false)
        }
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    internal fun checkStoragePermission() {
        setFieldTestingVisible(!Prefs.instance.bleOptions.advancedDebug)
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    internal fun onStorageDenied() {
        setFieldTestingVisible(false)
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    internal fun showRationaleForWriteExternal(request: PermissionRequest) {
        OptionsDialog()
                .message(getString(R.string.permission_write_external_desc))
                .okListener(View.OnClickListener { request.proceed() }).cancelListener(View.OnClickListener { request.cancel() }).show(supportFragmentManager)
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    internal fun showNeverAskAgainWriteExternal() {
        onStorageDenied()
        SnackbarCreator.showSnackbarWithSettings(this, R.string.permission_write_external_ask_enable)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private fun setFieldTestingVisible(visible: Boolean) {
        Prefs.instance.bleOptions.advancedDebug = visible
        layoutFineTune.visibility = if (visible) View.VISIBLE else View.GONE
        switchField.isChecked = visible
    }

    private fun bindValues() {
        val bleOptions = Prefs.instance.bleOptions
        bindSignalThreshold(bleOptions)
        bindReaderPower(bleOptions)
        bindReaderMovementTimeout(bleOptions)
        bindBleMethodSwitch(bleOptions)
        bindFieldTestSwitch(bleOptions)
        bindRadioButtons(bleOptions)
        bindEdits(bleOptions)
        bindDistanceLimit(bleOptions)
        bindKcoefficient(bleOptions)
        textFirmware.text = when (Prefs.instance.bleOptions.readerFirmwareVersion) {
            null -> null
            else -> "Firmware Version: ${Prefs.instance.bleOptions.readerFirmwareVersion}"
        }
    }

    private fun initFieldTestingControls() {
        if (Prefs.instance.isDebug) {
            seekbarSignal.setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {
                override fun onProgressChanged(seekBar: DiscreteSeekBar, value: Int, fromUser: Boolean) {
                    textRssiValue.text = value.toString()
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }
            })
            seekbarSignalPower.setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {
                override fun onProgressChanged(seekBar: DiscreteSeekBar, value: Int, fromUser: Boolean) {
                    textDbmPowerValue.text = BluetoothConstants.READER_POWER_TABLE[value].toString()
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }
            })
            seekbarMovementTimeout.setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {
                override fun onProgressChanged(seekBar: DiscreteSeekBar, value: Int, fromUser: Boolean) {
                    textMovementTimeoutValue.text = value.toString()
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }
            })
            seekbarDistanceLimit.setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {
                override fun onProgressChanged(seekBar: DiscreteSeekBar, value: Int, fromUser: Boolean) {
                    textDistanceValue.text = value.toString()
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }
            })
            seekbarCoefficientK.setOnProgressChangeListener(object : DiscreteSeekBar.OnProgressChangeListener {
                override fun onProgressChanged(seekBar: DiscreteSeekBar, value: Int, fromUser: Boolean) {
                    textCoefficientKValue.text = value.toString()
                }

                override fun onStartTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }

                override fun onStopTrackingTouch(seekBar: DiscreteSeekBar) {
                    //
                }
            })
            layoutSwitchBleMethod.setOnClickListener { switchBleMethod.isChecked = !switchBleMethod.isChecked }
            layoutSwitchField.setOnClickListener { checkStoragePermissionWithPermissionCheck() }
            switchField.setOnClickListener { checkStoragePermissionWithPermissionCheck() }
        } else {
            switchField.visibility = View.GONE
        }
    }

    private fun saveValues() {
        val bleOptions = Prefs.instance.bleOptions
        try {
            val code = editCode.text.toString().toLong()
            bleOptions.readerCode = code
        } catch (ex: NumberFormatException) {
            ToastUtils.show(this, "Illegal code, reverted to previous value.")
        }

        bleOptions.minRssiLevel = seekbarSignal.progress
        bleOptions.useNordic = switchBleMethod.isChecked
        bleOptions.advancedDebug = switchField.isChecked

        bleOptions.scanTimeout = editScanDuration.text.toString().toLongOrNull() ?: 0
        bleOptions.connectionTimeout = editConnTimeout.text.toString().toLongOrNull() ?: 0
        bleOptions.discoveringTimeout = editDiscoveringTimeout.text.toString().toLongOrNull() ?: 0
        bleOptions.gateStatusReadAfter = editNotifTimeout.text.toString().toLongOrNull() ?: 0
        bleOptions.pauseDefault = editPauseDefault.text.toString().toLongOrNull() ?: 0
        bleOptions.pauseConnAuthError = editPauseConnAuthError.text.toString().toLongOrNull() ?: 0
        bleOptions.pauseTech = editPauseTech.text.toString().toLongOrNull() ?: 0
        bleOptions.pauseNetwork = editPauseNetwork.text.toString().toLongOrNull() ?: 0
        bleOptions.distanceLimit = seekbarDistanceLimit.progress
        bleOptions.kCoefficient = seekbarCoefficientK.progress
        bleOptions.readerPower = seekbarSignalPower.progress
        bleOptions.readerTimeoutByMovement = seekbarMovementTimeout.progress

        setFromRadioButtons(bleOptions)
    }

    private fun setFromRadioButtons(bleOptions: BleOptions) {
        bleOptions.connPriority = when (rgConnPriority.checkedRadioButtonId) {
            ru.majordom.bletest.R.id.rbConnLow -> BluetoothConstants.CONN_PRIORITY_LOW
            ru.majordom.bletest.R.id.rbConnMid -> BluetoothConstants.CONN_PRIORITY_MID
            ru.majordom.bletest.R.id.rbConnHigh -> BluetoothConstants.CONN_PRIORITY_HIGH
            else -> BluetoothConstants.CONN_PRIORITY_LOW
        }

        bleOptions.scanMode = when (rgScanMode.checkedRadioButtonId) {
            ru.majordom.bletest.R.id.rbScanLow -> BluetoothConstants.SCAN_POWER_LOW
            ru.majordom.bletest.R.id.rbScanMid -> BluetoothConstants.SCAN_POWER_MID
            ru.majordom.bletest.R.id.rbScanHigh -> BluetoothConstants.SCAN_POWER_HIGH
            else -> BluetoothConstants.SCAN_POWER_HIGH
        }

        bleOptions.wiegandVersion = when (rgWiegand.getCheckedRadioButtonId()) {
            R.id.rbVer26 -> BluetoothConstants.WIEGAND_26
            R.id.rbVer34 -> BluetoothConstants.WIEGAND_34
            R.id.rbVer42 -> BluetoothConstants.WIEGAND_42
            else -> BluetoothConstants.DEFAULT_WIEGAND
        }
    }

    private fun bindSignalThreshold(bleOptions: BleOptions) {
        seekbarSignal.progress = bleOptions.minRssiLevel
        textRssiValue.text = seekbarSignal.progress.toString()
    }

    private fun bindReaderPower(bleOptions: BleOptions) {
        seekbarSignalPower.progress = bleOptions.readerPower
        textDbmPowerValue.text = BluetoothConstants.READER_POWER_TABLE[seekbarSignalPower.progress].toString()
    }

    private fun bindReaderMovementTimeout(bleOptions: BleOptions) {
        seekbarMovementTimeout.progress = bleOptions.readerTimeoutByMovement
        textMovementTimeoutValue.text = seekbarMovementTimeout.progress.toString()
    }

    private fun bindBleMethodSwitch(bleOptions: BleOptions) {
        switchBleMethod.isChecked = bleOptions.useNordic
        //rgConnPriority.isEnabled = !switchBleMethod.isChecked
    }

    private fun bindFieldTestSwitch(bleOptions: BleOptions) {
        switchField.isChecked = bleOptions.advancedDebug
        layoutFineTune.visibility = if (switchField.isChecked) View.VISIBLE else View.GONE
    }

    private fun bindDistanceLimit(bleOptions: BleOptions) {
        seekbarDistanceLimit.progress = bleOptions.distanceLimit
        textDistanceValue.text = seekbarDistanceLimit.progress.toString()
    }

    private fun bindKcoefficient(bleOptions: BleOptions) {
        seekbarCoefficientK.progress = bleOptions.kCoefficient
        textCoefficientKValue.text = seekbarCoefficientK.progress.toString()
    }

    private fun bindRadioButtons(bleOptions: BleOptions) {
        val connPriorityId: Int = when (bleOptions.connPriority) {
            BluetoothConstants.CONN_PRIORITY_LOW -> ru.majordom.bletest.R.id.rbConnLow
            BluetoothConstants.CONN_PRIORITY_MID -> ru.majordom.bletest.R.id.rbConnMid
            BluetoothConstants.CONN_PRIORITY_HIGH -> ru.majordom.bletest.R.id.rbConnHigh
            else -> ru.majordom.bletest.R.id.rbConnLow
        }
        rgConnPriority.check(connPriorityId)
        if (bleOptions.useNordic) {
            rgConnPriority.isEnabled = false
        }

        val scanModeId: Int = when (bleOptions.scanMode) {
            BluetoothConstants.SCAN_POWER_LOW -> ru.majordom.bletest.R.id.rbScanLow
            BluetoothConstants.SCAN_POWER_MID -> ru.majordom.bletest.R.id.rbScanMid
            BluetoothConstants.SCAN_POWER_HIGH -> ru.majordom.bletest.R.id.rbScanHigh
            else -> ru.majordom.bletest.R.id.rbScanHigh
        }
        rgScanMode.check(scanModeId)

        var wiegandVersion = BluetoothConstants.DEFAULT_WIEGAND
        when (bleOptions.wiegandVersion) {
            BluetoothConstants.WIEGAND_26 -> wiegandVersion = R.id.rbVer26
            BluetoothConstants.WIEGAND_34 -> wiegandVersion = R.id.rbVer34
            BluetoothConstants.WIEGAND_42 -> wiegandVersion = R.id.rbVer42
        }
        rgWiegand.check(wiegandVersion)
    }

    private fun bindEdits(bleOptions: BleOptions) {
        editScanDuration.setText(bleOptions.scanTimeout.toString())
        editConnTimeout.setText(bleOptions.connectionTimeout.toString())
        editDiscoveringTimeout.setText(bleOptions.discoveringTimeout.toString())
        editNotifTimeout.setText(bleOptions.gateStatusReadAfter.toString())
        editPauseDefault.setText(bleOptions.pauseDefault.toString())
        editPauseConnAuthError.setText(bleOptions.pauseConnAuthError.toString())
        editPauseTech.setText(bleOptions.pauseTech.toString())
        editPauseNetwork.setText(bleOptions.pauseNetwork.toString())
        editCode.setText(bleOptions.readerCode.toString())
    }
}