package ru.majordom.bletest.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.part_card_view_toolbar.*
import ru.majordom.bletest.BuildConfig
import ru.majordom.bletest.R
import ru.majordom.bletest.dialogs.MessageDialog
import ru.majordom.bletest.util.Prefs
import ru.majordom.bletest.util.ToolbarUtils


class SettingsActivity : AppCompatActivity() {

    companion object {
        fun newIntent(context: Context) = Intent(context, SettingsActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        initViews()
    }

    private fun initViews() {
        toolbar?.also {
            ToolbarUtils.setDisplayHomeAsUpEnabled(this, it, R.drawable.ic_back_button)
            it.setNavigationOnClickListener { this.onBackPressed() }
        }
        textVersion.text = BuildConfig.VERSION_NAME
        showFieldTestingControls()

        val prefs = Prefs.instance
        switchAutoOpening?.apply {
            isChecked = prefs.isAutoOpeningOn
            setOnCheckedChangeListener { _, isChecked ->
                if (isChecked != Prefs.instance.isAutoOpeningOn) {
                    Prefs.instance.isAutoOpeningOn = isChecked
                    /*if (!isChecked) {
                        MessageDialog()
                                .message(ru.majordom.bletest.R.string.alert_notifications_turned_off)
                                .show(supportFragmentManager)
                    }*/
                }
            }
        }
        layoutAutoOpening.setOnClickListener {
            switchAutoOpening.isChecked = !switchAutoOpening.isChecked
        }
        layoutBleTuning.setOnClickListener {
            startActivity(AdvancedBleActivity.newIntent(this))
        }
    }

    private fun showFieldTestingControls() {
        layoutBleTuning.visibility = if (Prefs.instance.isDebug) View.VISIBLE else View.GONE
    }
}
