package ru.majordom.bletest.activities

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.annotation.StringRes
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_open_gate.*
import permissions.dispatcher.*
import ru.majordom.bleservice.InternalsFacade
import ru.majordom.bleservice.bluetooth.bleService.BleReceiver
import ru.majordom.bleservice.bluetooth.bleService.BleService
import ru.majordom.bleservice.bluetooth.bleService.GateState
import ru.majordom.bleservice.notifications.ServiceNotificationType
import ru.majordom.bleservice.notifications.ServiceNotifications
import ru.majordom.bleservice.power.PowerManagerWrapper
import ru.majordom.bleservice.zones.GeolocationChecker
import ru.majordom.bletest.App
import ru.majordom.bletest.R
import ru.majordom.bletest.bleintegration.AppServiceNotificationsDelegate
import ru.majordom.bletest.dialogs.AppAlertDialog
import ru.majordom.bletest.dialogs.MessageDialog
import ru.majordom.bletest.dialogs.OptionsDialog
import ru.majordom.bletest.fragments.BlockOnDisabledBluetoothFragment
import ru.majordom.bletest.util.Prefs
import ru.majordom.bletest.util.SnackbarCreator
import ru.majordom.bletest.util.ToastUtils

@RuntimePermissions
class OpenGateActivity : AppCompatActivity(),
        BleReceiver.BleCallbackListener,
        BlockOnDisabledBluetoothFragment.Listener {

    companion object {
        fun newIntent(context: Context) = Intent(context, OpenGateActivity::class.java)
    }

    private var bleReceiver: BleReceiver? = null
    private var errorDialog: MessageDialog? = null
    private var isLocationDialogRequested: Boolean = false
    private var bottomSheet: BottomSheetBehavior<*>? = null
    private var logsAdapter: LogsAdapter? = null
    private lateinit var pmWrapper: PowerManagerWrapper
    private lateinit var serviceNotificationsDelegate: ServiceNotifications

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_gate)
        BlockOnDisabledBluetoothFragment.addInto(this, savedInstanceState)
        bleReceiver = BleReceiver(this)
        pmWrapper = InternalsFacade.powerManager
        serviceNotificationsDelegate = AppServiceNotificationsDelegate(this, Prefs.instance)
        initViews()
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(bleReceiver, BleReceiver.getIntentFilter())
        BleService.checkStatus(this)
        checkGeolocationEnabled()
        checkPowerManagementSettings()
        checkCustomPhonePowerOptimizations()
        btnShareLogs.visibility = if (Prefs.instance.bleOptions.advancedDebug) View.VISIBLE else View.INVISIBLE
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(bleReceiver)
    }

    private fun initViews() {
        imgOpen.isEnabled = false
        runServiceWithPermissionCheck()
        if (!Prefs.instance.isDebug) {
            btnShareLogs.visibility = View.GONE
            textGateName.visibility = View.GONE
        } else {
            layoutBottomSheet.visibility = View.VISIBLE
            setupBottomSheet()
        }
        imgClose.setOnClickListener { onBackPressed() }
        imgOpen.setOnClickListener { BleService.openGate(this) }
        btnShareLogs.setOnClickListener { BleService.shareLogs(this) }
        imgSettings.setOnClickListener {
            startActivity(SettingsActivity.newIntent(this))
        }
    }

    private fun setupBottomSheet() {
        BottomSheetBehavior.from<View>(layoutBottomSheet)?.also {
            bottomSheet = it
            it.state = BottomSheetBehavior.STATE_COLLAPSED
            logsAdapter = LogsAdapter()
            recyclerLogs.apply {
                layoutManager = LinearLayoutManager(this@OpenGateActivity, LinearLayoutManager.VERTICAL, true)
                layoutAnimation = null
                itemAnimator = null
                adapter = logsAdapter
            }
        }
    }

    private fun checkPowerManagementSettings() {
        if (pmWrapper.isPowerSaveMode && !pmWrapper.isIgnoringBatteryOptimizations) {
            val optionsDialog = OptionsDialog()
                    .message(getString(R.string.power_save_disable_description))
                    .okListener(View.OnClickListener {
                        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            val whitelistIntent = pmWrapper.addToWhitelistIntent
                            startActivity(whitelistIntent)
                        }
                    })
            optionsDialog.show(supportFragmentManager)
        } else {
            serviceNotificationsDelegate.clear(ServiceNotificationType.POWER_SAVE_EXPOSED)
        }
    }

    private fun checkCustomPhonePowerOptimizations() {
        val storage = Prefs.instance
        if (!storage.isPowerManagerAlertShowed) {
            pmWrapper.getNonStandardPhonesBatteryScreenIntent()?.let { intent ->
                val optionsDialog = OptionsDialog()
                        .message(getString(R.string.power_save_check))
                        .okListener(View.OnClickListener {
                            startActivity(intent)
                        })
                storage.isPowerManagerAlertShowed = true
                optionsDialog.show(supportFragmentManager)
            } ?: run {
                storage.isPowerManagerAlertShowed = true
            }
        }
    }

    private fun hideProgressHolder(hide: Boolean) {
        progressHolder.visibility = if (hide) View.INVISIBLE else View.VISIBLE
    }

    private fun updateGateStatus(isInRange: Boolean, isOpeningInProcess: Boolean) {
        textStatus.setText(if (isInRange) ru.majordom.bletest.R.string.gate_detected else ru.majordom.bletest.R.string.no_gate_detected)
        hideProgressHolder(isInRange && !isOpeningInProcess)
        imgOpen.isEnabled = !isOpeningInProcess && isInRange
        textSubStatus.setText(if (isInRange) ru.majordom.bletest.R.string.push_the_button_to_open_the_gate else ru.majordom.bletest.R.string.move_closer_to_the_gate)
        setHint(ru.majordom.bletest.R.string.gate_msg_searching)
    }

    private fun setErrorHint(hint: String?) {
        setHint(getString(ru.majordom.bletest.R.string.error_template, hint))
    }

    private fun setHint(@StringRes hintId: Int) {
        textHint.text = getString(hintId)
    }

    private fun setHint(hint: String?) {
        textHint.text = hint
    }

    override fun onGateStatus(state: GateState) {
        updateGateStatus(state.isInRange, state.isOpeningInProcess)
        setHint(state.message)
        textGateName.text = state.gateName
        if (state.isError) {
            if (errorDialog != null) {
                errorDialog?.dismiss()
                errorDialog = null
            }
            errorDialog = MessageDialog()
                    .title(state.messageTitle)
                    .message(state.message)
            errorDialog?.show(supportFragmentManager, "dialog")
        }
    }

    override fun onLogData(log: String) {
        if (logsAdapter != null) {
            logsAdapter!!.addItem(log)
        }
    }

    override fun bluetoothUnavailable() = Unit

    override fun bluetoothAvailable() = Unit

    override fun goBack() = Unit //this.onBackPressed()

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    internal fun runService() = App.runService()

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    internal fun showRationaleForLocation(request: PermissionRequest) {
        OptionsDialog()
                .message(getString(ru.majordom.bletest.R.string.permission_location_ble_desc))
                .okListener(View.OnClickListener { request.proceed() }).cancelListener(View.OnClickListener { request.cancel() }).show(supportFragmentManager)
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    internal fun showNeverAskAgainLocation() {
        SnackbarCreator.showSnackbarWithSettings(this, ru.majordom.bletest.R.string.permission_location_ask_enable)
    }

    private fun checkGeolocationEnabled() {
        if (!isLocationDialogRequested && !GeolocationChecker.isGeolocationEnabled(this)) {
            isLocationDialogRequested = true
            askToEnableGeolocation(this) {
                isLocationDialogRequested = false
            }
        }
    }

    private fun askToEnableGeolocation(context: Context, clickListener: (Boolean) -> Unit) {
        val dialog = AppAlertDialog.getBuilderWithDefaults(context, context.getString(R.string.enable_location_ble_desc))
                .setPositiveButton(R.string.yes) { dialog, _ ->
                    dialog.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    try {
                        context.startActivity(intent)
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.show(context, R.string.error_no_activity_found)
                    } finally {
                        clickListener.invoke(true)
                    }
                }
                .setNegativeButton(R.string.no) { dialog, _ ->
                    dialog.dismiss()
                    clickListener.invoke(false)
                }.create()
        dialog.show()
    }
}