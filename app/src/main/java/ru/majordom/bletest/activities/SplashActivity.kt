package ru.majordom.bletest.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.majordom.bletest.util.Prefs


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(OpenGateActivity.newIntent(this))
        finish()
    }
}