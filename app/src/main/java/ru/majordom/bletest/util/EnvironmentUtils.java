package ru.majordom.bletest.util;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.Locale;

import ru.majordom.bletest.BuildConfig;

public class EnvironmentUtils {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final float XHDPI = 2;

    private static String deviceName;

    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getAppVersion() {
        return BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";
    }

    public static String getModel() {
        return getManufacturer() + " " + getRawModel();
    }

    public static String getRawModel() {
        return Build.MODEL;
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public boolean checkPlayServices(Activity context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("Google Play Services", "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    public static float getDpiDigit(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
