package ru.majordom.bletest.util

import android.content.Context
import android.net.Uri
import android.support.annotation.RawRes
import android.util.TypedValue
import java.io.File

object RawUtils {

    fun getUriOfRaw(context: Context, @RawRes rawRes: Int) =
            Uri.parse("android.resource://" + context.packageName + "/" + rawRes)

    fun getUrlFromRaw(context: Context, @RawRes rawRes: Int): String {
        val rawValue = TypedValue()
        context.resources.getValue(rawRes, rawValue, true)
        val fileName = File(rawValue.string.toString()).name
        return "file:///android_res/raw/$fileName"
    }
}