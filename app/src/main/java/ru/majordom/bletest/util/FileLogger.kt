package ru.majordom.bletest.util

import android.content.Context
import android.net.Uri
import android.support.v4.content.FileProvider
import ru.majordom.bleservice.common.Logger
import java.io.*

class FileLogger(
        private val context: Context,
        private val filesDir: File,
        private val fileName: String,
        private val append: Boolean
) : Logger {

    private var file: File? = null
    private var fOut: FileOutputStream? = null
    private var writer: BufferedWriter? = null

    override val contentUri: Uri?
        get() = file?.let {
            FileProvider.getUriForFile(context, "ru.majordom.bletest.fileprovider", it)
        }

    @Throws(IOException::class)
    override fun close() {
        writer?.close()
        fOut?.close()
    }

    override fun log(data: String) {
        if (file == null || writer == null) {
            prepareFile()
        }
        writer?.write(data)
        writer?.newLine()
    }

    override fun flush() {
        writer?.apply {
            flush()
        }
    }

    @Throws(IOException::class)
    private fun prepareFile() {
        val logDir = File(filesDir, context.getString(ru.majordom.bletest.R.string.dir_log))
        if (!logDir.exists()) {
            if (!logDir.mkdirs()) throw IOException("Directory " + logDir.toString() + "was not created")
        }
        val newFile = File(logDir, fileName)
        file = newFile
        if (newFile.exists() && !append) {
            if (!newFile.delete()) throw IOException("File " + newFile.toString() + "was not deleted")
        } else if (!newFile.exists()) {
            if (!newFile.createNewFile()) throw IOException("File " + newFile.toString() + "was not created")
        }
        val newFout = FileOutputStream(newFile)
        fOut = newFout
        writer = BufferedWriter(OutputStreamWriter(newFout))
    }

    override fun toString(): String {
        return "FileLogger{" +
                "fileName='" + fileName + '\''.toString() +
                ", append=" + append +
                '}'.toString()
    }
}