package ru.majordom.bletest.util;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class ToolbarUtils {

    public static void setDisplayHomeAsUpEnabled(AppCompatActivity activity) {
        setDisplayHomeAsUpEnabled(activity, 0);
    }

    public static void setDisplayHomeAsUpEnabled(AppCompatActivity activity, int iconResId) {
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            if (iconResId != 0) {
                actionBar.setHomeAsUpIndicator(iconResId);
            }
        }
    }

    public static void setDisplayHomeAsUpEnabled(AppCompatActivity activity, Toolbar toolbar) {
        setDisplayHomeAsUpEnabled(activity, toolbar, 0);
    }

    public static void setDisplayHomeAsUpEnabled(AppCompatActivity activity, Toolbar toolbar, int iconResId) {
        if (iconResId != 0) {
            toolbar.setNavigationIcon(iconResId);
        }
        activity.setSupportActionBar(toolbar);
        setDisplayHomeAsUpEnabled(activity);
    }

    public static void setUpToolbarForFragment(
            Toolbar toolbar,
            int titleResId,
            int iconResId,
            int menuResId,
            View.OnClickListener navigationOnClickListener,
            Toolbar.OnMenuItemClickListener onMenuItemClickListener
    ) {
        if (titleResId != 0) toolbar.setTitle(titleResId);
        if (iconResId != 0) toolbar.setNavigationIcon(iconResId);
        if (menuResId != 0) toolbar.inflateMenu(menuResId);
        toolbar.setNavigationOnClickListener(navigationOnClickListener);
        toolbar.setOnMenuItemClickListener(onMenuItemClickListener);
    }
}
