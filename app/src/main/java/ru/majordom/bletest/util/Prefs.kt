package ru.majordom.bletest.util


import android.content.Context
import android.content.SharedPreferences
import ru.majordom.bleservice.common.*
import ru.majordom.bletest.App
import ru.majordom.bletest.BuildConfig
import ru.majordom.bletest.R

class Prefs private constructor() : LocalStorage {
    private val preferencesRaw: SharedPreferences =
            App.app.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    override val bleOptions: BleOptions = PrefsBleOptions(preferencesRaw)

    override val zonesOptions: ZonesOptions = PrefsZonesOptions(preferencesRaw)

    override var isAutoOpeningOn: Boolean
        get() = preferencesRaw.getBoolean(NOTIFICATIONS_AUTO_OPENING, false)
        set(value) = preferencesRaw.edit().putBoolean(NOTIFICATIONS_AUTO_OPENING, value).apply()

    override val isDebug: Boolean
        get() = BuildConfig.DEBUG

    override val appName: String
        get() = App.app.getString(R.string.app_name)

    override val osVersion: String
        get() = EnvironmentUtils.getOsVersion()

    override val appVersion: String
        get() = EnvironmentUtils.getAppVersion()

    override val deviceModel: String
        get() = EnvironmentUtils.getModel()

    override var isPowerManagerAlertShowed: Boolean
        get() = preferencesRaw.getBoolean(POWER_SCREEN_SHOWED, false)
        set(value) = preferencesRaw.edit().putBoolean(POWER_SCREEN_SHOWED, value).apply()

    companion object {

        private const val PREFS_NAME = "app_prefs"
        private const val NOTIFICATIONS_AUTO_OPENING = "automatic_opening"
        private const val POWER_SCREEN_SHOWED = "power_screen_showed"

        private lateinit var staticInstance: Prefs

        val instance: Prefs
            get() {
                if (!::staticInstance.isInitialized) {
                    staticInstance = Prefs()
                }
                return staticInstance
            }
    }
}