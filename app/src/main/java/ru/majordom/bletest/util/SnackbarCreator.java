package ru.majordom.bletest.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;

public class SnackbarCreator {

    public static void showSnackbarWithSettings(final Activity activity, View view, @StringRes int description) {
        Snackbar snackbar = Snackbar.make(view, description, Snackbar.LENGTH_LONG);
        snackbar.setAction(ru.majordom.bletest.R.string.action_settings, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                intent.setData(uri);
                try {
                    activity.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    ToastUtils.show(activity, ru.majordom.bletest.R.string.error_no_activity_found);
                }
            }
        });
        snackbar.setActionTextColor(ContextCompat.getColor(activity, ru.majordom.bletest.R.color.colorAccent));
        snackbar.show();
    }

    public static void showSnackbarWithSettings(final Activity activity, @StringRes int description) {
        showSnackbarWithSettings(activity, getViewRoot(activity), description);
    }

    private static ViewGroup getViewRoot(Activity activity) {
        return (ViewGroup)((ViewGroup) activity.findViewById(android.R.id.content)).getChildAt(0);
    }
}