package ru.majordom.bletest.util

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.AudioAttributes
import android.os.Build
import android.support.annotation.RawRes
import android.support.v4.app.NotificationCompat
import ru.majordom.bletest.R


class NotificationManagerHelper private constructor(private val context: Context) {

    private val manager: NotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    @JvmOverloads
    fun getBuilderWithDefaults(
            title: String? = null,
            message: String? = null,
            @RawRes rawRes: Int? = null,
            showBadge: Boolean = false): NotificationCompat.Builder {
        val channelId = createChannel(rawRes, showBadge = showBadge)
        val builder = NotificationCompat.Builder(context, channelId)
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(if (!title.isNullOrBlank()) title else context.getString(R.string.app_name))
                .setAutoCancel(true)
                .setGroup(GROUP)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            builder.priority = NotificationCompat.PRIORITY_MAX
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            if (rawRes != null) {
                builder.setSound(RawUtils.getUriOfRaw(context, rawRes))
            }
        }

        if (!message.isNullOrBlank()) {
            builder.setContentText(message)
            builder.setStyle(NotificationCompat.BigTextStyle().bigText(message))
        }
        return builder
    }

    private fun createChannel(
            @RawRes rawRes: Int? = null,
            importance: Int? = null,
            showBadge: Boolean = false): String {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return CHANNEL_DEFAULT
        }
        val channelId = if (rawRes == null) CHANNEL_DEFAULT else "Token Channel $rawRes"
        val channel = NotificationChannel(channelId,
                channelId,
                importance ?: NotificationManager.IMPORTANCE_MAX).apply {
            description = channelId
            enableVibration(true)
            setShowBadge(showBadge)
            lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            if (rawRes != null) {
                val audioAttributes = AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build()
                setSound(RawUtils.getUriOfRaw(context, rawRes), audioAttributes)
            }
        }
        manager.createNotificationChannel(channel)
        return channelId
    }

    fun notify(code: Int, builder: NotificationCompat.Builder) = manager.notify(code, builder.build())

    fun notify(code: Int, builder: NotificationCompat.Builder, flags: Int) {
        with(builder.build()) {
            this.flags = flags
            manager.notify(code, this)
        }
    }

    fun cancel(code: Int) = manager.cancel(code)

    fun cancelAll() = manager.cancelAll()

    companion object {
        private const val CHANNEL_DEFAULT = "BleTest Channel Default"
        private const val GROUP = "bletest.notifications.group"

        @SuppressLint("StaticFieldLeak")
        private var instance: NotificationManagerHelper? = null

        fun prepareWith(context: Context): NotificationManagerHelper =
                instance ?: NotificationManagerHelper(context.applicationContext)
    }
}