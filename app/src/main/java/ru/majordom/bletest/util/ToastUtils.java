package ru.majordom.bletest.util;

import android.content.Context;
import android.content.res.Resources;
import android.widget.Toast;


public class ToastUtils {

    public static void show(Context context, String str) {
        if (context != null) {
            Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
        }
    }

    public static void show(Context context, int strResId) {
        if (context != null) {
            try {
                Toast.makeText(context, strResId, Toast.LENGTH_SHORT).show();
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}