package ru.majordom.bletest.fragments

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import ru.majordom.bleservice.InternalsFacade
import ru.majordom.bleservice.bluetooth.bleService.BluetoothStateReceiver
import ru.majordom.bletest.R

class BlockOnDisabledBluetoothFragment : Fragment(), BluetoothStateReceiver.BluetoothStateListener {

    companion object {

        private const val FRAGMENT_TAG = "fragments.BlockOnDisabledBluetoothFragment.FRAGMENT_TAG"
        private const val REQUEST_CODE_ENABLE_BLUETOOTH = 1001

        fun newInstance() = BlockOnDisabledBluetoothFragment()

        fun addInto(activity: AppCompatActivity, savedInstanceState: Bundle?) {
            if (savedInstanceState == null) {
                activity.supportFragmentManager
                        .beginTransaction()
                        .add(newInstance(), FRAGMENT_TAG)
                        .commit()
            }
        }
    }

    private var listener: Listener? = null
    private var dialog: AlertDialog? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        initListener(context)
    }

    private fun initListener(context: Context?) {
        if (context is Listener) {
            listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        dialog = AlertDialog.Builder(requireContext())
                .setTitle(R.string.bluetooth_required)
                .setMessage(R.string.bluetooth_required_hint)
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok) { _, _ -> requestBluetooth(this@BlockOnDisabledBluetoothFragment, REQUEST_CODE_ENABLE_BLUETOOTH) }
                .setNeutralButton(R.string.bluetooth_dialog_go_back) { _, _ ->
                    dialog?.dismiss()
                    listener?.goBack()
                }
                .create()
        if (InternalsFacade.bleManager.isBluetoothEnabled) {
            onBluetoothAvailable()
        } else {
            onBluetoothUnavailable()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) onBluetoothAvailable()
            else onBluetoothUnavailable()
        }
    }

    override fun onPause() {
        InternalsFacade.bleManager.removeBluetoothStateListener(this)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        InternalsFacade.bleManager.addBluetoothStateListener(this)
    }

    override fun onBluetoothAvailable() {
        dialog?.dismiss()
        listener?.bluetoothAvailable()
    }

    override fun onBluetoothUnavailable() {
        dialog?.show()
        listener?.bluetoothUnavailable()
    }

    private fun requestBluetooth(fragment: Fragment, requestCode: Int) {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        fragment.startActivityForResult(enableBtIntent, requestCode)
    }

    interface Listener {
        fun bluetoothUnavailable()
        fun bluetoothAvailable()
        fun goBack()
    }
}