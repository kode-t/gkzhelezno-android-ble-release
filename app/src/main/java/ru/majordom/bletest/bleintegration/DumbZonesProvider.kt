package ru.majordom.bletest.bleintegration

import android.arch.lifecycle.MutableLiveData
import android.location.Location
import ru.majordom.bleservice.zones.ReaderZone
import ru.majordom.bleservice.zones.ZoneLocation
import ru.majordom.bleservice.zones.ZonesProvider

class DumbZonesProvider : ZonesProvider {
    private val zones = listOf(
            ReaderZone(
                    "0",
                    "KODE Tomsk Office Zone",
                    ZoneLocation(56.475938, 84.962539)
            ),
            ReaderZone(
                    "1",
                    "KODE Tomsk Hardware Office Zone",
                    ZoneLocation(56.453185, 84.975616)
            ),
            ReaderZone(
                    "2",
                    "Zhelezno Kirov Office Zone",
                    ZoneLocation(58.606961, 49.687782)
            ),
            ReaderZone(
                    "3",
                    "Zhelezno Kirov Test Site",
                    ZoneLocation(58.518324, 49.698103)
            )
    )

    override fun getZones(currentLocation: Location, radius: Int, maxCount: Int) =
            MutableLiveData<List<ReaderZone>>().apply {
                postValue(zones)
            }
}