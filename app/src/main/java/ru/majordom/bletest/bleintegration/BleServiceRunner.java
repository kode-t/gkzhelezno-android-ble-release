package ru.majordom.bletest.bleintegration;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import java.util.Objects;

import ru.majordom.bletest.App;

public class BleServiceRunner extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.equals(intent.getAction(), Intent.ACTION_BOOT_COMPLETED)) {
            App.Companion.runService();
        }
    }
}
