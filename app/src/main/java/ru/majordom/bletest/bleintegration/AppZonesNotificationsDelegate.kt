package ru.majordom.bletest.bleintegration

import android.app.PendingIntent
import android.content.Context
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import ru.majordom.bleservice.notifications.ZonesNotifications
import ru.majordom.bletest.R
import ru.majordom.bletest.activities.OpenGateActivity
import ru.majordom.bletest.util.NotificationManagerHelper

class AppZonesNotificationsDelegate : ZonesNotifications {

    override fun sendBluetoothEnabledNotification(context: Context) {
        val applicationIntent = OpenGateActivity.newIntent(context)
        val pendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, applicationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        NotificationManagerHelper.prepareWith(context)?.apply {
            val builder = getBuilderWithDefaults(
                    context.getString(R.string.notif_title_bluetooth_enabled_due_to_geofence),
                    context.getString(R.string.notif_bluetooth_enabled_due_to_geofence))
            builder.setSmallIcon(R.drawable.ic_manual_opening)
                    .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                    .setStyle(NotificationCompat.BigTextStyle().bigText(context.getString(R.string.notif_bluetooth_enabled_due_to_geofence)))
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent)
            notify(NOTIFICATION_ID, builder)
        }
    }

    private companion object {
        private const val NOTIFICATION_ID = 100000
    }
}