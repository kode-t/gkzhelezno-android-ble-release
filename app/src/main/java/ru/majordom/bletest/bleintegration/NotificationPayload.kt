package ru.majordom.bletest.bleintegration

import android.support.annotation.RawRes

internal class NotificationPayload(
        val title: String = "",
        val message: String = "",
        @RawRes val soundId: Int? = null
)