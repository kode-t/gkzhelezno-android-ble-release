package ru.majordom.bletest.bleintegration

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.support.annotation.RawRes
import android.support.v4.app.NotificationCompat
import ru.majordom.bleservice.bluetooth.bleService.BleService
import ru.majordom.bleservice.common.LocalStorage
import ru.majordom.bleservice.notifications.ServiceNotificationType
import ru.majordom.bleservice.notifications.ServiceNotifications
import ru.majordom.bletest.R
import ru.majordom.bletest.activities.OpenGateActivity
import ru.majordom.bletest.util.NotificationManagerHelper

class AppServiceNotificationsDelegate(
        private val context: Context,
        private val prefs: LocalStorage
) : ServiceNotifications {
    private val notificationManager = NotificationManagerHelper.prepareWith(context)

    override fun sendForegroundServiceNotification(service: Service, isServiceStarted: Boolean, type: ServiceNotificationType) {
        val code = getCodeByNotificationType(type)
        if (isServiceStarted) {
            val applicationIntent = OpenGateActivity.newIntent(context)
            val pendingIntent = PendingIntent.getActivity(service, code, applicationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            val builder = getDefaultNotificationBuilder(pendingIntent, null)
            val data = getPayloadByNotificationType(type)
            builder.setContentTitle(service.getString(R.string.app_name))
                    .setStyle(NotificationCompat.BigTextStyle().bigText(data.message))
                    .setContentText(data.message)
                    .setOnlyAlertOnce(true)
            notificationManager.notify(code, builder)
            service.startForeground(code, builder.build())
        } else {
            notificationManager.cancel(code)
        }
    }

    override fun sendInfoNotification(type: ServiceNotificationType, message: String?) {
        val code = getCodeByNotificationType(type)
        val data = getPayloadByNotificationType(type)
        val applicationIntent = OpenGateActivity.newIntent(context)
        val pendingIntent = PendingIntent.getActivity(context, code, applicationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = getDefaultNotificationBuilder(pendingIntent, data.soundId)
        val msg = message ?: data.message
        builder.setContentTitle(data.title)
                .setStyle(NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
        notificationManager.notify(code, builder)
    }

    override fun sendPossibleToOpenNotification() {
        val type = ServiceNotificationType.POSSIBLE_TO_OPEN
        val code = getCodeByNotificationType(type)
        val data = getPayloadByNotificationType(type)
        val openGateScreenIntent = OpenGateActivity.newIntent(context)
        val openGateScreenPendingIntent = PendingIntent.getActivity(context, code, openGateScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val openGateIntent = getServiceIntent(context, BleService.ACTION_OPEN_GATE)
        val openGatePendingIntent = PendingIntent.getService(context, code, openGateIntent, PendingIntent.FLAG_ONE_SHOT)

        val builder = getDefaultNotificationBuilder(openGateScreenPendingIntent, null)
        builder.setContentTitle(data.title)
                .setContentText(data.message)

        val action = NotificationCompat.Action.Builder(
                R.drawable.ic_manual_opening_vector, // will be displayed on <= api 23
                context.getString(R.string.notif_action_open_the_gates),
                openGatePendingIntent)
                .build()
        builder.addAction(action)
        notificationManager.notify(code, builder)
    }

    override fun sendOpeningNotification() {
        val type = ServiceNotificationType.OPENING
        val code = getCodeByNotificationType(type)
        val data = getPayloadByNotificationType(type)
        val openGateScreenIntent = OpenGateActivity.newIntent(context)
        val openGateScreenPendingIntent = PendingIntent.getActivity(context, code, openGateScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = getDefaultNotificationBuilder(openGateScreenPendingIntent, data.soundId)
        builder.setContentTitle(data.title)
                .setContentText(data.message)

        notificationManager.notify(code, builder)
    }

    override fun sendApproachingNotification(isTooFar: Boolean) {
        val type = ServiceNotificationType.APPROACHING
        val code = getCodeByNotificationType(type)
        val data = getPayloadByNotificationType(type)
        val openGateScreenIntent = OpenGateActivity.newIntent(context)
        val openGateScreenPendingIntent = PendingIntent.getActivity(context, code, openGateScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val message = context.getString(if (isTooFar) R.string.notif_approaching_gate_too_far else R.string.notif_approaching_gate_low_rssi)
        val soundRes = (if (prefs.bleOptions.advancedDebug) if (isTooFar) R.raw.sheep_7 else R.raw.meow_1 else null)?.toInt()
        val builder = getDefaultNotificationBuilder(openGateScreenPendingIntent, soundRes)
        builder.setContentTitle(data.title)
                .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setVibrate(longArrayOf(0L))
        notificationManager.notify(code, builder)
    }

    override fun sendBatterySaverNotification() {
        val type = ServiceNotificationType.POWER_SAVE_EXPOSED
        val code = getCodeByNotificationType(type)
        val data = getPayloadByNotificationType(type)
        val homeIntent = OpenGateActivity.newIntent(context)
        val homePendingIntent = PendingIntent.getActivity(context, code, homeIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = getDefaultNotificationBuilder(homePendingIntent, null)
        builder.setContentTitle(data.title)
                .setStyle(NotificationCompat.BigTextStyle().bigText(data.message))
                .setContentText(data.message)
                .setVibrate(longArrayOf(0L))
        notificationManager.notify(code, builder, Notification.FLAG_NO_CLEAR)
    }

    override fun clearServiceNotifications(all: Boolean) {
        if (all) {
            notificationManager.cancelAll()
        } else {
            notificationManager.cancel(OPENING_GATES_NOTIFY_ID)
            notificationManager.cancel(APPROACHING_NOTIFY_ID)
            notificationManager.cancel(POSSIBLE_TO_OPEN_NOTIFY_ID)
        }
    }

    override fun clear(type: ServiceNotificationType) {
        val code = getCodeByNotificationType(type)
        notificationManager.cancel(code)
    }

    private fun getDefaultNotificationBuilder(
            pendingIntent: PendingIntent,
            @RawRes rawRes: Int?
    ): NotificationCompat.Builder =
            notificationManager.getBuilderWithDefaults(null, null, rawRes)
                    .setContentIntent(pendingIntent)

    private fun getServiceIntent(context: Context, action: String) =
            Intent(context, BleService::class.java).apply {
                this.action = action
            }

    private fun getCodeByNotificationType(type: ServiceNotificationType): Int = when (type) {
        ServiceNotificationType.SERVICE_RUNNING,
        ServiceNotificationType.SERVICE_GEOLOCATION_DISABLED,
        ServiceNotificationType.SERVICE_BLUETOOTH_DISABLED -> BACKGROUND_SERVICE_NOTIFY_ID
        ServiceNotificationType.APPROACHING -> APPROACHING_NOTIFY_ID
        ServiceNotificationType.POSSIBLE_TO_OPEN -> POSSIBLE_TO_OPEN_NOTIFY_ID
        ServiceNotificationType.OPENING -> OPENING_GATES_NOTIFY_ID
        ServiceNotificationType.OPENED -> GATE_OPENED_NOTIFY_ID
        ServiceNotificationType.CONNECTION_INTERVAL_CHANGED -> BARRIER_ERROR_NOTIFY_ID
        ServiceNotificationType.NOTIFICATION_TIMEOUT -> BARRIER_ERROR_NOTIFY_ID
        ServiceNotificationType.POWER_SAVE_EXPOSED -> POWER_SAVE_EXPOSED_NOTIFY_ID
        ServiceNotificationType.BARRIER_ERROR -> BARRIER_ERROR_NOTIFY_ID
    }

    private fun getPayloadByNotificationType(type: ServiceNotificationType): NotificationPayload {
        var title: String = context.getString(R.string.app_name)
        var message: String? = null
        var soundId: Int? = null
        when (type) {
            ServiceNotificationType.SERVICE_RUNNING -> {
                message = context.getString(R.string.notif_bluetooth_service_is_running)
            }
            ServiceNotificationType.SERVICE_GEOLOCATION_DISABLED -> {
                title = context.getString(R.string.notif_geolocation_is_not_enabled)
                message = context.getString(R.string.permission_location_ask_enable)
            }
            ServiceNotificationType.SERVICE_BLUETOOTH_DISABLED -> {
                message = context.getString(R.string.bluetooth_unavailable)
            }
            ServiceNotificationType.APPROACHING -> Unit
            ServiceNotificationType.POSSIBLE_TO_OPEN -> {
                title = context.getString(R.string.notif_gate_detected)
                message = context.getString(R.string.notif_gate_tap_or_expand)
            }
            ServiceNotificationType.OPENING -> {
                message = context.getString(R.string.notif_initiating_parking_process)
                soundId = if (prefs.bleOptions.advancedDebug) R.raw.single_bark_2 else R.raw.user_connecting
            }
            ServiceNotificationType.OPENED -> {
                message = context.getString(R.string.gate_msg_opened)
                soundId = R.raw.response_success_5
            }
            ServiceNotificationType.CONNECTION_INTERVAL_CHANGED -> {
                if (prefs.bleOptions.advancedDebug) soundId = R.raw.gun_reload_6
            }
            ServiceNotificationType.NOTIFICATION_TIMEOUT -> {
                if (prefs.bleOptions.advancedDebug) soundId = R.raw.corolla_horn_4
            }
            ServiceNotificationType.POWER_SAVE_EXPOSED -> {
                message = context.getString(R.string.notif_battery_saver)
            }
            ServiceNotificationType.BARRIER_ERROR -> {
                soundId = if (prefs.bleOptions.advancedDebug) R.raw.gun_shot_heavy_3 else R.raw.user_failure
            }
        }
        return NotificationPayload(title, message ?: "", soundId)
    }

    private companion object {
        private const val BACKGROUND_SERVICE_NOTIFY_ID = 151
        private const val POSSIBLE_TO_OPEN_NOTIFY_ID = 152
        private const val OPENING_GATES_NOTIFY_ID = 153
        private const val APPROACHING_NOTIFY_ID = 154
        private const val POWER_SAVE_EXPOSED_NOTIFY_ID = 155
        private const val BARRIER_ERROR_NOTIFY_ID = 156
        private const val GATE_OPENED_NOTIFY_ID = 157
    }
}